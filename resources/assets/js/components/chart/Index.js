
  import { Bar } from 'vue-chartjs'

  export default {
  extends: Bar,
  props: ['label','data','description'],
  mounted () {
    // Overwriting base render method with actual data.
    var self = this
    this.renderChart({
      labels: this.label,
      datasets: [
        {
          label: self.description,
          backgroundColor: '#f87979',
          data: this.data
        }
      ]
    })
  }
}
