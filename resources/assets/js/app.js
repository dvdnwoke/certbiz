
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('certificate', require('./components/CertificateAddon.vue'));
Vue.component('AccountingLink', require('./components/AccountingLinks.vue'));
Vue.component('Items', require('./components/certificate/Items.vue'));
Vue.component('ItemsList', require('./components/certificate/ItemsList.vue'));
Vue.component('DashboardChart', require('./components/chart/Dashboard.vue'));
Vue.component('AccountChart', require('./components/chart/Account.vue'));
Vue.component('ItemsUpdate', require('./components/certificate/ItemsUpdate.vue'));
Vue.component('Notification', require('./components/Notification.vue'));

Vue.filter('url', function (value) {
  var url = document.location.href
  var arr = url.split("/")
  return arr[0] + '//' + arr[2] +  value
})

const app = new Vue({
  el: '#app',
  mounted () {

  }
});
