var deleteCustomer = function (id) {
  document.getElementById('deleteCustomerId').value = deleteCustomerId
  document.getElementById('deleteCustomerForm').submit()
}
var deleteCustomerId = null

$(document).ready(function(){
  var searchElementWidth = document.getElementById('searchUserForCert').clientWith

  window.confirmDeleteCustomer =  function (id) {
    $('#customerDeleteModal').modal('show')
    deleteCustomerId = id
    $('#deleteButton').click(deleteCustomer)
  }

  $('#searchUserForCert').keyup(function () {
    var by = document.getElementById('searchUserForCertFilter').value
    var searchUser = document.getElementById('searchUserForCert')
    var userIdElement = document.getElementById('searchUserForCertId')
    var query = searchUser.value
    if (query.length < 1) {
      $('#userSearchResult').empty()
      $('#userSearchResult').hide(true)
      return
    }
    $('#userSearchResult').empty()
    window.axios({
      method: 'post',
      url: '/customer/search/json',
      data: {
        _token: document.head.querySelector('meta[name="csrf-token"]'),
        by: by,
        query: query
      }
    }).then(function (response) {
      $('#userSearchResult').show(true)
      var results = response.data.results
      for (var i = 0; i < results.length; i++) {
        let name = `${results[i].firstName}, ${results[i].middleName} ${results[i].lastName}`
        let id = results[i].id
        $('#userSearchResult').append(function () {
          return $('<li>' + name + '</li>').click(function () {
            $('#userSearchResult').hide(true)
            searchUser.value = name
            userIdElement.value = id
          });
        })
      }
    })
  })
})
