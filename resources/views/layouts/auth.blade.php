<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Certiz') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
</head>
<body class="auth-body">

  <div id="app">
        @if(!Auth::guest())
        <div class="side-nav">
          <ul class="nav flex-column">
            <li class="nav-item first">
              <a class="nav-link active side-menu" href="{{url('/')}}"><i class="fa fa-bar-chart-o"></i> Dashboard</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active side-menu" href="{{url('/customer')}}"><i class="fa fa-users"></i> Manage Customers</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active side-menu" href="{{url('/certificate')}}"><i class="fa fa-file-text-o"></i> Manage Certificate</a>
            </li>
            @if(Auth::user()->role === 'admin' || Auth::user()->role === 'account')
            <li class="nav-item">
              <a class="nav-link active side-menu" href="{{url('/accounting')}}"><i class="fa fa-sort-numeric-asc"></i> Accounting</a>
            </li>
            @endif
            @if(Auth::user()->role === 'admin')
            <li class="nav-item">
              <a class="nav-link active side-menu" href="{{url('settings')}}"><i class="fa fa-cogs"></i> Settings</a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link active side-menu" href="#"><i class="fa fa-question-circle"></i> Help</a>
            </li>
          </ul>
        </div>
        @endif

        <div class="app">
          @yield('content')
        </div>
  </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
