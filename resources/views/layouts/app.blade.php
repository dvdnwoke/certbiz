<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Certiz') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/print.css') }}" rel="stylesheet">
    <link href="{{ asset('css/media.css') }}" rel="stylesheet">
</head>
<body id="body">

  <div id="app">
    <nav class="navbar navbar-default navbar-fixed-top top-menu">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <img class="navbar-brand logo" src="{{ url('/img/logo.png') }}"/>

                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <!-- <li><a href="{{ route('register') }}">Register</a></li> -->
                        @else
                          <notification></notification>
                            <li class="dropdown">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                  <i class="fa fa-user top-menu-icon"></i>   {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                  <li>
                                      <a href="{{url('user/profile')}}/{{Auth::user()->id}}">
                                        <i class="fa fa-cog"></i>  Account Settings
                                      </a>
                                  </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     <i class="fa fa-power-off"></i>
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @if(!Auth::guest())
        <div class="min-side-nav">
          <ul class="nav flex-column">
            <li class="nav-item first">
              <a class="nav-link active side-menu" href="{{url('/')}}" data-toggle="tooltip" data-placement="right" title="Dashboard" ><i class="fa fa-bar-chart-o"></i></a>
            </li>
            <li class="nav-item">
              <a class="nav-link active side-menu" href="{{url('/customer')}}" data-toggle="tooltip" data-placement="right" title="Manage Customer"><i class="fa fa-users"></i></a>
            </li>
            <li class="nav-item">
              <a class="nav-link active side-menu" href="{{url('/certificate')}}" data-toggle="tooltip" data-placement="right" title="Manage Certificate"><i class="fa fa-file-text-o"></i></a>
            </li>
            @if(Auth::user()->role === 'admin' || Auth::user()->role === 'account')
            <li class="nav-item">
              <a class="nav-link active side-menu" href="{{url('/accounting')}}" data-toggle="tooltip" data-placement="right" title="Accounting"><i class="fa fa-sort-numeric-asc"></i></a>
            </li>
            @endif
            @if(Auth::user()->role === 'admin')
            <li class="nav-item">
              <a class="nav-link active side-menu" href="{{url('settings')}}" data-toggle="tooltip" data-placement="right" title="Settings"><i class="fa fa-cogs"></i></a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link active side-menu" href="#" data-toggle="tooltip" data-placement="right" title="Help"><i class="fa fa-question-circle"></i></a>
            </li>
          </ul>
        </div>
        @endif

        @if(!Auth::guest())
        <div class="side-nav">
          <ul class="nav flex-column">
            <li class="nav-item first">
              <a class="nav-link active side-menu" href="{{url('/')}}"><i class="fa fa-bar-chart-o"></i> Dashboard</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active side-menu" href="{{url('/customer')}}"><i class="fa fa-users"></i> Manage Customers</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active side-menu" href="{{url('/certificate')}}"><i class="fa fa-file-text-o"></i> Manage Certificate</a>
            </li>
            @if(Auth::user()->role === 'admin' || Auth::user()->role === 'account')
            <li class="nav-item">
              <a class="nav-link active side-menu" href="{{url('/accounting')}}"><i class="fa fa-sort-numeric-asc"></i> Accounting</a>
            </li>
            @endif
            @if(Auth::user()->role === 'admin')
            <li class="nav-item">
              <a class="nav-link active side-menu" href="{{url('settings')}}"><i class="fa fa-cogs"></i> Settings</a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link active side-menu" href="#"><i class="fa fa-question-circle"></i> Help</a>
            </li>
          </ul>
        </div>
        @endif

        <div class="app">
          @yield('content')
        </div>
  </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/print.js') }}"></script>
    <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
      });
</script>
</body>
</html>
