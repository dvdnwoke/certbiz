@extends('layouts.app')

@section('content')
  <div class="content container-fluid">

    <div class="panel line-break panel-default">
      <div class="row-min-space">
        @include('include.accounting-links')
      </div>

    </div>

    <div class="panel panel-default">
      <div class="panel-heading heading panel-dark">
        <span>Accounting | <i>Certificates without invoice</i></span>
      </div>
      <div class="panel-body">
        @include('include.invoice-status')
        @if($certificates->count() > 0)
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th>
                    S/N
                  </th>
                  <th>
                    Certificate Group
                  </th>
                  <th>
                    Certificate Number
                  </th>
                  <th>
                    Customer
                  </th>
                  <th>
                    Creator(Staff)
                  </th>
                  <th>
                    Date Created
                  </th>
                  <th>
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($certificates as $certificate)
                  <tr data-href="{{url('certificate/view')}}">
                    <td>
                      {{$certificate->id}}
                    </td>
                    <td>
                      {{$certificate->name}}
                    </td>
                    <td>
                      {{$certificate->certificate_id}}
                    </td>
                    <td>
                      {{$certificate->customer->firstName}},
                      {{$certificate->customer->middleName}}
                      {{$certificate->customer->lastName}}
                    </td>
                    <td>
                      {{$certificate->user->name}}
                    </td>
                    <td>
                      {{$certificate->created_at}}
                    </td>
                    <td class="table-nav">
                      <a class="fa fa-file-o" href="{{url('accounting/invoice/add-invoice')}}/{{$certificate->certificate_id}}" title="Add Invoice"></a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="row">
            <div class="col-md-6 col-md-offset-5">
              {{$certificates->links()}}
            </div>
          </div>
        @else
          <div class="alert alert-warning">
            <p>
              <i class="fa fa-quote-left"></i>
              No certificate without invoice exist yet,  <a href="{{url('/certificate/select-user-cert')}}">try creating one</a>
            </p>
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
