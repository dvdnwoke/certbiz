@extends('layouts.app')

@section('content')
<div class="content container-fluid">

  <div class="panel line-break panel-default">
    <div class="row-min-space">
      @include('include.accounting-links')
    </div>

  </div>

  <div class="panel panel-default">
    <div class="panel-heading heading panel-dark">
      <span>Invoice for certificate ID : {{$invoice->certificate->certificate_id}}</span>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">
          @if($invoice->paid)
            <a class="btn btn-primary">PAID</a>
          @else
            <a class="btn btn-danger">UNPAID</a>
          @endif
          <a class="btn pull-right" target="_blank" href="{{url('accounting/invoice/print/')}}/{{$invoice->id}}"><i class="fa fa-print"></i> Print</a>
        </div>
      </div>
      <div class="row">
        <div class="pull-right">
          <div class="col-md-12">
            <h4>Account Details</h4>
            <label>Account Name:</label> {{$bank->account_name}}<br />
            <label>Account Number:</label> {{$bank->account_number}}<br />
            <label>Bank Name:</label> {{$bank->bank_name}}<br />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="pull-left">
          <div class="col-md-12">
            <label>Name:</label> {{$invoice->customer->firstName}},
            {{$invoice->customer->middleName}}
            {{$invoice->customer->lastName}} <br />
            <label>Address:</label> {{$invoice->customer->address}}
            <br />
            <label>Phone:</label> {{$invoice->customer->phone}}
            <br />
            <br />

          </div>
        </div>
      </div>

      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>S/N</th>
              <th>Description</th>
              <th>Quantity</th>
              <th>Unit Price</th>
              <th>Total Price</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>{{$invoice->certificate->name}} with certificate number of {{$invoice->certificate->certificate_id}},
                which expires by {{$invoice->expiry_date}}</td>
              <td>1</td>
              <td>&#8358; {{$invoice->amount}}</td>
              <td>&#8358; {{$invoice->amount}}</td>
            </tr>
            <tr>
              <td>VAT</td>
              <td>{{$setting->value}} %</td>
            </tr>
            <tr>
              <td>Total</td>
              <td>&#8358; {{(($setting->value/100) * $invoice->amount) + $invoice->amount}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
