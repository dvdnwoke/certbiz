@extends('layouts.app')

@section('content')
  <div class="content container-fluid">

    <div class="panel line-break panel-default">
      <div class="row-min-space">
        @include('include.accounting-links')
      </div>

    </div>

    <div class="panel panel-default">
      <div class="panel-heading heading panel-dark">
        <span>Invoice for certificate ID : {{$certificate->certificate_id}}</span>
      </div>
      <div class="panel-body">
        @if(isset($message))
          <div class="alert alert-success">
            {{$message}}
            <br />
            <a href="{{url('accounting/invoice/view')}}/{{$invoice->id}}">View Invocie</a>
          </div>
        @else
        <form class="" action="{{url('accounting/invoice/create')}}" method="post">
          <div class="row">
            <div class="col-md-8 col-md-offset-4">
              <div class="heading">
                Certificate Name : {{$certificate->name}}
              </div>
            </div>
          </div>

          <input name="certificate_name" type="hidden" value="{{$certificate->name}}"/>
          <input name="certificate_id" type="hidden" value="{{$certificate->id}}"/>
          <input name="customer_id" type="hidden" value="{{$certificate->customer_id}}"/>
          {{csrf_field()}}

          <certificate invoice="true" amount="{{($setting) ? $setting->value : 0}}" name="{{$certificate->customer->firstName}}, {{$certificate->customer->middleName}} {{$certificate->customer->lastName}}"></certificate>
          @if($errors->any())
            <div class="alert alert-danger">
              There was some error processing your request please retry..
            </div>
          @endif
          <input class="btn btn-primary" type="submit" value="Create Invoice"/>
        </form>
        @endif
      </div>
    </div>
  </div>
@endsection
