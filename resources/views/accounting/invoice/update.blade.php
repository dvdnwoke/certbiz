@extends('layouts.app')

@section('content')

<div class="content container-fluid">

  <div class="panel line-break panel-default">
    <div class="row-min-space">
      @include('include.accounting-links')
    </div>

  </div>

  @if(isset($updated))
    <div class="alert alert-success">
      Invoice was updated successfuly.
    </div>
  @endif
  @if(isset($deleted))
    <div class="alert alert-success">
      Invoice was deleted successfuly.
    </div>
  @endif

  <div class="panel panel-default">
    <div class="panel-heading heading panel-dark">
      <span>Edit Invoice : {{$invoice->certificate->certificate_id}}</span>
    </div>
    <div class="panel-body">
      <form action="{{url('/accounting/invoice/update')}}" method="post">
        {{csrf_field()}}
        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
              <label>Current Expiry Date: </label> {{$invoice->expiry_date}}
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="pull-right">
              <label>Current Status: </label> {{($invoice->paid) ? 'PAID' : 'UNPAID'}}
            </div>
          </div>
        </div>
        <input type="hidden" name="id" value="{{$invoice->id}}"/>
        <certificate invoice="true" amount="{{$invoice->amount}}" name="{{$invoice->customer->firstName}}, {{$invoice->customer->middleName}} {{$invoice->customer->lastName}}"></certificate>
        <input class="btn btn-primary" type="submit"/>
      </form>

      @if($errors->any())
        <br />
        <div class="alert alert-danger">
          Some fields were left empty, try feeling them.
        </div>
      @endif
    </div>
  </div>
</div>

@endsection
