@extends('layouts.app')

@section('content')
<div class="content container-fluid">

  <div class="panel line-break panel-default">
    <div class="row-min-space">
      @include('include.accounting-links')
    </div>

  </div>

  @if(session('deleted'))
    <div class="alert alert-success">
      Invoice was deleted successfuly.
    </div>
  @endif

  <div class="panel panel-default">
    <div class="panel-heading heading panel-dark">
      <span>
        @if(isset($paid))
          Paid
        @elseif(isset($unpaid))
          Unpaid
        @endif
        Invoices</span>
    </div>
    <div class="panel-body">
      @include('include.invoice-status')
      @if($invoices->count() > 0)
        <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <th>ID</th>
            <th>Certificate</th>
            <th>Certificate ID</th>
            <th>Amount</th>
            <th>Expires At</th>
            <th>Created By</th>
            <th>Create On</th>
            <th>Action</th>
          </thead>
          <tbody>
            @foreach($invoices as $invoice)
            <tr>
              <td>{{$invoice->id}}</td>
              <td>{{$invoice->certificate->name}}</td>
              <td>{{$invoice->certificate->certificate_id}}</td>
              <td>&#8358; {{$invoice->amount}}</td>
              <td>{{$invoice->expiry_date}}</td>
              <td>{{$invoice->user->name}}</td>
              <td>{{$invoice->created_at}}</td>
              <td class="table-nav">
                <a class="fa fa-eye" href="{{url('/accounting/invoice/view')}}/{{$invoice->id}}" title="View Invoice"></a>
                <a class="fa fa-pencil" href="{{url('/accounting/invoice/update')}}/{{$invoice->id}}" title="Edit Invoice"></a>
                <a class="fa fa-times" href="{{url('/accounting/invoice/delete')}}/{{$invoice->id}}" title="Delete Invoice"></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="row">
        <div class="col-md-6 col-md-offset-5">
          {{$invoices->links()}}
        </div>
      </div>
      @else
        <div class="alert alert-success">
          No invoice created yet!
        </div>
      @endif
    </div>
  </div>
</div>
@endsection
