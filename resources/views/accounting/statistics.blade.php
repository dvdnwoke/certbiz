@extends('layouts.app')

@section('content')
<div class="container-fluid content ">
  <div class="row">
      <div class="line-break">
          <div class="panel panel-default">
              <div class="panel-heading heading"><i class="fa fa-bar-chart-o" aria-hidden="true"></i>Accounting Statistics</div>

              <div class="panel-body">
                  <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                      <div class="row">
                        <a class="data col-md-4">
                          <span>{{$paid->count()}}</span> Paid Invoice(s)
                        </a>
                        <a class="data col-md-4">
                          <span>{{$unpaid->count()}}</span> Unpaid Invoice(s)
                        </a>
                        <a class="data col-md-4">
                          <span>{{$total->count()}}</span>All Invoice(s)
                        </a>
                      </div>
                    </div>

                  </div>
                  <account-chart></account-chart>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection
