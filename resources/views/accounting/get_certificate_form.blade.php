@extends('layouts.app')

@section('content')
  <div class="content container-fluid">

    <div class="panel line-break panel-default">
      <div class="row-min-space">
        @include('include.accounting-links')
      </div>

    </div>
    @if(isset($result))
      <div class="alert alert-warning">
        No certificate was found with the Certificate Number of : {{$result}}
      </div>
    @endif

    <div class="panel panel-default">
      <div class="panel-heading heading panel-dark">
        <span>Select certificate</span>
      </div>
      <div class="panel-body">
        <form class="" action="{{url('/accounting/invoice/get-certificate')}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="certificate">Certificate Number</label>
            <input type="text" value="{{(isset($_REQUEST['certificate_id'])) ? $_REQUEST['certificate_id'] : ''}}" class="form-control" name="certificate_id" id="" placeholder="">
            <p class="help-block text text-warning"><i class="fa fa-question-circle"></i> Enter certificate detail which you want to create invoice for.</p>
          </div>

          <div class="form-group">
            <input type="submit" value="Get Certificate" class="btn btn-primary"/>
          </div>

          @if($errors->any())
            <div class="alert alert-danger">
              There was some error processing your request please retry..
              <i class="fa fa-quote-left"> Maybe some field are left empty during invoice creation</i>
            </div>
          @endif
        </form>
      </div>
    </div>
  </div>
@endsection
