@extends('layouts.app')

@section('content')
<div class="content container-fluid">
  <div class="line-break">

  </div>
  <div class="panel panel-default">
    <div class="panel-heading heading">
      <span>User Profile</span>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <h3>User Detail</h3>
          <div class="form-group">
            <label for="">Name:</label>
            {{$user->name}}
          </div>
          <div class="form-group">
            <label for="">Email:</label>
            {{$user->email}}
          </div>
          <div class="form-group">
            <label for="">Phone:</label>
            {{$user->phone}}
          </div>
          <div class="form-group">
            <label for="">Role:</label>
            {{$user->role}}
          </div>
        </div>
        <div class="col-md-6">
          <h3>Change Password</h3>
          @if(isset($updated))
            <div class="alert alert-success">
              Your password has been changed successfully
            </div>
          @endif
          <form action="{{url('user/profile')}}" method="post">
            {{csrf_field()}}
            <input type="hidden" value="{{Auth::user()->id}}" name="user-id"/>
            <div class="form-group">
              <label for="">New Password</label>
              <input type="password" name="password" class="form-control" id="" placeholder="">

            </div>
            <div class="form-group">
              <label for="">Confirm New Password</label>
              <input type="password" name="password_confirmation" class="form-control" id="" placeholder="">

            </div>

            <div class="form-group">

              <input type="submit" class="btn btn-primary" value="Change">

            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
