@component('certificate.component.view', [
	'certificate' => $certificate,
    'certificateName' => $certificateName,
    'certificatePrettyName' => $certificatePrettyName,
    'content' => $content,
    'certificateHeaderName' => $certificateHeaderName,
    'address' => $address,
    'phone' => $phone,
    'alt_phone' => $alt_phone
])
    @slot('description')

    @endslot

    @slot('itemList')
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>TESTS</th>
                    <th>RESULTS</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Ambient Temperature</td>
                        <td>{{ (isset($content['ambient-temperature'])) ? $content['ambient-temperature'] : '' }}</td>
                    </tr>
                    <tr>
                        <td>pH at 26<sup>oc</sup></td>
                        <td>{{ (isset($content['ph'])) ? $content['ph'] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Appearance</td>
                        <td>{{ (isset($content['appearance'])) ? $content['appearance'] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Specific Gravity</td>
                        <td>{{ (isset($content['specific-gravity'])) ? $content['specific-gravity'] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Expansion</td>
                        <td>{{ (isset($content['expansion'])) ? $content['expansion'] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Stability</td>
                        <td>{{ (isset($content['stability'])) ? $content['stability'] : '' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endslot

    @slot('note')
        <br>
        The acceptable ranges for proportioning percentage “found in these standards are:
        <br>
        <div>
            <table class="table" border="1" cellspacing="2">
                <thead>
                <tr>
                    <th>Produced Foam</th>
                    <th>NEPA II</th>
                    <th>BS5306</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1 &percnt;</td>
                    <td>1.0 - 1.3 &percnt;</td>
                    <td>1.0 - 1.25 &percnt;</td>
                </tr>
                <tr>
                    <td>3 &percnt;</td>
                    <td>3.0 - 3.9 &percnt;</td>
                    <td>3.0 - 4.0 &percnt;</td>
                </tr>
                <tr>
                    <td>6 &percnt;</td>
                    <td>6.0 - 7.0 &percnt;</td>
                    <td>6.0 - 7.0 &percnt;</td>
                </tr>
                </tbody>
            </table>
        </div>
        <br/>
        <br/>
        <p>Conclusively, this sample was tested to <b>British standard (BS5306)</b> as 3&percnt; produced foam
            was fond to be in satisfactory condition.
        </p>

        <p>
            <b>Remark</b>:  The tests and results of foam sample showed that the high quality of the foam concentrate
            is sustained. Therefore, the foam sample have been assessed and approved for continuous usage
        </p>

        <br/>
        <div class="row">
            <div class="col-xs-6">
                <p class="notice">This Certificate is valid for 12 months from the above date:
                    Next Test Date:    <b>{{ $certificate->invoice->expiry_date }}</b></p>
            </div>
        </div>
    @endslot

@endcomponent