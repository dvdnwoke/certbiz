@component('certificate.component.create', [
    'certificatePrettyName' => $certificatePrettyName,
    'certificateName' => $certificateName,
    'customer' => $customer
])
    @slot('description')

    @endslot

    @slot('items')
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>TESTS</th>
                    <th>RESULTS</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Ambient Temperature</td>
                    <td><input class="form-control" type="text" name="ambient-temperature"/></td>
                </tr>
                <tr>
                    <td>pH at 26<sup>oc</sup></td>
                    <td><input class="form-control" type="text" name="ph"/></td>
                </tr>
                <tr>
                    <td>Appearance</td>
                    <td><input class="form-control" type="text" appearance/></td>
                </tr>
                <tr>
                    <td>Specific Gravity</td>
                    <td><input class="form-control" type="text" name="specific-gravity"/></td>
                </tr>
                <tr>
                    <td>Expansion</td>
                    <td><input class="form-control" type="text" name="expansion"/></td>
                </tr>
                <tr>
                    <td>Stability</td>
                    <td><input class="form-control" type="text" name="stability"/></td>
                </tr>
                </tbody>
            </table>
        </div>
    @endslot
@endcomponent