@component('certificate.component.edit', [
     'certificate' => $certificate,
     'invoice' => $invoice,
     'invoice_status' =>$invoice_status,
     'content' => $content
])

    @slot('description')

    @endslot

    @slot('items')
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>TESTS</th>
                    <th>RESULTS</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Ambient Temperature</td>
                        <td><input class="form-control" type="text"
                                   value="{{ (isset($content['ambient-temperature'])) ? $content['ambient-temperature'] : '' }}"
                                   name="ambient-temperature"/></td>
                    </tr>
                    <tr>
                        <td>pH at 26<sup>oc</sup></td>
                        <td><input class="form-control" type="text"
                                   value="{{ (isset($content['ph'])) ? $content['ph'] : '' }}"
                                   name="ph"/></td>
                    </tr>
                    <tr>
                        <td>Appearance</td>
                        <td><input class="form-control" type="text"
                                   value="{{ (isset($content['appearance'])) ? $content['appearance'] : '' }}"
                                   name="appearance"/></td>
                    </tr>
                    <tr>
                        <td>Specific Gravity</td>
                        <td><input class="form-control" type="text"
                                   value="{{ (isset($content['specific-gravity'])) ? $content['specific-gravity'] : '' }}"
                                   name="specific-gravity"/></td>
                    </tr>
                    <tr>
                        <td>Expansion</td>
                        <td><input class="form-control" type="text"
                                   value="{{ (isset($content['expansion'])) ? $content['expansion'] : '' }}"
                                   name="expansion"/></td>
                    </tr>
                    <tr>
                        <td>Stability</td>
                        <td><input class="form-control" type="text"
                                   value="{{ (isset($content['stability'])) ? $content['stability'] : '' }}"
                                   name="stability"/></td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endslot
@endcomponent