@component('certificate/component/create', [
    'customer' => $customer,
    'errors' => $errors,
    'certificateName' => $certificateName,
    'certificatePrettyName' => $certificatePrettyName
    ])

    @slot('description')
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Description Type</th>
                    <th>Description Working Codes</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-6">
                                <p>DCP = Dry Chemical Powder</p>
                                <p>W = Water</p>
                                <p>CO2 = Carbon Dioxide</p>
                            </div>
                            <div class="col-md-6">
                                <p>F = Foam</p>
                                <p>WC = Wet Chemical</p>
                                <p>WM = Water Mist</p>
                                <p>BCF = Halon 1211</p>
                                <p>IM = Immersion Suit</p>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-6">
                                <p>1 = Inspection</p>
                                <p>2 = Body Visually Checked</p>
                                <p>3 = Pressure Test</p>
                                <p>4 = External Maintenance</p>
                            </div>
                            <div class="col-md-6">
                                <p>5 = Label Attached</p>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br />
    @endslot

    @slot('items')
        <h4>IMMERSION SUIT</h4>
        <items :collection="['S/N', 'MAKE', 'SERIAL No.', 'MFD', 'PRESSURE TEST KPA', 'PRESSURE TEST DATE', 'LOCATION', 'WORKING CODES']"></items>
    @endslot
@endcomponent