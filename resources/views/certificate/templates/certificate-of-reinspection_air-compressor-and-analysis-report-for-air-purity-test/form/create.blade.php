@component('certificate.component.create', [
    'certificatePrettyName' => $certificatePrettyName,
    'certificateName' => $certificateName,
    'customer' => $customer
])

    @slot('items')
        <items :name="'items-first'" :collection="['Substance Checked', 'Total Range Of Tube', 'Range Level After Test', 'Acceptable Range', 'None Acceptable Range', 'Remark']"></items>

        <items :name="'items-second'" :collection="['COLOUR CHANGE', 'CO', 'CO2', 'OIL MIST', 'WATER VAPOUR', 'REMARK']"></items>
    @endslot
@endcomponent