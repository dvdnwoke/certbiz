@component('certificate.component.edit', [
    'certificate' => $certificate,
    'invoice' => $invoice,
    'invoice_status' =>$invoice_status,
    'content' => $content
])

    @slot('items')
        <items-update :name="items-first" :content="{{ $content['items-first'] }}" :collection="['Substance Checked', 'Total Range Of Tube', 'Range Level After Test', 'Acceptable Range', 'None Acceptable Range', 'Remark']"></items-update>

        <items-update :name="items-second" :content="{{ $content['items-second'] }}" :collection="['COLOUR CHANGE', 'CO', 'CO2', 'OIL MIST', 'WATER VAPOUR', 'REMARK']"></items-update>
    @endslot
@endcomponent