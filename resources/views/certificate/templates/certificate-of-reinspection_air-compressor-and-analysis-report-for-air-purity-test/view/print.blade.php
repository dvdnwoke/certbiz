@component('certificate.component.print', [
    'certificate' => $certificate,
    'logo' => $logo,
    'content' => $content,
    'address' => $address,
    'phone' => $phone,
    'alt_phone' => $alt_phone,
    'certificateHeaderName' => $certificateHeaderName
])
    @slot('header')
        <div class="row">
            <h3 class="text-center cert-title">{{strtoupper($certificateHeaderName)}}</h3>
        </div>
        <div class="row">
            <p class="text-center notice">
                This is to certify that the Breathing Air Compressor listed below have been inspected and
                tested according to En12021 breathing air standard. The Breathing Air Quality is in good condition
            </p>
        </div>

    @endslot

    @slot('description')

    @endslot

    @slot('itemList')
        <items-list :content="{{ $content['items-first'] }}" :collection="['Substance Checked', 'Total Range Of Tube', 'Range Level After Test', 'Acceptable Range', 'None Acceptable Range', 'Remark']"></items-list>

        <items-list :content="{{ $content['items-second'] }}" :collection="['COLOUR CHANGE', 'CO', 'CO2', 'OIL MIST', 'WATER VAPOUR', 'REMARK']"></items-list>
    @endslot

    @slot('note')
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <p>
                    This is to certify that the Breathing Air Compressor Plant was surveyed and the
                    Triplex Air purification system confirmed in good condition with the quality of Air Confirmed OK
                    The filter cartridge not saturated.
                </p>
                <p>
                    The Compressor is certified OK for use.
                </p>
                <p>
                    THIS CERTIFICATE IS VALID FOR FIVE YEAR FROM THE ABOVE DATE
                </p>

                <p>
                    This Certificate is valid for 12 months from the above date:
                    Next Inspection: <strong>
                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->month }},

                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->year }}
                    </strong>
                </p>
            </div>
        </div>
    @endslot
@endcomponent