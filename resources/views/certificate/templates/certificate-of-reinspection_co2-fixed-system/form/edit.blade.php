@component('certificate/component/edit', [
        'certificate' => $certificate,
        'invoice' => $invoice,
        'invoice_status' =>$invoice_status,
        'content' => $content
    ])

    @slot('description')
    @endslot

    @slot('items')
        <div class="row">
            <div class="col-md-6">
                <table class="table table-responsive">
                    <tr>
                        <th>N<sup>o</sup></th>
                        <th>DESCRIPTION</th>
                        <th>DETAILS</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Manufacturer</td>
                        <td><input name="manufacturer" value="{{ $content['manufacturer'] }}"
                                   type="text" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Number of main cylinders</td>
                        <td><input value="{{ $content['number-of-main-cylinders'] }}"
                                   name="number-of-main-cylinders" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Main cylinders capacity (each)</td>
                        <td><input value="{{ $content['main-cylinders-capacity-each'] }}"
                                   name="main-cylinders-capacity-each" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Number of pilot cylinders</td>
                        <td><input value="{{ $content['number-of-pilot-cylinders'] }}"
                                   name="number-of-pilot-cylinders" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Pilot cylinders capacity each</td>
                        <td>
                            <input value="{{ $content['pilot-cylinders-capacity-each'] }}"
                                   name="pilot-cylinders-capacity-each" type="text" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Number of distribution lines</td>
                        <td>
                            <input name="number-of-distribution-lines"
                                   value="{{ $content['number-of-distribution-lines'] }}"
                                   type="text" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Oldest cylinder pressure test date</td>
                        <td>
                            <input name="oldest-cylinder-pressure-test-date"
                                   value="{{ $content['oldest-cylinder-pressure-test-date'] }}"
                                   type="text" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Protected space (s)</td>
                        <td>
                            <input name="protected-spaces"
                                   value="{{ $content['protected-spaces'] }}"
                                   type="text" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Date flexible hoses fitted/ renewed</td>
                        <td>
                            <input name="date-flexible-hoses-fitted-renewed"
                                   value="{{ $content['date-flexible-hoses-fitted-renewed'] }}"
                                   type="text" class="form-control"/>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-md-6">
                <table class="table table-responsive">
                    <tr>
                        <td>Annual Inspection</td>
                        <th><input name="annual-inspection" type="checkbox" value="true"
                                    {{ ($content['annual-inspection']) ? 'checked' : '' }}/>
                        </th>
                    </tr>
                    <tr>
                        <td>5 Yearly Inspection</td>
                        <th><input name="5-yearly-inspection" type="checkbox" value="true"
                                    {{ ($content['5-yearly-inspection']) ? 'checked' : '' }}/>
                        </th>
                    </tr>
                    <tr>
                        <td>Partial Inspection/Repairs</td>
                        <th><input name="partial-inspection-repairs" type="checkbox" value="true"
                                    {{ ($content['partial-inspection-repairs']) ? 'checked' : '' }}/>
                        </th>
                    </tr>
                    <tr>
                        <td>Content verification</td>
                        <th><input name="content-verification" type="checkbox" value="true"
                                    {{ ($content['content-verification']) ? 'checked' : '' }}/>
                        </th>
                    </tr>
                    <tr>
                        <td>Tem. reading</td>
                        <th><input name="tem.-reading" type="text"
                                   value="{{ $content['tem.-reading'] }}"/>
                        </th>
                    </tr>
                </table>
                <div class="form-group">
                    <label class="">
                        Type Of Survey
                        <input class="form-control" type="text" name="type-of-survey"
                            value="{{ (isset($content['type-of-survey'])) ? $content['type-of-survey'] : ''}}"/>
                    </label>
                </div>
            </div>

        </div>

        <div class="row">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>N<sup>o</sup></th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Comment</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Release controls and distribution valves secured to prevent accidental discharge</td>
                    <td>
                        <select name="1item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['1item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="1comment" type="text"/></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Contents in cylinders checked by weighing </td>
                    <td>
                        <select name="2item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['2item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="2comment" type="text"/></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Contents in main cylinders checked by ultrasonic level indicator </td>
                    <td>
                        <select name="3item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['3item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="3comment" type="text"/></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Contents of pilot cylinders checked </td>
                    <td>
                        <select name="4item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['4item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="4comment" type="text"/></td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>All cylinder valve visually inspected</td>
                    <td>
                        <select name="5item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['5item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="5comment" type="text"/></td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>All cylinder clamps and connections checked for tightness</td>
                    <td>
                        <select name="6item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['6item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="6comment" type="text"/></td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>Manifold visually inspected</td>
                    <td>
                        <select name="7item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['7item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="7comment" type="text"/></td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>Manifold tested for leakage by applying dry working air</td>
                    <td>
                        <select name="8item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['8item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="8comment" type="text"/></td>
                </tr>
                <tr>
                    <td>9</td>
                    <td>Main valve and distribution valves visually inspected </td>
                    <td>
                        <select name="9item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['10item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="9comment" type="text"/></td>
                </tr>
                <tr>
                    <td>10</td>
                    <td>Main valve and distribution valves tested for operation </td>
                    <td>
                        <select name="10item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['10item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="10comment" type="text"/></td>
                </tr>
                <tr>
                    <td>11</td>
                    <td>Time delay devices tested for correct setting </td>
                    <td>
                        <select name="11item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['11item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="11comment" type="text"/></td>
                </tr>
                <tr>
                    <td>12</td>
                    <td>Remote release system visually inspected </td>
                    <td>
                        <select name="12item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['12item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="12comment" type="text"/></td>
                </tr>
                <tr>
                    <td>13</td>
                    <td>Remote release system tested </td>
                    <td>
                        <select name="13item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['13item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="13comment" type="text"/></td>
                </tr>
                <tr>
                    <td>14</td>
                    <td>Servo tubing/pilot lines pressure tested at maximum working pressure and checked for leakage and blockage </td>
                    <td>
                        <select name="14item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['14item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="14comment" type="text"/></td>
                </tr>
                <tr>
                    <td>15</td>
                    <td>Manual pull cables, pulleys, gang releases tested, serviced and tightened/adjusted as necessary </td>
                    <td>
                        <select name="15item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['15item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="15comment" type="text"/></td>
                </tr>
                <tr>
                    <td>16</td>
                    <td>Release stations visually inspected</td>
                    <td>
                        <select name="16item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['16item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="16comment" type="text"/></td>
                </tr>
                <tr>
                    <td>17</td>
                    <td>Warning alarms (audible/visual) tested</td>
                    <td>
                        <select name="17item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['17item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="17comment" type="text"/></td>
                </tr>
                <tr>
                    <td>18</td>
                    <td>Fan stop tested</td>
                    <td>
                        <select name="18item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['18item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="18comment" type="text"/></td>
                </tr>
                <tr>
                    <td>19</td>
                    <td>10% of cylinders and pilot cylinder (s) pressure tested every 10 years</td>
                    <td>
                        <select name="19item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['19item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="19comment" type="text"/></td>
                </tr>
                <tr>
                    <td>20</td>
                    <td>Distribution lines and nozzles blown through, by applying dry working air</td>
                    <td>
                        <select name="20item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['20item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="20comment" type="text"/></td>
                </tr>
                <tr>
                    <td>21</td>
                    <td>All doors, hinges and lock inspected</td>
                    <td>
                        <select name="21item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['21item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="21comment" type="text"/></td>
                </tr>
                <tr>
                    <td>22</td>
                    <td>All instruction and warning signs on installation inspected</td>
                    <td>
                        <select name="22item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['22item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="22comment" type="text"/></td>
                </tr>
                <tr>
                    <td>23</td>
                    <td>All flexible hoses renewed and check valves in manifold visually inspected every 10 years </td>
                    <td>
                        <select name="23item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['23item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="23comment" type="text"/></td>
                </tr>
                <tr>
                    <td>24</td>
                    <td>Release controls and distribution valves reconnected and system put back in service</td>
                    <td>
                        <select name="24item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['24item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="24comment" type="text"/></td>
                </tr>
                <tr>
                    <td>25</td>
                    <td>Inspection tags attached.</td>
                    <td>
                        <select name="25item" >
                            <optgroup label="Old Value">
                                <option value="Done" selected="true">{{ $content['25item'] }}</option>
                            </optgroup>
                            <optgroup label="Change To">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </optgroup>
                        </select>
                    </td>
                    <td><input name="25comment" type="text"/></td>
                </tr>
                </tbody>
            </table>
        </div>
    @endslot
@endcomponent