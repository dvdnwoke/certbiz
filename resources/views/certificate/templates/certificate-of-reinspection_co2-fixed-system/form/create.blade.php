@component('certificate/component/create', [
    'customer' => $customer,
    'errors' => $errors,
    'certificateName' => $certificateName,
    'certificatePrettyName' => $certificatePrettyName
    ])

    @slot('description')
    @endslot

    @slot('items')
        <div class="row">
            <div class="col-md-6">
                <table class="table table-responsive">
                    <tr>
                        <th>N<sup>o</sup></th>
                        <th>DESCRIPTION</th>
                        <th>DETAILS</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Manufacturer</td>
                        <td><input name="manufacturer" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Number of main cylinders</td>
                        <td><input name="number-of-main-cylinders" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Main cylinders capacity (each)</td>
                        <td><input name="main-cylinders-capacity-each" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Number of pilot cylinders</td>
                        <td><input name="number-of-pilot-cylinders" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Pilot cylinders capacity each</td>
                        <td><input name="pilot-cylinders-capacity-each" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Number of distribution lines</td>
                        <td><input name="number-of-distribution-lines" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Oldest cylinder pressure test date</td>
                        <td><input name="oldest-cylinder-pressure-test-date" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Protected space (s)</td>
                        <td><input name="protected-spaces" type="text" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Date flexible hoses fitted/ renewed</td>
                        <td><input name="date-flexible-hoses-fitted-renewed" type="text" class="form-control"/></td>
                    </tr>
                </table>
            </div>

            <div class="col-md-6">
                <table class="table table-responsive">
                    <tr>
                        <td>Annual Inspection</td>
                        <th><input name="annual-inspection" type="checkbox" value="true"/></th>
                    </tr>
                    <tr>
                        <td>5 Yearly Inspection</td>
                        <th><input name="5-yearly-inspection" type="checkbox" value="true"/></th>
                    </tr>
                    <tr>
                        <td>Partial Inspection/Repairs</td>
                        <th><input name="partial-inspection-repairs" type="checkbox" value="true"/></th>
                    </tr>
                    <tr>
                        <td>Content verification</td>
                        <th><input name="content-verification" type="checkbox" value="true"/></th>
                    </tr>
                    <tr>
                        <td>Tem. reading</td>
                        <th><input name="tem.-reading" type="text" value="true"/></th>
                    </tr>
                </table>
                <div class="form-group">
                    <label class="">
                        Type Of Survey
                        <input class="form-control" type="text" name="type-of-survey"/>
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>N<sup>o</sup></th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Comment</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Release controls and distribution valves secured to prevent accidental discharge</td>
                        <td>
                            <select name="1item" >

                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="1comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Contents in cylinders checked by weighing </td>
                        <td>
                            <select name="2item" >

                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="2comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Contents in main cylinders checked by ultrasonic level indicator </td>
                        <td>
                            <select name="3item" >

                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="3comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Contents of pilot cylinders checked </td>
                        <td>
                            <select name="4item" >

                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="4comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>All cylinder valve visually inspected</td>
                        <td>
                            <select name="5item" >

                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="5comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>All cylinder clamps and connections checked for tightness</td>
                        <td>
                            <select name="6item" >

                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="6comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Manifold visually inspected</td>
                        <td>
                            <select name="7item" >

                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="7comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Manifold tested for leakage by applying dry working air</td>
                        <td>
                            <select name="8item" >

                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="8comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Main valve and distribution valves visually inspected </td>
                        <td>
                            <select name="9item" >

                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="9comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>Main valve and distribution valves tested for operation </td>
                        <td>
                            <select name="10item" >

                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="10comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>Time delay devices tested for correct setting </td>
                        <td>
                            <select name="11item" >

                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="11comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>Remote release system visually inspected </td>
                        <td>
                            <select name="12item">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="12comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td>Remote release system tested </td>
                        <td>
                            <select name="13item">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="13comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td>Servo tubing/pilot lines pressure tested at maximum working pressure and checked for leakage and blockage </td>
                        <td>
                            <select name="14item">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="14comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>15</td>
                        <td>Manual pull cables, pulleys, gang releases tested, serviced and tightened/adjusted as necessary </td>
                        <td>
                            <select name="15item">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="15comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>16</td>
                        <td>Release stations visually inspected</td>
                        <td>
                            <select name="16item">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="16comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>17</td>
                        <td>Warning alarms (audible/visual) tested</td>
                        <td>
                            <select name="17item">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="17comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>18</td>
                        <td>Fan stop tested</td>
                        <td>
                            <select name="18item">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="18comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>19</td>
                        <td>10% of cylinders and pilot cylinder (s) pressure tested every 10 years</td>
                        <td>
                            <select name="19item" >
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="19comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>20</td>
                        <td>Distribution lines and nozzles blown through, by applying dry working air</td>
                        <td>
                            <select name="20item" >
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="20comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>21</td>
                        <td>All doors, hinges and lock inspected</td>
                        <td>
                            <select name="21item" >
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="21comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>22</td>
                        <td>All instruction and warning signs on installation inspected</td>
                        <td>
                            <select name="22item" >
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="22comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>23</td>
                        <td>All flexible hoses renewed and check valves in manifold visually inspected every 10 years </td>
                        <td>
                            <select name="23item">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="23comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>24</td>
                        <td>Release controls and distribution valves reconnected and system put back in service</td>
                        <td>
                            <select name="24item" >
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="24comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>25</td>
                        <td>Inspection tags attached.</td>
                        <td>
                            <select name="25item">
                                <option value="Done">Done</option>
                                <option value="Not Done">Not Done</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </td>
                        <td><input name="25comment" type="text"/></td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endslot
@endcomponent