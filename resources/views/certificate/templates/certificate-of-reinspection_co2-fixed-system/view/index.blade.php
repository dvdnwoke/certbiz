@component('certificate.component.view', [
  'certificate' => $certificate,
  'certificateName' => $certificateName,
  'certificatePrettyName' => $certificatePrettyName,
  'content' => $content,
  'certificateHeaderName' => $certificateHeaderName,
  'address' => $address,
  'phone' => $phone,
  'alt_phone' => $alt_phone
])

    @slot('description')

    @endslot

    @slot('itemList')
        <div class="row">
            <div class="col-md-6">
                <table class="table table-responsive">
                    <tr>
                        <th>N<sup>o</sup></th>
                        <th>DESCRIPTION</th>
                        <th>DETAILS</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Manufacturer</td>
                        <td>{{$content['manufacturer']}}</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Number of main cylinders</td>
                        <td>{{$content['number-of-main-cylinders']}}</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Main cylinders capacity (each)</td>
                        <td>{{$content['main-cylinders-capacity-each']}}</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Number of pilot cylinders</td>
                        <td>{{$content['number-of-pilot-cylinders']}}</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Pilot cylinders capacity each</td>
                        <td>{{$content['pilot-cylinders-capacity-each']}}</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Number of distribution lines</td>
                        <td>{{$content['number-of-distribution-lines']}}</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Oldest cylinder pressure test date</td>
                        <td>{{$content['oldest-cylinder-pressure-test-date']}}</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Protected space (s)</td>
                        <td>{{$content['protected-spaces']}}</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Date flexible hoses fitted/ renewed</td>
                        <td>{{$content['date-flexible-hoses-fitted-renewed']}}</td>
                    </tr>
                </table>
            </div>

            <div class="col-md-6">
                <table class="table table-responsive">
                    <tr>
                        <td>Annual Inspection</td>
                        <td>{!! ($content['annual-inspection'] === 'true') ? '&checkmark;' : '' !!}</td>
                    </tr>
                    <tr>
                        <td>5 Yearly Inspection</td>
                        <td>{!! ($content['5-yearly-inspection'] === 'true') ? '&checkmark;' : '' !!}</td>
                    </tr>
                    <tr>
                        <td>Partial Inspection/Repairs</td>
                        <td>{!! ($content['partial-inspection-repairs'] === 'true') ? '&checkmark;' : '' !!}</td>
                    </tr>
                    <tr>
                        <td>Content verification</td>
                        <td>{!! ($content['content-verification'] === 'true') ? '&checkmark;' : '' !!}</td>
                    </tr>
                    <tr>
                        <td>Tem. reading</td>
                        <td>{!! ($content['tem.-reading'] === 'true') ? '&checkmark;' : '' !!}</td>
                    </tr>
                </table>
                <div class="form-group">
                    <label class="">
                        Type Of Survey:
                        {{ (isset($content['type-of-survey'])) ? $content['type-of-survey'] : ''}}
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>N<sup>o</sup></th>
                    <th>Description</th>
                    <th>Done</th>
                    <th>Not Done</th>
                    <th>N/A</th>
                    <th>Comment</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Release controls and distribution valves secured to prevent accidental discharge</td>
                    <td>{!! ($content['1item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['1item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['1item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['1comment'] }}</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Contents in cylinders checked by weighing </td>
                    <td>{!! ($content['2item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['2item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['2item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['2comment'] }}</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Contents in main cylinders checked by ultrasonic level indicator </td>
                    <td>{!! ($content['3item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['3item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['3item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['3comment'] }}</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Contents of pilot cylinders checked </td>
                    <td>{!! ($content['4item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['4item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['4item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['4comment'] }}</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>All cylinder valve visually inspected</td>
                    <td>{!! ($content['5item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['5item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['5item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['5comment'] }}</td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>All cylinder clamps and connections checked for tightness</td>
                    <td>{!! ($content['6item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['6item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['6item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['6comment'] }}</td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>Manifold visually inspected</td>
                    <td>{!! ($content['7item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['7item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['7item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['7comment'] }}</td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>Manifold tested for leakage by applying dry working air</td>
                    <td>{!! ($content['8item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['8item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['8item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['8comment'] }}</td>
                </tr>
                <tr>
                    <td>9</td>
                    <td>Main valve and distribution valves visually inspected </td>
                    <td>{!! ($content['9item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['9item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['9item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['9comment'] }}</td>
                </tr>
                <tr>
                    <td>10</td>
                    <td>Main valve and distribution valves tested for operation </td>
                    <td>{!! ($content['10item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['10item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['10item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['10comment'] }}</td>
                </tr>
                <tr>
                    <td>11</td>
                    <td>Time delay devices tested for correct setting </td>
                    <td>{!! ($content['11item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['11item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['11item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['11comment'] }}</td>
                </tr>
                <tr>
                    <td>12</td>
                    <td>Remote release system visually inspected </td>
                    <td>{!! ($content['12item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['12item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['12item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['12comment'] }}</td>
                </tr>
                <tr>
                    <td>13</td>
                    <td>Remote release system tested </td>
                    <td>{!! ($content['13item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['13item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['13item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['13comment'] }}</td>
                </tr>
                <tr>
                    <td>14</td>
                    <td>Servo tubing/pilot lines pressure tested at maximum working pressure and checked for leakage and blockage </td>
                    <td>{!! ($content['14item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['14item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['14item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['14comment'] }}</td>
                </tr>
                <tr>
                    <td>15</td>
                    <td>Manual pull cables, pulleys, gang releases tested, serviced and tightened/adjusted as necessary </td>
                    <td>{!! ($content['15item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['15item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['15item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['15comment'] }}</td>
                </tr>
                <tr>
                    <td>16</td>
                    <td>Release stations visually inspected</td>
                    <td>{!! ($content['16item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['16item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['16item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['16comment'] }}</td>
                </tr>
                <tr>
                    <td>17</td>
                    <td>Warning alarms (audible/visual) tested</td>
                    <td>{!! ($content['17item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['17item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['17item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['17comment'] }}</td>
                </tr>
                <tr>
                    <td>18</td>
                    <td>Fan stop tested</td>
                    <td>{!! ($content['18item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['18item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['18item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['18comment'] }}</td>
                </tr>
                <tr>
                    <td>19</td>
                    <td>10&percnt; of cylinders and pilot cylinder (s) pressure tested every 10 years</td>
                    <td>{!! ($content['19item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['19item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['19item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['19comment'] }}</td>
                </tr>
                <tr>
                    <td>20</td>
                    <td>Distribution lines and nozzles blown through, by applying dry working air</td>
                    <td>{!! ($content['20item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['20item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['20item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['20comment'] }}</td>
                </tr>
                <tr>
                    <td>21</td>
                    <td>All doors, hinges and lock inspected</td>
                    <td>{!! ($content['21item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['21item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['21item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['21comment'] }}</td>
                </tr>
                <tr>
                    <td>22</td>
                    <td>All instruction and warning signs on installation inspected</td>
                    <td>{!! ($content['22item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['22item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['22item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['22comment'] }}</td>
                </tr>
                <tr>
                    <td>23</td>
                    <td>All flexible hoses renewed and check valves in manifold visually inspected every 10 years </td>
                    <td>{!! ($content['23item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['23item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['23item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['23comment'] }}</td>
                </tr>
                <tr>
                    <td>24</td>
                    <td>Release controls and distribution valves reconnected and system put back in service</td>
                    <td>{!! ($content['24item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['24item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['24item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['24comment'] }}</td>
                </tr>
                <tr>
                    <td>25</td>
                    <td>Inspection tags attached. </td>
                    <td>{!! ($content['25item'] === 'Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['25item'] === 'Not Done') ? '&checkmark;' : '' !!}</td>
                    <td>{!! ($content['25item'] === 'N/A') ? '&checkmark;' : '' !!}</td>
                    <td>{{ $content['25comment'] }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    @endslot

    @slot('note')
        <br/>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <p>
                    This is to certify that the above mentioned safety equipment,
                    were duly inspected and necessary maintenance made. They are
                    in good condition for operation in any emergency
                </p>
                <p>
                    THIS CERTIFICATE IS VALID FOR ONE YEAR FROM THE ABOVE DATE
                </p>

                <p>
                    NEXT INSPECTION DATE: <strong>
                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->month }},

                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->year }}
                    </strong>
                </p>
            </div>
        </div>
    @endslot
@endcomponent