@component('certificate/component/edit', [
        'certificate' => $certificate,
        'invoice' => $invoice,
        'invoice_status' =>$invoice_status,
        'content' => $content
    ])
    @slot('description')
        <h4 class="text-center">NOVEC 1230 FIXED SYSTEM</h4>
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Description Working Codes</th>
                    <th>Description Working Codes</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <p>1 = Inspection</p>
                                <p>2 = Contents Checked</p>
                                <p>3 = Recharged</p>
                                <p>4 = Hydro Test</p>
                                <p>5 = Valve Renewal</p>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <p>6 = Valve Repair</p>
                                <p>7 = External Maintenance</p>
                                <p>8 = New Cylinder</p>
                                <p>9 = Blowing of lines</p>
                                <p>10 = Label attached</p>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br />
    @endslot

    @slot('items')

        <items-update :name="'items-fm-200'" :content="{{ $content['items-pilot-cylinder'] }}" :collection="['S/N','MAKE','CYL. NO', 'CAPACITY KG', 'TARE WEIGHT KG','LAST HYD. TEST', 'WORKING CODES']"></items-update>

        <h4 class="text-center">PILOT CYLINDER</h4>
        <br />

        <items-update :name="'items-pilot-cylinder'" :content="{{ $content['items-pilot-cylinder'] }}" :collection="['S/N','MAKE','CYL. NO', 'CAPACITY KG', 'TARE WEIGHT KG','LAST HYD. TEST', 'WORKING CODES']"></items-update>
    @endslot
@endcomponent