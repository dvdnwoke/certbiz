@component('certificate.component.view', [
    'certificate' => $certificate,
    'certificateName' => $certificateName,
    'certificatePrettyName' => $certificatePrettyName,
    'content' => $content,
    'certificateHeaderName' => $certificateHeaderName,
    'address' => $address,
    'phone' => $phone,
    'alt_phone' => $alt_phone
])

    @slot('description')
        <h4 class="text-center">NOVEC 1230 FIXED SYSTEM</h4>
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Description Working Codes</th>
                    <th>Description Working Codes</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <p>1 = Inspection</p>
                                <p>2 = Contents Checked</p>
                                <p>3 = Recharged</p>
                                <p>4 = Hydro Test</p>
                                <p>5 = Valve Renewal</p>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <p>6 = Valve Repair</p>
                                <p>7 = External Maintenance</p>
                                <p>8 = New Cylinder</p>
                                <p>9 = Blowing of lines</p>
                                <p>10 = Label attached</p>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br />
    @endslot

    @slot('itemList')

        <items-list :content="{{ $content['items-fm-200'] }}" :collection="['S/N','MAKE','CYL. NO', 'CAPACITY KG', 'TARE WEIGHT KG','LAST HYD. TEST', 'WORKING CODES']"></items-list>

        <h4 class="text-center">PILOT CYLINDER</h4>
        <br />

        <items-list :content="{{ $content['items-pilot-cylinder'] }}" :collection="['S/N','MAKE','CYL. NO', 'CAPACITY KG', 'TARE WEIGHT KG','LAST HYD. TEST', 'WORKING CODES']"></items-list>
    @endslot

    @slot('note')
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <p>
                    This is to certify that the above mentioned safety equipment,
                    were duly inspected and necessary maintenance made. They are
                    in good condition for operation in any emergency.
                </p>
                <p>
                    THIS CERTIFICATE IS VALID FOR ONE YEAR FROM THE ABOVE DATE
                </p>

                <p>
                    NEXT INSPECTION DATE: <strong>
                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->month }},

                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->year }}
                    </strong>
                </p>
            </div>
        </div>
    @endslot
@endcomponent