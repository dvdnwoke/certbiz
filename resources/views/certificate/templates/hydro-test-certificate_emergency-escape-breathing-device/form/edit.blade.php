@component('certificate/component/edit', [
        'certificate' => $certificate,
        'invoice' => $invoice,
        'invoice_status' =>$invoice_status,
        'content' => $content
    ])

    @slot('description')
        <h4 class="text-center">EMERGENCY ESCAPE BREATHING DEVICE (EEBD)</h4>
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Description Type</th>
                    <th>Description Working Codes</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <p>EEBD = EMERGENCY ESCAPE BREATHING DEVICE</p>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-6">
                                <p>1 = Inspected</p>
                                <p>2 = Contents Checked</p>
                                <p>3 = Hydro Tested</p>
                                <p>4 = Refilled</p>
                            </div>
                            <div class="col-md-6">
                                <p>5 = Replaced Mask</p>
                                <p>6 = Valve Replaced</p>
                                <p>7 = External Maintenance</p>
                                <p>8 = New EEBD</p>
                                <p>9 = Label Attached</p>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br />
    @endslot

    @slot('items')

        <items-update :content="{{ $content['items'] }}" :collection="['S/N', 'CYL. NO', 'WATER CAP.', 'WEIGHT, 'CAPACITY', 'TEST PRESSURE', 'LHT', 'WORKING CODES']"></items-update>


    @endslot
@endcomponent