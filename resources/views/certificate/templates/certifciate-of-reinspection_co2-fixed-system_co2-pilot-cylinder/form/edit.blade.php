@component('certificate/component/edit', [
        'certificate' => $certificate,
        'invoice' => $invoice,
        'invoice_status' =>$invoice_status,
        'content' => $content
    ])

    @slot('description')
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Description Of Working Codes</th>
                    <th>Description Of Working Codes</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <p>1. = Inspection</p>
                                <p>2. = Contents checked by weighing</p>
                                <p>3. = Contents checked by Liquid level indicator > 2/3</p>
                                <p>4. = Recharged</p>
                            </div>

                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-6">
                                <p>5.  = Hydro test</p>
                                <p>6.  = Valve renewal</p>
                                <p>7.  =  Valve repair</p>
                                <p>8.  =   External maintenance</p>
                            </div>
                            <div class="col-md-6">
                                <p>9.  =   New cylinder</p>
                                <p>10. =  Blowing of lines</p>
                                <p>11. = Label attached </p>
                                <p>12 = Temp. range 25<sup>oc</sup> - 28<sup>oc</sup></p>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br />
    @endslot

    @slot('items')
        <h4 class="text-center">CO2 FIXED SYSTEM</h4>
        <items-update :name="'items-co2-fixed-system'" :content="{{ $content['items-co2-fixed-system'] }}" :collection="['S/N', 'MAKE', 'CY. NO', 'CAPACITY KG', 'TARE WEIGHT KG', 'TOTAL WEIGHT KG', 'LAST HYD. TEST', 'WORKING CODES']"></items-update>

        <h4 class="text-center">CO2 PILOT CYLINDER</h4>
        <items-update :name="'items-co2-pilot-cylinder'" :content="{{ $content['items-co2-pilot-cylinder'] }}" :collection="['S/N', 'MAKE', 'CY. NO', 'CAPACITY KG', 'TARE WEIGHT KG', 'TOTAL WEIGHT KG', 'LAST HYD. TEST', 'WORKING CODES']"></items-update>

        <br />
    @endslot
@endcomponent