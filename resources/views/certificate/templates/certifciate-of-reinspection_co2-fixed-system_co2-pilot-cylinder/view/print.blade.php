@component('certificate/component/print', [
    'certificate' => $certificate,
    'logo' => $logo,
    'content' => $content,
    'address' => $address,
    'phone' => $phone,
    'alt_phone' => $alt_phone,
    'certificateHeaderName' => $certificateHeaderName
    ])
    @slot('itemList')
        <h4 class="text-center">CO2 FIXED SYSTEM</h4>

        <items-list :content="{{$content['items-co2-fixed-system']}}" :collection="['S/N', 'TYPE', 'CAPACITY', 'CYL S/NO', 'MAKE', 'LHT', 'LOCATION', 'WORKING CODES']"></items-list>

        <h4 class="text-center">CO2 PILOT CYLINDER</h4>

        <items-list :content="{{$content['items-co2-pilot-cylinder']}}" :collection="['S/N', 'TYPE', 'CAPACITY', 'CYL S/NO', 'MAKE', 'LHT', 'LOCATION', 'WORKING CODES']"></items-list>
    @endslot

    @slot('description')
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Description Of Working Codes</th>
                    <th>Description Of Working Codes</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <p>1. = Inspection</p>
                                <p>2. = Contents checked by weighing</p>
                                <p>3. = Contents checked by Liquid level indicator > 2/3</p>
                                <p>4. = Recharged</p>
                            </div>

                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <p>5.  = Hydro test</p>
                                <p>6.  = Valve renewal</p>
                                <p>7.  =  Valve repair</p>
                                <p>8.  =   External maintenance</p>
                            </div>
                            <div class="col-md-6 col-xs-6">
                                <p>9.  =   New cylinder</p>
                                <p>10. =  Blowing of lines</p>
                                <p>11. = Label attached </p>
                                <p>12 = Temp. range 25<sup>oc</sup> - 28<sup>oc</sup></p>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        @endslot

    @slot('note')
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <p>
                    This is to certify that the above mentioned safety equipment,
                    were duly inspected and necessary maintenance made. They are
                    in good condition for operation in any emergency
                </p>
                <p>
                    THIS CERTIFICATE IS VALID FOR ONE YEAR FROM THE ABOVE DATE
                </p>

                <p>
                    NEXT INSPECTION DATE: <strong>
                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->month }},

                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->year }}
                    </strong>
                </p>
            </div>
        </div>
    @endslot
@endcomponent