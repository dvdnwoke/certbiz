@component('certificate/component/edit', [
        'certificate' => $certificate,
        'invoice' => $invoice,
        'invoice_status' =>$invoice_status,
        'content' => $content
    ])

    @slot('description')
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Description Type</th>
                    <th>Description Working Codes</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <p>MO = Medical Oxygen</p>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-6">
                                <p>1 = Inspection</p>
                                <p>2 = Contents Checked</p>
                                <p>3 = Hydro Tested</p>
                                <p>4 = Refilled</p>
                            </div>
                            <div class="col-md-6">
                                <p>5 = Repaired</p>
                                <p>6 = External Maintenance</p>
                                <p>8 = New Medical Oxygen</p>
                                <p>9 = Label Attached</p>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br />
    @endslot

    @slot('items')
        <h4 class="text-center">MEDICAL OXYGEN</h4>
        <items-update :content="{{ $content['items'] }}" :collection="['S/N', 'TYPE', 'CYL. NO', 'MAKE', 'CAPACITY', 'TEST PRESSURE', 'LHT', 'WORKING CODES']"></items-update>

    @endslot
@endcomponent