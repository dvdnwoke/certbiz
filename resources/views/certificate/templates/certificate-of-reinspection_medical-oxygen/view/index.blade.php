@component('certificate.component.view', [
  'certificate' => $certificate,
  'certificateName' => $certificateName,
  'certificatePrettyName' => $certificatePrettyName,
  'content' => $content,
  'certificateHeaderName' => $certificateHeaderName,
  'address' => $address,
  'phone' => $phone,
  'alt_phone' => $alt_phone
])
    @slot('description')
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Description Type</th>
                    <th>Description Working Codes</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <p>MO = Medical Oxygen</p>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-6">
                                <p>1 = Inspection</p>
                                <p>2 = Contents Checked</p>
                                <p>3 = Hydro Tested</p>
                                <p>4 = Refilled</p>
                            </div>
                            <div class="col-md-6">
                                <p>5 = Repaired</p>
                                <p>6 = External Maintenance</p>
                                <p>8 = New Medical Oxygen</p>
                                <p>9 = Label Attached</p>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br />
    @endslot

    @slot('itemList')

        <h4 class="text-center">MEDICAL OXYGEN</h4>

        <items-list :content="{{ $content['items'] }}" :collection="['S/N', 'TYPE', 'CYL. NO', 'MAKE', 'CAPACITY', 'TEST PRESSURE', 'LHT', 'WORKING CODES']"></items-list>

    @endslot

    @slot('note')
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <p>
                    This is to certify that the above mentioned safety equipment,
                    were duly inspected and necessary maintenance made. They are
                    in good condition for operation in any emergency
                </p>
                <p>
                    THIS CERTIFICATE IS VALID FOR ONE YEAR FROM THE ABOVE DATE
                </p>

                <p>
                    NEXT INSPECTION DATE: <strong>
                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->month }},

                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->year }}
                    </strong>
                </p>
            </div>
        </div>
    @endslot
@endcomponent