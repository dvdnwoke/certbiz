@component('certificate/component/print', [
    'certificate' => $certificate,
    'logo' => $logo,
    'content' => $content,
    'address' => $address,
    'phone' => $phone,
    'alt_phone' => $alt_phone,
    'certificateHeaderName' => $certificateHeaderName
    ])
    @slot('itemList')
        <br />
        <h4 class="text-center">PORTABLE FIRE EXTINGUISHERS</h4>
        <br />
        <items-list :content="{{$content['items']}}" :collection="['S/N', 'TYPE', 'CAPACITY', 'CYL S/NO', 'MAKE', 'LHT', 'LOCATION', 'WORKING CODES']"></items-list>
    @endslot

    @slot('note')
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <p>
                    This is to certify that the above mentioned safety equipment,
                    were duly inspected and necessary maintenance made. They are
                    in good condition for operation in any emergency
                </p>
                <p>
                    THIS CERTIFICATE IS VALID FOR ONE YEAR FROM THE ABOVE DATE
                </p>

                <p>
                    NEXT INSPECTION DATE: <strong>
                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->month }},

                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->year }}
                    </strong>
                </p>
            </div>
        </div>
    @endslot
@endcomponent