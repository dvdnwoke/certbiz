@component('certificate/component/create', [
    'customer' => $customer,
    'errors' => $errors,
    'certificateName' => $certificateName,
    'certificatePrettyName' => $certificatePrettyName
    ])

    @slot('items')
        <h4 class="text-center">PORTABLE FIRE EXTINGUISHERS            </h4>
        <br />

        <items :collection="['S/N', 'TYPE', 'CAPACITY', 'CYL S/NO', 'MAKE', 'LHT', 'LOCATION', 'WORKING CODES']"></items>

        <br />
    @endslot
@endcomponent