@component('certificate/component/edit', [
        'certificate' => $certificate,
        'invoice' => $invoice,
        'invoice_status' =>$invoice_status,
        'content' => $content
    ])

    @slot('items')
        <br />
        <h4 class="text-center">PORTABLE FIRE EXTINGUISHERS            </h4>
        <br />

        <items-update :content="{{ $content['items'] }}" :collection="['S/N', 'TYPE', 'CAPACITY', 'CYL S/NO', 'MAKE', 'LHT', 'LOCATION', 'WORKING CODES']"></items-update>
    @endslot
@endcomponent