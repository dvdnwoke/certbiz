@component('certificate.component.view', [
    'certificate' => $certificate,
    'certificateName' => $certificateName,
    'certificatePrettyName' => $certificatePrettyName,
    'content' => $content,
    'certificateHeaderName' => $certificateHeaderName,
    'address' => $address,
    'phone' => $phone,
    'alt_phone' => $alt_phone
])

@endcomponent