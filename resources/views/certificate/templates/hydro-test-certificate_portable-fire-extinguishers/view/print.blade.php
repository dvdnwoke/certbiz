@component('certificate/component/print', [
    'certificate' => $certificate,
    'logo' => $logo,
    'content' => $content,
    'address' => $address,
    'phone' => $phone,
    'alt_phone' => $alt_phone,
    'certificateHeaderName' => $certificateHeaderName
    ])

@endcomponent