@component('certificate/component/create', [
    'customer' => $customer,
    'errors' => $errors,
    'certificateName' => $certificateName,
    'certificatePrettyName' => $certificatePrettyName
    ])

    @slot('description')
        <h4 class="text-center">SELF CONTAINED BREATHING APPARATUS SET (BAS)</h4>
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Description Type</th>
                    <th>Description Working Codes</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <p>SCBA = SELF CONTAINED BREATHING APPARATUS</p>
                                <p>BAS = Breathing Apparatus Set</p>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-6">
                                <p>1 = Inspected</p>
                                <p>2 = Contents Checked</p>
                                <p>3 = Hydro Tested</p>
                                <p>4 = Refilled</p>
                            </div>
                            <div class="col-md-6">
                                <p>5 = Replaced Mask</p>
                                <p>6 = With Mask</p>
                                <p>7 = External Maintenance</p>
                                <p>8 = New SCBA</p>
                                <p>9 = Label Attached</p>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br />
    @endslot

    @slot('items')
        <items :collection="['S/N', 'CYL. NO', 'FACE MASK CHECKED', 'SUPPLY AIR VALVE CHECKED', 'REDUCER UNIT CHECKED', 'BACK BRACKET CHECKED', 'FUNCTION TEST', 'WORKING CODES']"></items>
    @endslot
@endcomponent