@component('certificate.component.view', [
  'certificate' => $certificate,
  'certificateName' => $certificateName,
  'certificatePrettyName' => $certificatePrettyName,
  'content' => $content,
  'certificateHeaderName' => $certificateHeaderName,
  'address' => $address,
  'phone' => $phone,
  'alt_phone' => $alt_phone
])
    @slot('description')
        <h4 class="text-center">SELF CONTAINED BREATHING APPARATUS (SCBA)</h4>
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Description Type</th>
                    <th>Description Working Codes</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <p>SCBA = SELF CONTAINED BREATHING APPARATUS</p>
                                <p>BAS = Breathing Apparatus Set</p>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-md-6">
                                <p>1 = Inspected</p>
                                <p>2 = Contents Checked</p>
                                <p>3 = Hydro Tested</p>
                                <p>4 = Refilled</p>
                            </div>
                            <div class="col-md-6">
                                <p>5 = Replaced Mask</p>
                                <p>6 = Valve Replaced</p>
                                <p>7 = External Maintenance</p>
                                <p>8 = New SCBA</p>
                                <p>9 = Label Attached</p>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <br />
    @endslot

    @slot('itemList')

        <items-list :content="{{ $content['items'] }}" :collection="['S/N', 'CYL. NO', 'WATER CAP.', 'WEIGHT, 'CAPACITY', 'TEST PRESSURE', 'LHT', 'WORKING CODES']"></items-list>

    @endslot

    @slot('note')
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <p>
                    This is to certify that the cylinder listed above,
                    have been subjected to a hydraulic pressure test
                    of 450 bars according to “SOLAS” Regulations.
                    They can be considered safe for a working pressure
                    of 300 bars, provided they are used under the normal
                    recommended conditions
                </p>
                <p>
                    THIS CERTIFICATE IS VALID FOR FIVE YEAR FROM THE ABOVE DATE
                </p>

                <p>
                    NEXT INSPECTION DATE: <strong>
                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->month }},

                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->year }}
                    </strong>
                </p>
            </div>
        </div>
    @endslot
@endcomponent