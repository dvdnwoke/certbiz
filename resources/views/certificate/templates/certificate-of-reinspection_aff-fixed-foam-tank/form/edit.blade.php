@component('certificate.component.edit', [
        'certificate' => $certificate,
        'invoice' => $invoice,
        'invoice_status' =>$invoice_status,
        'content' => $content
    ])

    @slot('description')
        <h4 class="text-center">AFF Fixed Foam Tank</h4>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <h4 class="pull-left">Technical Details</h4>
                <div class="">
                    <table class="table table-bordered ">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Description</th>
                            <th>Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Number of Tanks</td>
                            <td><input type="text" name="number-of-tanks" value="{{ (isset($content['number-of-tanks'])) ? $content['number-of-tanks'] : '' }}"/></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Tank capacity (each)</td>
                            <td><input type="text" name="tank-capacity-each" value="{{ (isset($content['tank-capacity-each'])) ? $content['tank-capacity-each'] : '' }}"/></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Tank serial number(s)</td>
                            <td><input type="text" name="tank-serial-number" value="{{ (isset($content['tank-serial-number'])) ? $content['tank-serial-number'] : '' }}"/></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <br />
            </div>
        </div>

    @endslot

    @slot('items')
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <h4 class="pull-left">Description of Inspection</h4>
            </div>
            <div class="col-md-12">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Comment</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>
                            Foam tank inspected
                        </td>
                        <td>
                            <select name="1item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['1item'])) ? $content['1item'] : '' }}" selected>
                                        {{ (isset($content['1item'])) ? $content['1item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>

                            </select>
                        </td>
                        <td>
                            <input name="1comment"
                                   value="{{ (isset($content['1comment'])) ? $content['1comment'] : '' }}"
                                   type="text"/>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>All monitor checked for vertical and horizontal movement </td>
                        <td>
                            <select name="2item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['2item'])) ? $content['2item'] : '' }}" selected>
                                        {{ (isset($content['2item'])) ? $content['2item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>

                            </select>
                        </td>
                        <td><input name="2comment" type="text"
                                   value="{{ (isset($content['2comment'])) ? $content['2comment'] : '' }}"/>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Pressure/vacuum relief valve inspected </td>
                        <td>
                            <select name="3item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['3item'])) ? $content['3item'] : '' }}" selected>
                                        {{ (isset($content['3item'])) ? $content['3item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="3comment" type="text"
                                   value="{{ (isset($content['3comment'])) ? $content['3comment'] : '' }}"/>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Control panel inspected </td>
                        <td>
                            <select name="4item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['4item'])) ? $content['4item'] : '' }}" selected>
                                        {{ (isset($content['4item'])) ? $content['4item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>

                            </select>
                        </td>
                        <td><input name="4comment" type="text"
                                   value="{{ (isset($content['4comment'])) ? $content['4comment'] : '' }}"/>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>All pipe lines and valves inspected for proper movement</td>
                        <td>
                            <select name="5item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['5item'])) ? $content['5item'] : '' }}" selected>
                                        {{ (isset($content['5item'])) ? $content['5item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="5comment" type="text"
                                   value="{{ (isset($content['5comment'])) ? $content['5comment'] : '' }}"/>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>All foam Nozzles inspected</td>
                        <td>
                            <select name="6item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['6item'])) ? $content['6item'] : '' }}" selected>
                                        {{ (isset($content['6item'])) ? $content['6item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="6comment" type="text"
                                   value="{{ (isset($content['6comment'])) ? $content['6comment'] : '' }}"/></tr>
                    <tr>
                        <td>7</td>
                        <td>All connections checked for tightness</td>
                        <td>
                            <select name="7item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['7item'])) ? $content['7item'] : '' }}" selected>
                                        {{ (isset($content['7item'])) ? $content['7item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="7comment" type="text"
                                   value="{{ (isset($content['7comment'])) ? $content['7comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Lines and nozzles blown through</td>
                        <td>
                            <select name="8item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['8item'])) ? $content['8item'] : '' }}" selected>
                                        {{ (isset($content['8item'])) ? $content['8item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="8comment" type="text"
                                   value="{{ (isset($content['8comment'])) ? $content['8comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Foam concentrate quality checked </td>
                        <td>
                            <select name="9item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['9item'])) ? $content['9item'] : '' }}" selected>
                                        {{ (isset($content['9item'])) ? $content['9item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="9comment" type="text"
                                   value="{{ (isset($content['9comment'])) ? $content['9comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>Foam concentrate quality checked (sample taken for analysis) </td>
                        <td>
                            <select name="10item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['10item'])) ? $content['10item'] : '' }}" selected>
                                        {{ (isset($content['10item'])) ? $content['10item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="10comment" type="text"
                                   value="{{ (isset($content['10comment'])) ? $content['10comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>System flushed/drained </td>
                        <td>
                            <select name="11item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['11item'])) ? $content['11item'] : '' }}" selected>
                                        {{ (isset($content['11item'])) ? $content['11item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="11comment" type="text"
                                   value="{{ (isset($content['11comment'])) ? $content['11comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>All instruction plates on installation inspected </td>
                        <td>
                            <select name="12item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['12item'])) ? $content['12item'] : '' }}" selected>
                                        {{ (isset($content['12item'])) ? $content['12item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="12comment" type="text"
                                   value="{{ (isset($content['12comment'])) ? $content['12comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td>Balanced pressure proportion inspected</td>
                        <td>
                            <select name="13item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['13item'])) ? $content['13item'] : '' }}" selected>
                                        {{ (isset($content['13item'])) ? $content['13item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="13comment" type="text"
                                   value="{{ (isset($content['13comment'])) ? $content['13comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td>Foam pumps incl. Pressure relief valve/orifice inspected and tested. </td>
                        <td>
                            <select name="14item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['14item'])) ? $content['14item'] : '' }}" selected>
                                        {{ (isset($content['14item'])) ? $content['14item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="14comment" type="text"
                                   value="{{ (isset($content['14comment'])) ? $content['14comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>15</td>
                        <td>Main fire water pump and emergency fire pump inspected and tested </td>
                        <td>
                            <select name="15item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['15item'])) ? $content['15item'] : '' }}" selected>
                                        {{ (isset($content['15item'])) ? $content['15item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="15comment" type="text"
                                   value="{{ (isset($content['15comment'])) ? $content['15comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>16</td>
                        <td>All gauge inspected</td>
                        <td>
                            <select name="16item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['16item'])) ? $content['16item'] : '' }}" selected>
                                        {{ (isset($content['16item'])) ? $content['16item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="16comment" type="text"
                                   value="{{ (isset($content['16comment'])) ? $content['16comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>17</td>
                        <td>All distribution valves inspected/tested</td>
                        <td>
                            <select name="17item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['17item'])) ? $content['17item'] : '' }}" selected>
                                        {{ (isset($content['17item'])) ? $content['17item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="17comment" type="text"
                                   value="{{ (isset($content['17comment'])) ? $content['17comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>18</td>
                        <td>Alarm and electrical ventilation shutdown tested</td>
                        <td>
                            <select name="18item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['18item'])) ? $content['18item'] : '' }}" selected>
                                        {{ (isset($content['18item'])) ? $content['18item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="18comment" type="text"
                                   value="{{ (isset($content['18comment'])) ? $content['18comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>19</td>
                        <td>Foam liquid pump function tested</td>
                        <td>
                            <select name="19item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['19item'])) ? $content['19item'] : '' }}" selected>
                                        {{ (isset($content['19item'])) ? $content['19item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="19comment" type="text"
                                   value="{{ (isset($content['19comment'])) ? $content['19comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>20</td>
                        <td>Tank drained, clean and fitted with new foam</td>
                        <td>
                            <select name="20item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['20item'])) ? $content['20item'] : '' }}" selected>
                                        {{ (isset($content['20item'])) ? $content['20item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="20comment" type="text"
                                   value="{{ (isset($content['20comment'])) ? $content['20comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>21</td>
                        <td>All installation operating instruction and details checked</td>
                        <td>
                            <select name="21item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['21item'])) ? $content['21item'] : '' }}" selected>
                                        {{ (isset($content['21item'])) ? $content['21item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="21comment" type="text"
                                   value="{{ (isset($content['21comment'])) ? $content['21comment'] : '' }}"/></tr>
                    </tr>
                    <tr>
                        <td>22</td>
                        <td>System sealed and left in operational order, inspected date labels attached.</td>
                        <td>
                            <select name="22item" >
                                <optgroup label="Old Value">
                                    <option value="{{ (isset($content['22item'])) ? $content['22item'] : '' }}" selected>
                                        {{ (isset($content['22item'])) ? $content['22item'] : '' }}
                                    </option>
                                </optgroup>
                                <optgroup label="Change To">
                                    <option value="DONE">DONE</option>
                                    <option value="NOT DONE">NOT DONE</option>
                                    <option value="N/A">N/A</option>
                                    <option value="NOT VAL">NOT VAL</option>
                                </optgroup>
                            </select>
                        </td>
                        <td><input name="22comment" type="text"
                                   value="{{ (isset($content['22comment'])) ? $content['22comment'] : '' }}"/></tr>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    @endslot
@endcomponent