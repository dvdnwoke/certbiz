@component('certificate.component.create', [
    'customer' => $customer,
    'errors' => $errors,
    'certificateName' => $certificateName,
    'certificatePrettyName' => $certificatePrettyName
    ])

    @slot('description')
        <h4 class="text-center">AFF Fixed Foam Tank</h4>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <h4 class="pull-left">Technical Details</h4>
                <div class="">
                    <table class="table table-bordered ">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Description</th>
                            <th>Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Number of Tanks</td>
                            <td><input type="text" name="number-of-tanks"/></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Tank capacity (each)</td>
                            <td><input type="text" name="tank-capacity-each"/></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Tank serial number(s)</td>
                            <td><input type="text" name="tank-serial-number"/></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <br />
            </div>
        </div>

    @endslot

    @slot('items')
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <h4 class="pull-left">Description of Inspection</h4>
            </div>
            <div class="col-md-12">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Comment</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>
                            Foam tank inspected
                        </td>
                        <td>
                            <select name="1item" >

                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="1comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>All monitor checked for vertical and horizontal movement </td>
                        <td>
                            <select name="2item" >

                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="2comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Pressure/vacuum relief valve inspected </td>
                        <td>
                            <select name="3item" >

                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="3comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Control panel inspected </td>
                        <td>
                            <select name="4item" >

                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="4comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>All pipe lines and valves inspected for proper movement</td>
                        <td>
                            <select name="5item" >

                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="5comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>All foam Nozzles inspected</td>
                        <td>
                            <select name="6item" >

                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="6comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>All connections checked for tightness</td>
                        <td>
                            <select name="7item" >

                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="7comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Lines and nozzles blown through</td>
                        <td>
                            <select name="8item" >

                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="8comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Foam concentrate quality checked </td>
                        <td>
                            <select name="9item" >

                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="9comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>Foam concentrate quality checked (sample taken for analysis) </td>
                        <td>
                            <select name="10item" >

                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="10comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>System flushed/drained </td>
                        <td>
                            <select name="11item" >

                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="11comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>All instruction plates on installation inspected </td>
                        <td>
                            <select name="12item">
                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="12comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td>Balanced pressure proportion inspected</td>
                        <td>
                            <select name="13item">
                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="13comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td>Foam pumps incl. Pressure relief valve/orifice inspected and tested. </td>
                        <td>
                            <select name="14item">
                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="14comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>15</td>
                        <td>Main fire water pump and emergency fire pump inspected and tested </td>
                        <td>
                            <select name="15item">
                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="15comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>16</td>
                        <td>All gauge inspected</td>
                        <td>
                            <select name="16item">
                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="16comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>17</td>
                        <td>All distribution valves inspected/tested</td>
                        <td>
                            <select name="17item">
                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="17comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>18</td>
                        <td>Alarm and electrical ventilation shutdown tested</td>
                        <td>
                            <select name="18item">
                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="18comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>19</td>
                        <td>Foam liquid pump function tested</td>
                        <td>
                            <select name="19item" >
                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="19comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>20</td>
                        <td>Tank drained, clean and fitted with new foam</td>
                        <td>
                            <select name="20item" >
                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="20comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>21</td>
                        <td>All installation operating instruction and details checked</td>
                        <td>
                            <select name="21item" >
                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="21comment" type="text"/></td>
                    </tr>
                    <tr>
                        <td>22</td>
                        <td>System sealed and left in operational order, inspected date labels attached.</td>
                        <td>
                            <select name="22item" >
                                <option value="DONE">DONE</option>
                                <option value="NOT DONE">NOT DONE</option>
                                <option value="N/A">N/A</option>
                                <option value="NOT VAL">NOT VAL</option>
                            </select>
                        </td>
                        <td><input name="22comment" type="text"/></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    @endslot
@endcomponent