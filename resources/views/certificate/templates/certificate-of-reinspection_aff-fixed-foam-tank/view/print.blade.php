@component('certificate/component/print', [
    'certificate' => $certificate,
    'logo' => $logo,
    'content' => $content,
    'address' => $address,
    'phone' => $phone,
    'alt_phone' => $alt_phone,
    'certificateHeaderName' => $certificateHeaderName
    ])

    @slot('description')
        <br/>
        <h4 class="text-center">AFF Fixed Foam Tank</h4>
        <br/>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <h4 class="pull-left">Technical Details</h4>
                <div class="">
                    <table class="print-table my-table">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Description</th>
                            <th>Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Number of Tanks</td>
                            <td>{{ (isset($content['number-of-tanks'])) ? $content['number-of-tanks'] : '' }}</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Tank capacity (each)</td>
                            <td>{{ (isset($content['tank-capacity-each'])) ? $content['tank-capacity-each'] : '' }}</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Tank serial number(s)</td>
                            <td>{{ (isset($content['tank-serial-number'])) ? $content['tank-serial-number'] : '' }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <br />
            </div>
        </div>
    @endslot

    @slot('itemList')
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <h4 class="pull-left">Description of Inspection</h4>
            </div>
            <div class="col-md-12">
                <table class="print-table my-table">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>DESCRIPTION</th>
                        <th>DONE</th>
                        <th>NOT DONE</th>
                        <th>N/A</th>
                        <th>NOT VAL</th>
                        <th>COMMENT</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>
                            Foam tank inspected
                        </td>
                        <td> {!! ($content['1item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['1item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['1item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['1item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['1comment'])) ? $content['1comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>All monitor checked for vertical and horizontal movement </td>
                        <td> {!! ($content['2item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['2item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['2item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['2item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['2comment'])) ? $content['2comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Pressure/vacuum relief valve inspected </td>
                        <td> {!! ($content['3item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['3item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['3item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['3item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['3comment'])) ? $content['3comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Control panel inspected </td>
                        <td> {!! ($content['4item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['4item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['4item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['4item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['4comment'])) ? $content['4comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>All pipe lines and valves inspected for proper movement</td>
                        <td> {!! ($content['5item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['5item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['5item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['5item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['5comment'])) ? $content['5comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>All foam Nozzles inspected</td>
                        <td> {!! ($content['6item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['6item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['6item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['6item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['6comment'])) ? $content['6comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>All connections checked for tightness</td>
                        <td> {!! ($content['7item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['7item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['7item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['7item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['7comment'])) ? $content['7comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Lines and nozzles blown through</td>
                        <td> {!! ($content['8item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['8item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['8item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['8item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['8comment'])) ? $content['8comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Foam concentrate quality checked </td>
                        <td> {!! ($content['9item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['9item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['9item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['9item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['9comment'])) ? $content['9comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>Foam concentrate quality checked (sample taken for analysis) </td>
                        <td> {!! ($content['10item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['10item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['10item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['10item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['10comment'])) ? $content['10comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>System flushed/drained </td>
                        <td> {!! ($content['11item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['11item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['11item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['11item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['11comment'])) ? $content['11comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>All instruction plates on installation inspected </td>
                        <td> {!! ($content['12item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['12item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['12item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['12item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['12comment'])) ? $content['12comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td>Balanced pressure proportion inspected</td>
                        <td> {!! ($content['13item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['13item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['13item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['13item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['13comment'])) ? $content['13comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td>Foam pumps incl. Pressure relief valve/orifice inspected and tested. </td>
                        <td> {!! ($content['14item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['14item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['14item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['14item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['14comment'])) ? $content['14comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>15</td>
                        <td>Main fire water pump and emergency fire pump inspected and tested </td>
                        <td> {!! ($content['15item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['15item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['15item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['15item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['15comment'])) ? $content['15comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>16</td>
                        <td>All gauge inspected</td>
                        <td> {!! ($content['16item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['16item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['16item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['16item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['16comment'])) ? $content['16comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>17</td>
                        <td>All distribution valves inspected/tested</td>
                        <td> {!! ($content['17item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['17item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['17item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['17item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['17comment'])) ? $content['17comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>18</td>
                        <td>Alarm and electrical ventilation shutdown tested</td>
                        <td> {!! ($content['18item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['18item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['18item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['18item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['18comment'])) ? $content['18comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>19</td>
                        <td>Foam liquid pump function tested</td>
                        <td> {!! ($content['19item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['19item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['19item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['19item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['19comment'])) ? $content['19comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>20</td>
                        <td>Tank drained, clean and fitted with new foam</td>
                        <td> {!! ($content['20item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['20item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['20item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['20item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['20comment'])) ? $content['20comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>21</td>
                        <td>All installation operating instruction and details checked</td>
                        <td> {!! ($content['21item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['21item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['21item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['21item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['21comment'])) ? $content['21comment'] : '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>22</td>
                        <td>System sealed and left in operational order, inspected date labels attached.</td>
                        <td> {!! ($content['22item'] === 'DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['22item'] === 'NOT DONE') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['22item'] === 'N/A') ? '&checkmark;' : '' !!} </td>
                        <td> {!! ($content['22item'] === 'NOT VAL') ? '&checkmark;' : '' !!} </td>
                        <td>
                            {{ (isset($content['22comment'])) ? $content['22comment'] : '' }}
                        </td>
                    </tr>
                    </tbody>
                </table>
                <br/>
            </div>
        </div>
    @endslot

    @slot('note')
        <br/>
        <br/>
        <br/>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <p>
                    This is to certify that the above mentioned safety equipment,
                    were duly inspected and necessary maintenance made. They are
                    in good condition for operation in any emergency
                </p>
                <p>
                    THIS CERTIFICATE IS VALID FOR ONE YEAR FROM THE ABOVE DATE
                </p>

                <p>
                    NEXT INSPECTION DATE: <strong>
                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->month }},

                        {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->year }}
                    </strong>
                </p>
            </div>
        </div>
    @endslot

@endcomponent