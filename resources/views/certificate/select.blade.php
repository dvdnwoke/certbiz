@extends('layouts.app')

@section('content')
  <div class="content container-fluid">
    <div class="line-break">

    </div>
    <div class="panel panel-default">
      <div class="panel-heading heading">
        <span>Select Certificate</span>
      </div>

      <div class="panel-body">
          <div class="row">
            <div class="col-md-6">
                @if(isset($certificates))
                <form class="col-md-12" method="get" action="{{url('/certificate/create')}}">
                  {{csrf_field()}}
                  <label>Certificate for:</label> {{$customer->firstName}},
                   {{$customer->middleName}} {{$customer->lastName}}
                  <input name="customer-id" type="hidden" value="{{$customer->id}}"/>
                  <div class="row-space">
                    <div class="form-group">
                        <select name="certificate-name" id="" class="form-control">
                          <option value="" class="form-control">Select Certificate</option>
                            @foreach($certificates as $certificate)
                              <option class="form-control" value="{{$certificate['fullName']}}">{{$certificate['name']}}</option>
                            @endforeach
                        </select>
                      </div>
                  </div>
                  <div class="row">
                    <button class="btn btn-primary">Select</button>
                  </div>
                </form>
              @else
                No Certificate Template available
              @endif
            </div>
            <div class="col-md-6">
                @include('include.cert-acronym')
            </div>
          </div>
      </div>
    </div>
  </div>
@endsection
