@extends('layouts.app')

@section('content')
  <div class="content container-fluid">
    <div class="line-break">

    </div>
    <div class="panel panel-default">
      <div class="panel-heading heading">
        <span>Create new certificate</span>
      </div>
      <div class="panel-body">
        @if ($errors->has('customerId'))
        <div class="alert alert-danger ">
            No Customer was selected
          </div>
        @endif
        @if ($errors->has('certificate'))
          <div class="alert alert-danger">
            No certificate was selected
          </div>

        @endif
        <div class="pull-right form-inline row-space">
          <label>Filter by</label>
          <select class="form-control" id="searchUserForCertFilter">
            <option value="name">Name</option>
            <option value="phone">Phone</option>
            <option value="email">Email</option>
          </select>
        </div>
        <h4>Search customer</h4>
        <form class="" method="get" action="{{url('/certificate/create')}}">
          {{csrf_field()}}

          @if($customers->count() < 1)
          <div class="row">
            <div class="col-md-12">
              <div class=" alert alert-success">
                No customer created yet, <a href="{{url('customer/create')}}">Create customer</a>
              </div>
            </div>
          </div>

          @else
          <input class="form-control" id="searchUserForCert" name="name" autocomplete="off"/>
          <input  id="searchUserForCertId" name="customer-id" type="hidden"/>
          <ul id="userSearchResult" class="userSearchResult">

          </ul>
          @endif

          @if(isset($certificates))
            <div class="row">
                <div class="col-md-6">
                  <div class="row-space">
                      <label for="">Select Certificate:</label>
                  </div>
                  <div class="form-group">
                      <select name="certificate-name" id="" class="form-control">
                        <option value="" class="form-control">Select</option>
                          @foreach($certificates as $certificate)
                            <option class="form-control" value="{{$certificate['fullName']}}">{{$certificate['name']}}</option>
                          @endforeach
                      </select>
                  </div>
                  <div class="">
                      <button class="btn btn-primary">Proceed</button>
                  </div>
                </div>
                <div class="col-md-6">
                    @include('include.cert-acronym')
                </div>
            </div>
            
          @else
            No Certificate Template available
          @endif

        </form>

      </div>
    </div>
  </div>
@endsection
