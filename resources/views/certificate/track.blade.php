<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Certiz') }} | Check Certificate</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/print.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
</head>
<body>

  <div id="app">
    <div class="container-fluid ">
      <div class="row">
        <img src="{{asset('storage/')}}/{{$logo}}" class="track col-xs-4 col-xs-offset-4"  height="100" alt="Logo"/>
      </div>
      <div class="row">
        <form class="form" method="get" action="{{url('certificate/check')}}">
          <div class="form-group col-xs-6 col-xs-offset-3">
            <input type="text" name="certificate-number" class="form-control" id="" placeholder="Enter Certificate Number">
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-primary" id="" value="Check Certificate">
          </div>
        </form>

      </div>
      @if(isset($certificate))
        @if($certificate)
          <div class="row">
            <div class="container panel panel-default">
              <div class="panel-heading heading">
                Certificate Number: {{$certificate->certificate_id}}
              </div>
              <div class="panel-body">
                <p>This Certificate With The Certificate Number: {{$certificate->certificate_id}} Was Certified By Us</p>
                <div class="form-group">
                  <label for="">Name:</label>
                  {{$certificate->name}}

                </div>
                <div class="form-group">
                  <label for="">Date Created:</label>
                  {{$certificate->created_at}}

                </div>
                <div class="form-group">
                  <label for="">Date Expiring:</label>
                  {{$certificate->invoice->expiry_date}}
                </div>
              </div>
            </div>
          </div>
        @else
        <div class="row">
          <div class="container panel panel-default">
            <div class="panel-heading heading">
              No certificate was found
            </div>
            <div class="panel-body">
              <p>No Certificate With Such Certificate Number Was Found In Our Record</p>
            </div>
          </div>
        </div>
        @endif
      @endif
    </div>

  </div>
  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
