@extends('layouts.app')

@section('content')
  <div class="content container-fluid">
    <div class="top-content">
      @include('include.certificate-search')
    </div>

    @if(session('deleted'))
      <div class="alert alert-success">
        Certificate was deleted successfuly.
      </div>
    @endif
    <div class="panel panel-default">
      <div class="panel-heading heading">
        Certificate(s)
      </div>

      <div class="panel-body">
        @if($certificates->count() > 0)
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>
                  ID
                </th>
                <th>
                  Certificate Group
                </th>
                <th>
                  Certificate Number
                </th>
                <th>
                  Customer
                </th>
                <th>
                  Creator(Staff)
                </th>
                <th>
                  Date Created
                </th>
                <th>
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach($certificates as $certificate)
                <tr data-href="{{url('certificate/view')}}">
                  <td>
                    {{$certificate->id}}
                  </td>
                  <td title="{{$certificate->name}}">
                    @if(strlen($certificate->name) >= 40)
                      {{substr($certificate->name,0,40)}} ...
                    @else
                      {{$certificate->name}}
                    @endif
                  </td>
                  <td>
                    {{$certificate->certificate_id}}
                  </td>
                  <td>
                    {{$certificate->customer->firstName}},
                    {{$certificate->customer->middleName}}
                    {{$certificate->customer->lastName}}
                  </td>
                  <td>
                    {{$certificate->user->name}}
                  </td>
                  <td>
                    {{$certificate->created_at}}
                  </td>
                  <td class="table-nav">
                      <a class="fa fa-eye" href="{{url('/certificate/view')}}/{{$certificate->id}}"></a>
                      <a class="fa fa-pencil" href="{{url('/certificate/update')}}/{{$certificate->id}}"></a>
                      <a class="fa fa-times" style="color:red" href="{{url('/certificate/delete')}}/{{$certificate->id}}"></a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-md-6 col-md-offset-5">
            {{$certificates->links()}}
          </div>
        </div>

        @else
        <div class="alert alert-link">
          No certificates created yet <a href="{{url('certificate/select-user-cert')}}">try creating one</a>
        </div>
        @endif
      </div>
    </div>
  </div>
@endsection
