@extends('layouts.app')

@section('content')
  <div class="content container-fluid">
    <div class="line-break">

    </div>
    @if(session('certificate'))
      <div class="alert alert-success">
        Record was updated successfully
      </div>
    @endif

    @if(session('message'))
      <div class="alert alert-success">
        {{session('message')}}
      </div>
    @endif

    <div class="panel panel-default">
      <div class="panel-heading heading">
         Certificate for | <span>{{$certificatePrettyName}}</span>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <a class="btn btn-primary pull-right space" href="{{url('certificate/update/')}}/{{$certificate->id}}"><i class="fa fa-pencil"></i> Edit</a>
            @if($certificate->has_invoice)
              <a class="btn btn-default pull-right space" id="printButton" href="#"><i class="fa fa-print"></i> Print</a>
            @else
              <a class="btn btn-warning pull-right space" href="{{url('accounting/invoice/get-certificate')}}?certificate_id={{$certificate->certificate_id}}"><i class="fa fa-certificate"></i> Certificate has no Invoice, Create</a>
            @endif
          </div>
        </div>
        <div id="printThis">
            <div id="header" style="display: none">
                <div class="row">
                    <table class="print-table my-table-logo">
                      <tr class="my-table-logo">
                        <td class="my-table-logo"><img src="{{url('img/logo2.png')}}" class="col-xs-12 img-logo" height="55" alt=""></td>
                        <td class="my-table-logo"><img src="{{url('img/seasystemlogo.png')}}" class="col-xs-12 img-logo" height="40" alt=""></td>
                      </tr>
                    </table>
                  </div>
                <div class="row">
                    <br/>
                    <br/>
                  <h3 class="solas text-center">SOLAS</h3>
                    <br/>
                  <p class="text-center notice bold">
                      Int l Covention For Safety Of Life at Sea, 1974
                  </p>
                    <br/>
                    <br/>
                </div>
                <div class="row">
                  <h3 class="text-center cert-title bold">{{$certificateHeaderName}}</h3>
                    <br/>
                    <br/>
                </div>
            </div>
          <div id="wrapper">
          <form method="POST" action="{{url('certificate/store')}}">
            <input type="hidden" name="certificate-name" value="{{$certificateName}}"/>
            <input type="hidden" name="customer-id" value="{{$certificate->customer->id}}"/>
            {{csrf_field()}}
              {{--Details--}}
              @if(isset($details))
                {{$details}}
              @else
              <div class="row" id="detailsView">
                <div class="col-md-6 col-xs-6">
                  <div class="form-group">
                    <label for="">Name of ship:</label>
                    {{empty($content['name-of-ship'])? '' : $content['name-of-ship'] }}
                  </div>
                  <div class="form-group">
                    <label for="">Flag State:</label>
                    {{empty($content['flag'])? '' : $content['flag'] }}
                  </div>
                  <div class="form-group">
                    <label for="">Name of Owner:</label>
                    <a href="{{url('customer/view')}}/{{$certificate->customer->id}}">{{empty($content['name-of-owner'])? '' : $content['name-of-owner'] }}</a>
                  </div>
                  <div class="form-group">
                    <label for="">Port of registry:</label>
                    {{empty($content['port-of-registry'])? '' : $content['port-of-registry'] }}

                  </div>

                  <div class="form-group">
                    <label for="">Certificate Number:</label>
                    {{$certificate->certificate_id}}
                  </div>

                </div>
                <div class="col-md-6 col-xs-6">
                  <div class="form-group">
                    <label for="">Class:</label>
                    {{empty($content['class'])? '' : $content['class'] }}
                  </div>
                  <div class="form-group">
                    <label for="">IMO No.:</label>
                    {{empty($content['imo'])? '' : $content['imo'] }}
                  </div>
                  <div class="form-group">
                    <label for="">CALL SIGN:</label>
                    {{empty($content['call-sign'])? '' : $content['call-sign'] }}
                  </div>
                  <div class="form-group">
                    <label for="">Place of service:</label>
                    {{empty($content['place-of-service'])? ''  : $content['place-of-service'] }}
                  </div>
                </div>
              </div>
              <div class="">
                <table class="table table-responsive" id="detailsPrint" style="display:none">
                        <tr>
                          <td>
                            <p>Year</p>
                            <br/>
                            {{ \Carbon\Carbon::parse($certificate->created_at)->year }}
                          </td>
                          <td>
                            <p>Month</p>
                            <br/>
                            {{ \Carbon\Carbon::parse($certificate->created_at)->month }}
                          </td>
                          <td>
                            <p>Day</p>
                            <br/>
                            {{ \Carbon\Carbon::parse($certificate->created_at)->day }}
                          </td>
                          <td>
                            <p>Name of ship</p>
                            <br/>
                            {{empty($content['name-of-ship'])? '' : $content['name-of-ship'] }}
                          </td>
                          <td>
                            <p>Classification Society</p>
                            <br/>
                            {{empty($content['certificate-society'])? ' ' : $content['certificate-society'] }}
                          </td>
                          <td>
                            <p>Certificate No</p>
                            <br/>
                            {{$certificate->certificate_id}}
                          </td>
                        </tr>
                  <tr>
                    <td>
                      <p>IMO NO</p>
                      <br/>
                      {{empty($content['imo'])? '' : $content['imo'] }}
                    </td>
                    <td>
                      <p>Flag state</p>
                      <br/>
                      {{empty($content['flag-state'])? '' : $content['flag-state'] }}
                    </td>
                    <td>
                      <p>Call Sign</p>
                      <br/>
                      {{empty($content['call-sign'])? '' : $content['call-sign'] }}
                    </td>
                    <td>
                      <p>Port of registry</p>
                      <br/>
                      {{empty($content['port-of-registry'])? ' ' : $content['port-of-registry'] }}
                    </td>
                    <td>
                      <p>Place of service</p>
                      <br/>
                      {{empty($content['place-of-service'])? ' '  : $content['place-of-service'] }}
                    </td>
                    <td>
                      <p>Name of Owner</p>
                      <br/>
                      {{empty($content['name-of-owner'])? '' : $content['name-of-owner'] }}
                    </td>
                  </tr>
                
                </table>
              </div>
              <br/>
              @endif
              {{--/Details--}}

              {{--Description--}}
              @if(isset($description))
                {{ $description }}
              @else

                <div class="table-responsive">
                  <table class="table table-bordered">
                    <tr>

                      <td>
                        <p>1 = Inspection</p>
                        <p>2 = Contents checked by scaling Liquid level indicator</p>
                        <p>3 = Recharged</p>
                      </td>
                      <td>
                        <p>4 = Hydro test</p>
                        <p>5 = Valve Renewal</p>
                        <p>6 = Valve Repair</p>
                        <p>7 = External Maintenance</p>
                      </td>
                      <td>
                        <p>9 = New cylinder</p>
                        <p>10 = Blowing of lines</p>
                        <p>11 = Label attached</p>
                        <p>12 = Temp. range 25 <sup>oc</sup>-28 <sup>oc</sup></p>
                      </td>
                    </tr>
                  </table>
                </div>
                <br />
              @endif
              {{--/Description--}}

              {{--ItemList--}}
              @if(isset($itemList))
                <br/>
                {{ $itemList }}
              @else
                  <br />
                  <h4 class="text-center">PORTABLE FIRE EXTINGUISHERS</h4>
                  <br />
                <items-list :content="{{$content['items']}}" :collection="['S/N','TYPE','CYL S/NO', 'WATER CAPACITY', 'WEIGHT KG','TEST PRESSURE', 'LHT', 'WORKING CODES']"></items-list>
              @endif
              {{--/ItemList--}}
            <br />
          </form>

          {{--Note--}}
          <div id="showOnPrint" style="display:none">
          @if(isset($note))
            {{ $note }}
          @else
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <p>
                  This is to certify that the cylinders listed above, have been subjected to hydraulic
                  pressure test as stated above according to “SOLAS” Regulations. They are safe for
                  use provided they are used under the normal recommended conditions.
                </p>
                <br/>
                <p>
                  THIS CERTIFICATE IS VALID FOR FIVE YEAR FROM THE ABOVE DATE
                </p>

                <p>
                  NEXT INSPECTION DATE: <strong>
                    {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->month }},

                    {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->year }}
                  </strong>
                </p>
              </div>
            </div>
          @endif
          </div>
        {{--/Note--}}

          </div>
          <div class="print-footer" id="footer" style="display:none">
              <div class="row">
                <div class="col-xs-3 col-xs-offset-9">
                  <br/>
                  <br/>
                  <br/>
                  <br/>
                  <h4 class="line-black"></h4>
                  <p class="signatory text-center">Technical Manager</p>
                  <br/>
                  <br/>
                </div>
              </div>
              <div>
                <div class="row">
                  <table class="print-table my-table-logo">
                    <tr class="my-table-logo">
                      <td class="my-table-logo"> <img src="{{url('img/imo.png')}}" width="100" height="40"/>
                      </td>
                      <td class="my-table-logo"><img src="{{url('img/image001.jpg')}}" width="100" height="40"/>
                      </td>
                      <td class="my-table-logo"><img src="{{url('img/abs.png')}}" width="100" height="40"/></td>
                      <td class="my-table-logo"><img src="{{url('img/beurer.png')}}" width="100" height="40"/>
                      </td>
                      <td class="my-table-logo"><img src="{{url('img/dnv.jpg')}}" width="100" height="40"/>
                      </td>
                      <td class="my-table-logo"><img src="{{url('img/dd.jpg')}}" width="100" height="40"/>
                      </td>
                      <td class="my-table-logo"><img src="  url('img/rina.png')}}" width="100" height="40"/>
                      </td>
                    </tr>
                  </table>
                  <h4 class="line"></h4>
                </div>
  
                <div class="row">
                  <div class="col-xs-10 col-xs-offset-1">
                    <br/>
                    <br/>
                    <p class="text-center notice no-margin"><strong class="notice">Head Office: </strong>{{$address}}</p>
                    <br/>
                    <p class="text-center no-margin phone-footer"><strong class="phone-footer">Tel: </strong>{{$phone}}, {{$alt_phone}}</p>
                    <br/>
                    <p class="text-center notice no-margin"><strong class="notice">Email: </strong>seasystemsmarine@yahoo.com,sales@seasystemsmarine.com,www.seasystemsmarine.com</p>
                  </div>
                </div>
              </div>
          </div> 
        </div>
      </div>
    </div>
  </div>
@endsection