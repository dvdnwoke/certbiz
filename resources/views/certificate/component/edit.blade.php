@extends('layouts.app')

@section('content')
  <div class="content container-fluid">
    <div class="line-break">
    </div>

    <!-- Error Display-->
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <!-- Error End-->

    <div class="panel panel-default">
      <div class="panel-heading heading">
         Certificate for | <span>{{ $certificate->name }}</span>
      </div>
      <div class="panel-body">
        <br />
        <form method="POST" action="{{url('certificate/update')}}">
          {{csrf_field()}}
          <input type="hidden" name="certificate-id" value="{{ $certificate->id }}"/>
          <input type="hidden" name="certificate-name" value="{{ $certificate->name }}"/>
          <input type="hidden" name="customer-id" value="{{ $certificate->customer->id }}"/>

          @if($certificate->has_invoice)
            <div class="row">
              <div class="col-md-12">
                <div class="pull-right">
                  <label>Current Invoice Expiry Date:</label> {{ $invoice->expiry_date }}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="pull-right">
                  <label>Current Invoice Status: </label> {{($invoice->paid) ? 'PAID' : 'UNPAID'}}
                </div>
              </div>
            </div>
          @endif
          <div class="col-md-12">
            <certificate invoice="{{$invoice_status}}" amount="{{($certificate->has_invoice) ? $invoice->amount : 0}}" name="{{$certificate->customer->firstName}}, {{$certificate->customer->middleName}} {{$certificate->customer->lastName}}"></certificate>
          </div>

          {{--Details--}}
            @if(isset($details))
              {{ $details }}
            @else
              <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="">Name of ship:</label>
                  <input type="text" name="name-of-ship" value="{{empty($content['name-of-ship'])? '' : $content['name-of-ship'] }}" class="form-control" id="" placeholder="">
                </div>
                <div class="form-group">
                  <label for="">Flag State:</label>
                  <input type="text" name="flag-state" value="{{empty($content['flag-state'])? '' : $content['flag-state'] }}" class="form-control" id="" placeholder="">
                </div>
                <div class="form-group">
                  <label for="">Name of Owner:</label>
                  <input type="text" name="name-of-owner" value="{{$certificate->customer->firstName}}, {{$certificate->customer->middleName}} {{$certificate->customer->lastName}}" class="form-control" disabled/>
                  <input type="hidden" name="name-of-owner" value="{{$certificate->customer->firstName}}, {{$certificate->customer->middleName}} {{$certificate->customer->lastName}}" class="form-control"/>
                </div>

              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="">Port Of Registry:</label>
                  <input type="text" name="port-of-registry" value="{{empty($content['port-of-registry'])? '' : $content['port-of-registry'] }}" class="form-control" id="" placeholder="">
                </div>
                <div class="form-group">

                  <label for="">Place Of Service:</label>

                  <input type="text" name="place-of-service" value="{{empty($content['place-of-service'])? '' : $content['place-of-service'] }}" class="form-control" id="" placeholder="">

                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="">Classification Society:</label>
                  <input type="text" name="classification-society" value="{{empty($content['classification-society'])? '' : $content['classification-society'] }}" class="form-control" id="" placeholder="">
                </div>
                <div class="form-group">
                  <label for="">IMO NO:</label>
                  <input type="text" name="imo-no" value="{{empty($content['imo-no'])? '' : $content['imo-no'] }}" class="form-control" id="" placeholder="">
                </div>

                <div class="form-group">

                  <label for="">CALL SIGN:</label>

                  <input type="text" name="call-sign" value="{{empty($content['call-sign'])? '' : $content['call-sign'] }}" class="form-control" id="" placeholder="">

                </div>


              </div>

            </div>
            @endif
          {{--/Details--}}

          {{--Description--}}
          @if(isset($description))
            {{ $description }}
          @else
            <div class="table-responsive">
              <table class="table table-bordered ">
                <thead>
                <tr>
                  <th>Description Type</th>
                  <th>Description Working Codes</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>
                    <div class="row">
                      <div class="col-md-6">
                        <p>DCP = Dry Chemical Powder</p>
                        <p>W = Water</p>
                        <p>CO2 = Carbon Dioxide</p>
                        <p>W = Water</p>
                      </div>
                      <div class="col-md-6">
                        <p>F = Foam</p>
                        <p>WC = Wet Chemical</p>
                        <p>WM = Water Mist</p>
                        <p>BCF = Halon 1211</p>
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="row">
                      <div class="col-md-6">
                        <p>1 = Inspection</p>
                        <p>2 = Contents Checked</p>
                        <p>3 = Hydrostatic Test</p>
                        <p>4 = Refilled</p>
                      </div>
                      <div class="col-md-6">
                        <p>5 = Valve Replaced</p>
                        <p>6 = External Maintenance</p>
                        <p>8 = New Extinguisher</p>
                        <p>9 = Label Attached</p>
                      </div>
                    </div>
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
            <br />
          @endif
          {{--/Description--}}

          {{--ItemList--}}
            @if(isset($items))
              {{ $items }}
            @else
              <br />
              <h4 class="text-center">PORTABLE FIRE EXTINGUISHERS            </h4>
              <br />

              <items-update :content="{{ $content['items'] }}" :collection="['S/N','TYPE','CYL S/NO', 'WATER CAPACITY', 'WEIGHT KG','TEST PRESSURE', 'LHT', 'WORKING CODES']"></items-update>
            @endif
          {{--/ItemList--}}

            <div class="form-group">
              <div class="col-md-12">
                <button class="btn btn-primary" type="submit">Update</button>
              </div>
            </div>
            <br />
        </form>
      </div>
    </div>
  </div>
@endsection
