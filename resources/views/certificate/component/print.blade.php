@extends('layouts.print')

@section('content')
  <div class="">
    <div class="">
      <div class="page-body">
        <br/>
        <div class="row">
          <table class="print-table my-table-logo">
            <tr class="my-table-logo">
              <td class="my-table-logo"><img src="{{url('img/logo2.png')}}" class="col-xs-12 img-logo" height="55" alt=""></td>
              <td class="my-table-logo"><img src="{{url('img/seasystemlogo.png')}}" class="col-xs-12 img-logo" height="40" alt=""></td>
            </tr>
          </table>
        </div>

        {{--Header--}}
          @if(isset($header))
            {{ $header }}
          @else
            <div class="row">
                <br/>
                <br/>
              <h3 class="solas text-center">SOLAS</h3>
                <br/>
              <p class="text-center notice bold">
                  Int l Covention For Safety Of Life at Sea, 1974
              </p>
                <br/>
                <br/>
            </div>
            <div class="row">
              <h3 class="text-center cert-title bold">{{strtoupper($certificateHeaderName)}}</h3>
                <br/>
                <br/>
            </div>
          @endif
        {{--/Header--}}
        
        {{--Details--}}
          @if(isset($details))
            {{ $details }}
          @else
          <table class="print-table my-table">
            <tr>
              <td colspan="6">
                <p>Year</p>
                <br/>
                {{ \Carbon\Carbon::parse($certificate->created_at)->year }}
              </td>
              <td colspan="6">
                <p>Month</p>
                <br/>
                {{ \Carbon\Carbon::parse($certificate->created_at)->month }}
              </td>
              <td colspan="6">
                <p>Day</p>
                <br/>
                {{ \Carbon\Carbon::parse($certificate->created_at)->day }}
              </td>
              <td colspan="6">
                <p>Name of ship</p>
                <br/>
                {{empty($content['name-of-ship'])? '' : $content['name-of-ship'] }}
              </td>
              <td colspan="6">
                <p>Classification Society</p>
                <br/>
                {{empty($content['certificate-society'])? ' ' : $content['certificate-society'] }}
              </td>
              <td colspan="6">
                <p>Certificate No</p>
                <br/>
                {{$certificate->certificate_id}}
              </td>
            </tr>
            <tr>
              <td colspan="6">
                <p>IMO NO</p>
                <br/>
                {{empty($content['imo'])? '' : $content['imo'] }}
              </td>
              <td colspan="6">
                <p>Flag state</p>
                <br/>
                {{empty($content['flag-state'])? '' : $content['flag-state'] }}
              </td>
              <td colspan="6">
                <p>Call Sign</p>
                <br/>
                {{empty($content['call-sign'])? '' : $content['call-sign'] }}
              </td>
              <td colspan="6">
                <p>Port of registry</p>
                <br/>
                {{empty($content['port-of-registry'])? ' ' : $content['port-of-registry'] }}
              </td>
              <td colspan="6">
                <p>Place of service</p>
                <br/>
                {{empty($content['place-of-service'])? ' '  : $content['place-of-service'] }}
              </td>
              <td colspan="6">
                <p>Name of Owner</p>
                <br/>
                {{empty($content['name-of-owner'])? '' : $content['name-of-owner'] }}
              </td>
            </tr>
          </table>
          @endif
        {{--/Details--}}

        {{--Description--}}
          @if(isset($description))
            {{ $description }}
          @else
            <div class="">
            <table class="print-table my-table">
              <thead>
              <tr>
                <th>Description Type</th>
                <th>Description Working Codes</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>
                  <div class="row">
                    <div class="col-md-6 col-xs-6">
                      <p>DCP = Dry Chemical Powder</p>
                      <p>W = Water</p>
                      <p>CO2 = Carbon Dioxide</p>
                      <p>W = Water</p>
                    </div>
                    <div class="col-md-6 col-xs-6">
                      <p>F = Foam</p>
                      <p>WC = Wet Chemical</p>
                      <p>WM = Weter Mist</p>
                      <p>BCF = Halon 1211</p>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="row">
                    <div class="col-md-6 col-xs-6">
                      <p>1 = Inspection</p>
                      <p>2 = Contents Checked</p>
                      <p>3 = Hydrostatic Test</p>
                      <p>4 = Refilled</p>
                    </div>
                    <div class="col-md-6 col-xs-6">
                      <p>5 = Valve Replaced</p>
                      <p>6 = External Maintenance</p>
                      <p>8 = New Extinguisher</p>
                      <p>9 = Label Attached</p>
                    </div>
                  </div>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
          @endif
        {{--/Description--}}

        {{--ItemList--}}
          @if(isset($itemList))
            {{ $itemList }}
          @else
            <br />
            <h3 class="text-center">PORTABLE FIRE EXTINGUISHERS</h3>
            <br />

            <items-list :content="{{ $content['items'] }}" :collection="['S/N','TYPE','CYL S/NO', 'WATER CAPACITY', 'WEIGHT KG','TEST PRESSURE', 'LHT', 'WORKING CODES']"></items-list>

            <br />
          @endif
        {{--/ItemList--}}

        {{--Note--}}
          @if(isset($note))
            {{ $note }}
          @else
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <p>
                  This is to certify that the cylinders listed above, have been subjected to hydraulic
                  pressure test as stated above according to “SOLAS” Regulations. They are safe for
                  use provided they are used under the normal recommended conditions.
                </p>
                <br/>
                <p>
                  THIS CERTIFICATE IS VALID FOR FIVE YEAR FROM THE ABOVE DATE
                </p>

                <p>
                  NEXT INSPECTION DATE: <strong>
                    {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->month }},

                    {{ \Carbon\Carbon::parse($certificate->invoice->expiry_date)->year }}
                  </strong>
                </p>
              </div>
            </div>
          @endif
        {{--/Note--}}
      </div>
        {{--Footer--}}
        <div class="print-footer">
          @if(isset($footer))
            {{ $footer }}
          @else
            <div class="row">
              <div class="col-xs-3 col-xs-offset-9">
                <br/>
                <br/>
                <br/>
                <br/>
                <h4 class="line-black"></h4>
                <p class="signatory text-center">Technical Manager</p>
                <br/>
                <br/>
              </div>
            </div>
            <div>
              <div class="row">
                <table class="print-table my-table-logo">
                  <tr class="my-table-logo">
                    <td class="my-table-logo"> <img src="{{url('img/imo.png')}}" width="100" height="40"/>
                    </td>
                    <td class="my-table-logo"><img src="{{url('img/image001.jpg')}}" width="100" height="40"/>
                    </td>
                    <td class="my-table-logo"><img src="{{url('img/abs.png')}}" width="100" height="40"/></td>
                    <td class="my-table-logo"><img src="{{url('img/beurer.png')}}" width="100" height="40"/>
                    </td>
                    <td class="my-table-logo"><img src="{{url('img/dnv.jpg')}}" width="100" height="40"/>
                    </td>
                    <td class="my-table-logo"><img src="{{url('img/dd.jpg')}}" width="100" height="40"/>
                    </td>
                    <td class="my-table-logo"><img src="  url('img/rina.png')}}" width="100" height="40"/>
                    </td>
                  </tr>
                </table>
                <h4 class="line"></h4>
              </div>

              <div class="row">

                <div class="col-xs-10 col-xs-offset-1">
                  <br/>
                  <br/>
                  <br/>
                  <br/>
                  <p class="text-center notice no-margin"><strong class="notice">Head Office: </strong>{{$address}}</p>
                  <br/>
                  <p class="text-center no-margin phone-footer"><strong class="phone-footer">Tel: </strong>{{$phone}}, {{$alt_phone}}</p>
                  <br/>
                  <p class="text-center notice no-margin"><strong class="notice">Email: </strong>seasystemsmarine@yahoo.com,sales@seasystemsmarine.com,www.seasystemsmarine.com</p>
                </div>
              </div>
            </div>
          @endif
        </div>  
    </div>
  </div>
@endsection