@extends('layouts.app')

@section('content')
<div class="content container-fluid">
  <div class="line-break">
    @include('include.certificate-search')
    <br />
  </div>
  <div class="alert alert-success">
    Search results for "{{$query}}"
  </div>
  <div class="panel panel-default">
    <div class="panel-heading heading">
      <span>Search result(s)</span>
    </div>
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Certificate Group</th>
              <th>Certificate Number</th>
              <th>Customer Name</th>
              <th>Creator (Staff)</th>
              <th>Created Date</th>
            </tr>
          </thead>
          <tbody>
            @foreach($results as $result)
              <tr>
                <td>{{$result->id}}</td>
                <td>{{$result->name}}</td>
                <td>{{$result->certificate_id}}</td>
                <td>{{$result->customer->firstName}}, {{$result->customer->middleName}} {{$result->customer->lastName}}</td>
                <td>{{$result->user->name}}</td>
                <td>{{$result->created_at}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
