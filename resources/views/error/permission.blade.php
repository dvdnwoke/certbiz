@extends('layouts.error')

@section('content')
<div class="col-md-6 col-md-offset-4">
  <h2><i class="fa fa-cog"></i> Opps Something went wrong</h2>
  <div class="help-block">
    <p>
      Our engineers are on it... But are you suppose to be here?
    </p>
  </div>
  <a href="{{url('/')}}">Go to home page</a>
</div>

@endsection
