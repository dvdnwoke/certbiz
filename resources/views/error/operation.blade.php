@extends('layouts.error')

@section('content')
<div class="col-md-6 col-md-offset-4">
  <h2><i class="fa fa-cog"></i> Opps Something went wrong</h2>
  <div class="help-block">
    <p>
      {{$message}}
    </p>
  </div>
  <a href="{{URL::previous()}}">Go Back</a>
</div>

@endsection
