@extends('layouts.app')

@section('content')

  <form id="deleteCustomerForm" method="post" action="{{url('/customer/delete')}}">
    <input id="deleteCustomerId" type="hidden" name="id" value="" />
    {{ csrf_field() }}
  </form>

  <div class="modal fade" id="customerDeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Are you sure you want to delete this user.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No keep user</button>
          <button type="button" class="btn btn-danger" id="deleteButton">Yes</button>
        </div>
      </div>
    </div>
  </div>

  <div class="content container-fluid">
    @if(isset($delete))
    <div class="alert alert-success">
      {{$message}}
    </div>
    @endif

    @if ($errors->any())
      <div class="alert alert-danger">
        Error processing your request
      </div>
    @endif

    <div class="top-content">
      <form class="form-inline" method="post" action="{{url('/customer/search')}}">
        <input class="form-control" type="search"  name="query" required/>
        <select class="form-control" name="by">
          <option value="name">Search By</option>
          <option value="name">Name</option>
          <option value="email">Email</option>
          <option value="phone">Phone</option>
        </select>
        {{csrf_field()}}
        <button class="btn btn-default">Search</button>
        <a class="btn btn-primary pull-right" href="{{url('customer/create')}}"><i class="fa fa-plus"></i> Create Customer</a>
      </form>
    </div>

    @if($results->count() >= 1)
      <div class="alert alert-info query">
        Search results for: <span>"{{$query}}"</span>
        searched by: <span>{{ucfirst($by)}}</span>
      </div>
    @else
    <div class="alert alert-info query">
      No results found for: <span>"{{$query}}"</span>
      searched by: <span>{{ucfirst($by)}}</span>
    </div>
    @endif
    <div class="panel panel-default main-content">
      <div class="panel-heading heading">
        Customer
      </div>
      <div class="panel-body">

        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>
                  S/N
                </th>
                <th>
                  Name
                </th>
                <th>
                  Phone
                </th>
                <th>
                  Email
                </th>
                <th>
                  Date
                </th>
                <th>
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($results as $result)
              <tr>
                <td>
                  {{$result->id}}
                </td>
                <td>
                  {{$result->firstName}} {{$result->middleName}} {{$result->lastName}}
                </td>
                <td>
                  {{$result->phone}}
                </td>
                <td>
                  {{$result->email}}
                </td>
                <td>
                  {{$result->created_at}}
                </td>
                <td class="table-nav">
                  <ul class="nav navbar-nav">
                    <li class="dropdown">
                      <i class="fa fa-bars dropdown-toggle" aria-hidden="true"data-toggle="dropdown" role="button" aria-expanded="false"></i>

                      <ul class="dropdown-menu" role="menu">
                        <li>
                          <a>Add Certificate</a>
                        </li>
                        <li>
                          <a href="{{url('/customer/view')}}/{{$result->id}}">View</a>
                        </li>
                        <li>
                          <a href="{{url('/customer/update')}}/{{$result->id}}">Edit</a>
                        </li>
                        <li>
                          <a onclick="confirmDeleteCustomer({{$result->id}})">Delete</a>
                        </li>
                      </ul>
                    </li>

                  </ul>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    {{$results->links()}}
  </div>
@endsection
