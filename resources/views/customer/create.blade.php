@extends('layouts.app')

@section('content')
  <div class="content container-fluid">
    <div class="line-break">

    </div>
    <div class="panel panel-default">
      <div class="panel-heading heading">
        <i class="fa fa-plus"></i>
        Create Customer
      </div>
      <div class="panel-body">
        @if(isset($message))
          <div class="alert alert-success">
            {{$message}}
          </div>
        @endif
        <form class="form-inline" method="post" action="{{url('/customer/create')}}">
          {{csrf_field()}}

          <div class="row form-row">
            <div class="col-md-4">
              <div class="input-group">
                <label>First Name:</label>
                <input class="form-control" name="firstName" value="{{ old('firstName')}}"/>
              </div>
            </div>

            <div class="col-md-4">
              <div class="input-group">
                <label>Middle Name:</label>
                <input class="form-control" name="middleName" value="{{ old('middleName')}}"/>
                <small class="text-muted">
                  Optional
                </small>
              </div>
            </div>

            <div class="col-md-4">
              <div class="input-group">
                <label>Last Name:</label>
                <input class="form-control" name="lastName" value="{{ old('lastName')}}"/>
              </div>
            </div>

          </div>

          <div class="row form-row">

            <div class="col-md-4">
              <div class="input-group">
                <label>Phone:</label>
                <input  class="form-control" name="phone" value="{{ old('phone')}}"/>
              </div>
            </div>

            <div class="col-md-4">
              <div class="input-group">
                <label>Email:</label>
                <input  class="form-control" name="email" value="{{ old('email')}}"/>
              </div>
            </div>

            <div class="col-md-4">
              <div class="input-group">
                <label>State:</label>
                <input class="form-control" name="state" value="{{ old('state')}}"/>
              </div>
            </div>


          </div>

          <div class="row form-row">
            <div class="col-md-6">
              <div class="input-group">
                <label>Address:</label>
                <textarea class="form-control form-control-lg" rows="3" name="address">{{ old('address')}}</textarea>
              </div>
            </div>

            <div class="col-md-6">
              @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
              @endif
            </div>
          </div>

          <div class="row form-row">
            <div class="col-md-6">
              <button class="btn btn-primary" type="submit">Create</button>
            </div>



          </div>

        </form>
      </div>
    </div>
  </div>
@endsection
