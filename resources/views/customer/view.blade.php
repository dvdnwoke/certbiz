@extends('layouts.app')
@section('content')
<div class="content container-fluid">
  <div class="line-break">

  </div>
  <div class="panel panel-default main-content">
    <div class="panel-heading heading">
      Customer | <span>{{$customer->firstName}} {{$customer->middleName}}
      {{$customer->lastName}}</span>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">
          <a class="btn btn-success pull-right space" href="{{url('/customer/update')}}/{{$customer->id}}"><i class="fa fa-pencil"></i> Edit Customer</a>
          <a class="btn btn-primary pull-right" href="{{url('/certificate/create/select')}}/{{$customer->id}}"><i class="fa fa-certificate"></i> Add Certificate</a>
        </div>
      </div>

      <div class="row row-space">
        <div class="col-md-4">
          <label>First Name:</label>
          {{$customer->firstName}}
        </div>
        <div class="col-md-4">
          <label>Middle Name:</label>
          {{$customer->middleName}}
        </div>
        <div class="col-md-4">
          <label>Last Name:</label>
          {{$customer->lastName}}
        </div>
      </div>

      <div class="row row-space">
        <div class="col-md-6">
          <label>Address:</label>
          {{$customer->address}}
        </div>
        <div class="col-md-4">
          <label>Phone:</label>
          {{$customer->phone}}
        </div>
        <div class="col-md-2">
          <label>State:</label>
          {{$customer->state}}
        </div>
      </div>

      <div class="row row-space">
        <div class="col-md-6">
          <label>Address:</label>
          {{$customer->email}}
        </div>
      </div>
    </div>
  </div>
</div>

<div class="content container-fluid">
  <div class="panel panel-default">
    <div class="panel-heading heading">
      <span>Customer Certificate(s)</span>
    </div>

    <div class="panel-body">
      @if($has_certificate->count() > 1)
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>
                ID
              </th>
              <th>
                Certificate Group
              </th>
              <th>
                Certificate Number
              </th>
              <th>
                Customer
              </th>
              <th>
                Creator(Staff)
              </th>
              <th>
                Date Created
              </th>
              <th>
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach($has_certificate as $certificate)
              <tr data-href="{{url('certificate/view')}}">
                <td>
                  {{$certificate->id}}
                </td>
                <td>
                  @if(strlen($certificate->name) >= 40)
                    {{substr($certificate->name,0,40)}} ...
                  @else
                    {{$certificate->name}}
                  @endif
                </td>
                <td>
                  {{$certificate->certificate_id}}
                </td>
                <td>
                  {{$certificate->customer->firstName}},
                  {{$certificate->customer->middleName}}
                  {{$certificate->customer->lastName}}
                </td>
                <td>
                  {{$certificate->user->name}}
                </td>
                <td>
                  {{$certificate->created_at}}
                </td>
                <td class="table-nav">
                    <a class="fa fa-eye" href="{{url('/certificate/view')}}/{{$certificate->id}}"></a>
                    <a class="fa fa-pencil" href="{{url('/certificate/update')}}/{{$certificate->id}}"></a>
                    <a class="fa fa-times" style="color:red" href="{{url('/certificate/delete')}}/{{$certificate->id}}"></a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="row">
        <div class="col-md-6 col-md-offset-5">
          {{$has_certificate->links()}}
        </div>
      </div>
      @else
        No certificate for this customer <a href="{{url('/certificate/create/select')}}/{{$customer->id}}">Add certificate</a>
      @endif
    </div>
  </div>
</div>

@endsection
