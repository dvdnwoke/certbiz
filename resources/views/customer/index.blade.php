@extends('layouts.app')

@section('content')

  <form id="deleteCustomerForm" method="post" action="{{url('/customer/delete')}}">
    <input id="deleteCustomerId" type="hidden" name="id" value="" />
    {{ csrf_field() }}
  </form>

  <div class="modal fade" id="customerDeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Are you sure you want to delete this user.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No keep user</button>
          <button type="button" class="btn btn-danger" id="deleteButton">Yes</button>
        </div>
      </div>
    </div>
  </div>

  <div class="content container-fluid">
    @if(isset($delete))
    <div class="alert alert-success">
      {{$message}}
    </div>
    @endif

    @if ($errors->any())
      <div class="alert alert-danger">
        Error processing your request
      </div>
    @endif

    <div class="top-content">
      <form class="form-inline" method="post" action="{{url('/customer/search')}}">
        <input class="form-control" type="search" name="query" required />
        <select class="form-control" name="by">
          <option value="name">Search By</option>
          <option value="name">Name</option>
          <option value="email">Email</option>
          <option value="phone">Phone</option>
        </select>
        {{csrf_field()}}
        <button class="btn btn-default">Search</button>
        <a class="btn btn-primary pull-right" href="{{url('customer/create')}}"><i class="fa fa-plus"></i> Create Customer</a>
      </form>
    </div>
    <div class="panel panel-default main-content">
      <div class="panel-heading heading">
        Customer
      </div>
      <div class="panel-body">
        @if($results->count() > 0)
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>
                  ID
                </th>
                <th>
                  Name
                </th>
                <th>
                  Phone
                </th>
                <th>
                  Email
                </th>
                <th>
                  Date
                </th>
                <th>
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($results as $result)
              <tr>
                <td>
                  {{$result->id}}
                </td>
                <td>
                  {{$result->firstName}} {{$result->middleName}} {{$result->lastName}}
                </td>
                <td>
                  {{$result->phone}}
                </td>
                <td>
                  {{$result->email}}
                </td>
                <td>
                  {{$result->created_at}}
                </td>
                <td class="table-nav">
                  <a class="fa fa-plus" href="{{url('/certificate/create/select')}}/{{$result->id}}" data-toggle="tooltip" title="Add Certificate" data-placement="top"></a>
                  <a class="fa fa-eye" href="{{url('/customer/view')}}/{{$result->id}}" data-toggle="tooltip" title="View Customer" data-placement="top"></a>
                  <a class="fa fa-pencil" href="{{url('/customer/update')}}/{{$result->id}}" data-toggle="tooltip" title="Edit Customer" data-placement="top"></a>
                  <a class="fa fa-times" style="color:red" href="{{url('/customer/delete')}}/{{$result->id}}" data-toggle="tooltip" title="Delete Customer" data-placement="top"></a>

                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-md-6 col-md-offset-5">
            {{$results->links()}}
          </div>
        </div>
        @else
        <div class="alert alert-link">
          No customer yet try registering customer <a href="{{url('customer/create')}}">Create customer</a>
        </div>
        @endif
      </div>
    </div>
    
  </div>
@endsection
