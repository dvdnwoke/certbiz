<div class="alert alert-info row-space">
        <div class="table-responsive acronym">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Acronym</th>
                <th>Meaning</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>PFE</td>
                <td>Portable Fire Extinguisher</td>
              </tr>
              <tr>
                <td>CFS</td>
                <td>CO2 Fixed System</td>
              </tr>
              <tr>
                <td>CPC</td>
                <td>CO2 Pilot Cylinder</td>
              </tr>
              <tr>
                <td>AFFT</td>
                <td>AFF Fixed Foam Tank</td>
              </tr>
              <tr>
                <td>ACAARFAPT</td>
                <td>Air Compressor And Analysis Report For Air Purity Test</td>
              </tr>
              <tr>
                <td>EEBD</td>
                <td>Emergency Escape Breathing Device</td>
              </tr>
              <tr>
                <td>F2</td>
                <td>Fm 200</td>
              </tr>
              <tr>
                <td>IS</td>
                <td>Immersion Suit</td>
              </tr>
              <tr>
                <td>MO</td>
                <td>Medical Oxygen</td>
              </tr>
              <tr>
                <td>N1FS</td>
                <td>Novec 1230 Fixed System</td>
              </tr>
              <tr>
                <td>SCBA</td>
                <td>Self Contained Breathing Apparatus</td>
              </tr>
              <tr>
                <td>SCBAS</td>
                <td>Self Contained Breathing Apparatus Set</td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>