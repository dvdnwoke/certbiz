<div class="settings">
  <div class="setting-box">
    <a  href="{{url('settings/general')}}">
      <div>
        <i class="fa fa-cogs"></i>
      </div>
      General Settings
    </a>
  </div>
  <div class="setting-box">
    <a  href="{{url('/settings/user')}}">
      <div class="icon">
        <i class="fa fa-users"></i>
      </div>
      User Settings
    </a>
  </div>
  <div class="setting-box">
    <a href="{{url('/settings/accounting')}}">
      <div>
        <i class="fa fa-usd"></i>
      </div>
      Accounting Settings
    </a>
  </div>
</div>
