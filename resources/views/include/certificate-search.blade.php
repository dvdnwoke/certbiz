<form class="form-inline" action="{{url('/certificate/search')}}" method="post">
  {{csrf_field()}}
  <input class="form-control" name="query" type="search" />
  <select class="form-control" name="type">
    <option value="">Search By</option>
    <option value="customer-group">Certificate Group</option>
    <option value="certificate-number">Certificate Number</option>
  </select>
  <input type="submit" class="btn btn-default" value="Search"/>
  <a class="btn btn-primary pull-right" href="{{url('certificate/select-user-cert')}}"><i class="fa fa-plus"></i> Create Certificate</a>
</form>
