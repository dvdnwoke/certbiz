<div class="row">
  <div class="col-md-1">
    <a class="btn btn-success" href="{{url('accounting/invoices/unpaid')}}"><i class="fa fa-ban"></i> Unpaid Invoice</a>
  </div>
  <div class="col-md-1 col-md-offset-1">
    <a class="btn btn-primary" href="{{url('accounting/invoices/paid')}}"><i class="fa fa-money"></i> Paid Invoice</a>
  </div>
</div>
