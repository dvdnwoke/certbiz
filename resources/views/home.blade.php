@extends('layouts.app')

@section('content')
  <div class="container-fluid content ">
    <div class="row">
        <div class="line-break">
            <div class="panel panel-default panel-content">
                <div class="panel-heading heading"><i class="fa fa-tachometer" aria-hidden="true"></i>Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                      <div class="col-md-8 col-md-offset-2">
                        <div class="row">
                          <a class="data col-md-4" href="{{url('/customer')}}">
                            <span>{{$num_of_customers}}</span> Customer(s)
                          </a>
                          <a class="data col-md-4" href="{{url('/certificate')}}">
                            <span>{{$num_of_certificates}}</span> Certificate(s)
                          </a>
                          <a class="data col-md-4">
                            <span>{{$num_of_users}}</span> User(s)
                          </a>
                        </div>
                      </div>

                    </div>
                    <dashboard-chart></dashboard-chart>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
