@extends('layouts.app')

@section('content')
  <div class="content container-fluid">

    <div class="panel line-break panel-default">
      <div class="row-min-space">
        @include('include.setting-links')
      </div>
    </div>


    @if(isset($updated))
    <div class="alert alert-success">
      {{$updated}}
    </div>
    @endif
    <div class="panel panel-default">
      <div class="panel-heading heading panel-dark">
        <span>Update Staff</span>
      </div>
      <div class="panel-body">
          <form class="form-horizontal" method="POST" action="{{ url('settings/user/update') }}/{{$staff->id}}">
              {{ csrf_field() }}

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name" class="col-md-4 control-label">Name</label>

                  <div class="col-md-6">
                      <input id="name" type="text" class="form-control" name="name" value="{{ $staff->name }}" required autofocus>

                      @if ($errors->has('name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                  <label for="name" class="col-md-4 control-label">Mobile Number</label>

                  <div class="col-md-6">
                      <input id="name" type="text" class="form-control" name="phone" value="{{ $staff->phone }}"  autofocus>

                      @if ($errors->has('phone'))
                          <span class="help-block">
                              <strong>{{ $errors->first('phone') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                  <label for="name" class="col-md-4 control-label">State of origin</label>

                  <div class="col-md-6">
                      <input id="name" type="text" class="form-control" name="state" value="{{ $staff->state }}">

                      @if ($errors->has('state'))
                          <span class="help-block">
                              <strong>{{ $errors->first('state') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                  <label for="name" class="col-md-4 control-label">Address</label>

                  <div class="col-md-6">
                      <textarea class="form-control" name="address">{{ $staff->address }}</textarea>


                      @if ($errors->has('address'))
                          <span class="help-block">
                              <strong>{{ $errors->first('address') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                  <div class="col-md-6">
                      <input id="email" type="email" class="form-control" name="email" value="{{ $staff->email }}" required>

                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="col-md-4 control-label">Password</label>

                  <div class="col-md-6">
                      <input id="password" type="password" class="form-control" name="password">

                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                  <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                  <div class="col-md-6">
                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                  </div>
              </div>

              <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                  <label for="password-confirm" class="col-md-4 control-label">Account Type</label>

                  <div class="col-md-6">
                    <select class="form-control" name="role">
                      <option value="">Select role</option>
                      <option value="staff">Staff</option>
                      <option value="account">Accounts</option>
                      <option value="admin">Admin</option>
                    </select>
                    <p>
                      Current account type: {{ $staff->role }}
                    </p>

                    @if ($errors->has('role'))
                        <span class="help-block">
                            <strong>{{ $errors->first('role') }}</strong>
                        </span>
                    @endif
                  </div>
              </div>


              <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                      <button type="submit" class="btn btn-primary">
                          Update
                      </button>
                  </div>
              </div>
          </form>
      </div>
    </div>
  </div>
@endsection
