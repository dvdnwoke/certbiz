@extends('layouts.app')

@section('content')
  <div class="content container-fluid">

    <div class="panel line-break panel-default">
      <div class="row-min-space">
        @include('include.setting-links')
      </div>
    </div>

    <div class="panel panel-default line-break">
      <div class="panel-heading heading panel-dark">
        <span>User | {{$staff->name}}</span>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="pull-right">
            <div class="col-md-12">
              <a class="btn btn-primary" href="{{url('settings/user/update')}}/{{$staff->id}}"><i class="fa fa-pencil"></i> Update Staff</a>
            </div>

          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-inline">
              <label for="">Name:</label>
              {{$staff->name}}
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-inline">
              <label for="">Role:</label>
              {{$staff->role}}
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-inline">
              <label for="">Email:</label>
              {{$staff->email}}
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-inline">
              <label for="">Phone:</label>
              {{$staff->phone}}
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-inline">
              <label for="">State of Origin:</label>
              {{$staff->state}}
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-inline">
              <label for="">Address:</label>
              {{$staff->address}}
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-inline">
              <label for="">Created at:</label>
              {{$staff->created_at}}
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-inline">
              <label for="">Last updated:</label>
              {{$staff->updated_at}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
