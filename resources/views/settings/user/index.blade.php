@extends('layouts.app')

@section('content')
<div class="content container-fluid">
  <div class="panel line-break panel-default">
    <div class="row-min-space">
      @include('include.setting-links')
    </div>
  </div>
  @if(session('deleted'))
    <div class="alert alert-success">
      User was deleted successfully
    </div>
  @endif
  <div class="panel panel-default">
    <div class="panel-heading heading">
      <span>Staffs</span>
    </div>

    <div class="panel-body">

      <div class="row">
        <div class="pull-right">
          <div class="col-md-12">
            <a class="btn btn-primary" href="{{url('settings/user/create')}}"><i class="fa fa-user-plus"></i> Add Staff</a>
          </div>

        </div>
      </div>
      <div class="table-responsive">
        <table class="table table-hover table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Role</th>
              <th>Date Created</th>
              <th>Date Updated</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($results as $staff)
            <tr>
              <td>{{$staff->name}}</td>
              <td>{{$staff->email}}</td>
              <td>{{$staff->role}}</td>
              <td>{{$staff->created_at}}</td>
              <td>{{$staff->updated_at}}</td>
              <td class="table-nav">
                <a class="fa fa-eye" href="{{url('settings/user/view')}}/{{$staff->id}}" data-toggle="tooltip" title="View User" data-placement="top"></a>
                <a class="fa fa-pencil" href="{{url('settings/user/update')}}/{{$staff->id}}" data-toggle="tooltip" title="Edit User" data-placement="top"></a>
                @if(!($staff->id == 1))
                  <a class="fa fa-times" href="{{url('settings/user/delete')}}/{{$staff->id}}" style="color:red" data-toggle="tooltip" title="Delete User" data-placement="top"></a>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
