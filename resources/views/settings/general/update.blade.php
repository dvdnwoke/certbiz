@extends('layouts.app')

@section('content')
  <div class="content container-fluid">

    <div class="panel line-break panel-default">
      <div class="row-min-space">
        @include('include.setting-links')
      </div>
    </div>

    @if(isset($created))
      <div class="alert alert-success">
        Settings was updated successfully
      </div>
    @endif

    <div class="panel panel-default">
      <div class="panel-heading heading">
        <span>General Settings</span>
      </div>
      <div class="panel-body">
        <form action="{{url('settings/general/update/')}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}}
          <div class="form-group{{ $errors->has('business-name') ? ' has-error' : '' }}">
            <label for="">Bussiness Name</label>
            <input type="text" name="business-name" class="form-control" value="{{$settings['business-name']}}" placeholder="">
            <p class="help-block">Complete business name.</p>
            @if ($errors->has('business-name'))
                <span class="help-block">
                    <strong>{{ $errors->first('business-name') }}</strong>
                </span>
            @endif
          </div>
          <div class="form-group">
            <label for="">Alias</label>
            <input type="text" name="alias" value="{{$settings['alias']}}" class="form-control" id="" placeholder="">
            <p class="help-block">* optional short name for the company</p>
          </div>
          <div class="form-group">
            <label for="">Company Address</label>
            <textarea name="address" value="{{(isset($settings['address'])) ? $settings['address'] : ''}}" class="form-control"></textarea>
          </div>
          <div class="form-group">
            <label for="">Company Phone 1</label>
            <input name="phone1" value="{{(isset($settings['phone1'])) ? $settings['phone1'] : ''}}" class="form-control"/>
          </div>
          <div class="form-group">
            <label for="">Company Phone 2</label>
            <input name="phone2" value="{{(isset($settings['phone2'])) ? $settings['phone2'] : ''}}" class="form-control"/>
          </div>
          <div class="form-group{{ $errors->has('business-name') ? ' has-error' : '' }}">
            <label for="">Logo</label>
            <input type="file" name="logo" class="form-control" id="" placeholder="">
            <p class="help-block">Logo of the business.</p>
            @if ($errors->has('logo'))
                <span class="help-block">
                    <strong>{{ $errors->first('logo') }}</strong>
                </span>
            @endif
          </div>

          <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Update"/>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
