@extends('layouts.app')

@section('content')
<div class="content container-fluid">
  <div class="line-break">
    <div class="panel panel-default">
      <div class="panel-heading heading">
        <i class="fa fa-cog"></i>
        Settings
      </div>
      <div class="panel-body">
        @include('include.setting-links')
        <div class="row-space col-md-6 col-md-offset-3">
          @if(session('incomplete'))
          <div class=" alert alert-danger line-break">
            <h4>Satisfy the following to use the App</h4>
            <ul>
              @if(session('name'))
              <li>{!!session('name')!!}</li>
              @endif
              @if(session('logo'))
              <li>{!!session('logo')!!}</li>
              @endif
              @if(session('vat'))
              <li>{!!session('vat')!!}</li>
              @endif
              @if(session('bank'))
                <li>{!!session('bank')!!}</li>
              @endif
              @if(session('alias'))
                <li>{!!session('alias')!!}</li>
              @endif
              @if(session('address'))
                <li>{!!session('address')!!}</li>
              @endif

            </ul>
          </div>
          @endif
        </div>
        <div class="row-space col-md-4 col-md-offset-4">
          <div class=" alert alert-warning line-break">
            <p>
              <i class="fa fa-quote-left"></i>
              This dashboard allows you to create and delete Staffs, add settings related to accounts and overall system.
            </p>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection
