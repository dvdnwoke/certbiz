@extends('layouts.app')

@section('content')
  <div class="content container-fluid">

    <div class="panel line-break panel-default">
      <div class="row-min-space">
        @include('include.setting-links')
      </div>
    </div>
    @if(isset($name))
      <div class="alert alert-success">
        {{$name}} price has been changed to &#8358; {{$amount}}
      </div>
    @endif
    @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    @endif

    @if(isset($status))
      @if($status === 'updated')
        <div class="alert alert-success">
          Bank Record was updated
        </div>
      @else
        <div class="alert alert-success">
          Bank Record was created successfuly
        </div>
      @endif
    @endif

    @if(isset($vatStatus))
      @if($vatStatus === 'updated')
        <div class="alert alert-success">
          VAT Record was updated
        </div>
      @else
        <div class="alert alert-success">
          VAT Record was created successfuly
        </div>
      @endif
    @endif


    <div class="panel panel-default">
      <div class="panel-heading heading panel-dark">
        <span>Bank Account Settings</span>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <div class="mod-table table table-hover">
            <div class="thead">
              <div class="td th">Account Name</div>
              <div class="td th">Account Number</div>
              <div class="td th">Bank Name</div>
              <div class="td th">Action</div>
            </div>
            <div class="tbody">
              <form class="tr" action="{{url('settings/accounting/bank-details')}}" method="post">
                {{csrf_field()}}
                <div class="td">
                  <input class="form-control" name="account-name" value="{{isset($bank->account_name) ? $bank->account_name : ''}}" placeholder="Acccount Name"/>
                </div>
                <div class="td">
                  <input class="form-control" name="account-number" value="{{isset($bank->account_number) ? $bank->account_number : ''}}" placeholder="Account Number"/>
                </div>
                <div class="td">
                  <input class="form-control" name="bank-name" value="{{isset($bank->bank_name) ? $bank->bank_name : ''}}" placeholder="Bank Name"/>
                </div>
                <div class="td">
                  <input type="submit" class="btn btn-primary" value="Save"/>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading heading panel-dark">
        <span>VAT Settings</span>
      </div>
      <div class="panel-body">
        <form action="{{url('settings/accounting/vat')}}" method="post">
          {{csrf_field()}}
          <div class="form-group">
            <label for="">VAT:</label>
            <input type="number" name="vat" value="{{isset($vat) ? $vat->value : ''}}" class="form-control" id="" placeholder="">
            <p class="help-block">Enter percentage of VAT. eg 5 or enter 0 for no VAT</p>
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Save">
          </div>
        </form>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading heading panel-dark">
        <span>Certificate Settings</span>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <div class="mod-table table table-striped table-hover">
            <div class="thead">
              <div class="td th">Certificate Name</div>
              <div class="td th">Certificate Price</div>
              <div class="td th">Action</div>
            </div>
            <div class="tbody">
              @foreach($certificates as $certificate)
                <form class="tr" action="{{url('settings/accounting/certificate')}}" method="post" id="{{$certificate['name']}}">
                  {{csrf_field()}}
                  <div class="td">{{$certificate['name']}}</div>
                  <div class="td">
                    <div class="input-group">
                      <div class="input-group-addon">&#8358;</div>
                      <input type="hidden" class="form-inline" value="{{$certificate['name']}}" name="certificate-name" placeholder="Amount">
                      <input type="text" class="form-control-sm" value="{{$certificate['amount']}}" name="amount" placeholder="Amount">
                    </div>
                  </div>
                  <div class="td"><input type="submit" class="btn btn-primary" value="Save" form="{{$certificate['name']}}"/></div>
                </form>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
