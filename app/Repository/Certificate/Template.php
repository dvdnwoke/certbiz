<?php
namespace App\Repository\Certificate;

use Illuminate\Http\Request;
/**
 * Class to manage templates for
 * Certification
 */
class Template {

  /**
   * Path where templates
   * are located
   *
   * @var string
   */
  private $templatePath = 'certificate/templates';

  /**
   * Contains available
   * certificate names
   *
   * @var array
   */
  private $files;

  /**
   * Contains content of
   * the json guard
   *
   * @var string
   */
  private $jsonFile;

  /**
   * Contains rules processed
   * from json guard file for
   * verifying inputs
   *
   * @var string
   */
  private $rules = [];

  /**
   * Contains the current certificate
   * template name been worked on
   *
   * @var string
   */

   private $certificateName;

  public function __construct($certificateName = null)
  {

    if($certificateName != null)
    {
      $this->certificateName = $certificateName;
      $this->getGuard($certificateName);
    }
    $this->scanDir();
  }

  /**
   * Sets the current certificate
   * name
   *
   * @param string
   * @return Object
   */

  public function setCertificateName($certificateName)
  {
    $this->certificateName = strtolower(str_replace(' ', '-', $certificateName));
    return $this;
  }

  /**
   * Gets content submitted a particular
   * Certificate
   *
   * @param Request $request
   * @return string
   */

  public function getData(Request $request)
  {
      $data = array();
      /** @var array $jsonFile */
      $jsonFile = (array) $this->jsonFile;

    foreach ($jsonFile as $fieldName => $value)
    {
    	$field = $request->input($fieldName);

    	$data = array_merge($data, [
    		$fieldName => $field
	    ]);

    }
    return $data;
  }

  /**
   * Returns the current certificate
   * name
   *
   * @param boolean $pretty
   * @return string
   */
  public function getCertificateName($pretty = false)
  {
      if($pretty)
      {
          return $this->createNameArray($this->certificateName)['name'];
      }
      return $this->certificateName;
  }

  /**
   * Returns certificate name
   * for printing header
   *
   * @return string
   */
    public function getCertificateHeaderName()
    {
        $array = explode('_', $this->certificateName);
        return $array[0];
    }
  /**
   * Scans the templates directory
   * for certificate template and
   * stores it.
   *
   * @return void
   */
  private function scanDir()
  {
    $path = base_path().'/resources/views/'. $this->templatePath;
    $this->files = scandir($path);
  }

  /**
   * Returns the path to the json guard
   * of a particular certificate template
   *
   * @param string
   * @return string
   */
  private function getGuardPath ($cert)
  {
    return str_replace(' ', '-', strtolower($cert)).'/policy/index.json';
  }

  /**
   * Gets the guards from the json
   * file
   *
   * @param string
   * @return string
   */
  private function getGuard($cert)
  {
    $path = base_path().'/resources/views/'. $this->templatePath;
    $filePath = $path . '/' . $this->getGuardPath($cert);
    $string = file_get_contents($filePath);
    $this->jsonFile = json_decode($string, true);
    return $this->jsonFile;
  }

  /**
   * Returns certificate templates
   * available on the system
   *
   * @return array
   */
  public function getCertNames ()
  {
    $files = $this->trimDir();
    return $this->prepareCertNames($files);
  }

  /**
   * Takes a word sperated with space
   * or hyphen, then returns its
   * acronym
   *
   * @param string
   * @return String
   */
  private function abbreviate($string)
  {
    $acronym = '';
    $delimiter = ' ';
    if(str_contains('-', $string))
    {
      $delimiter = '-';
    }

    $eachWord = explode($delimiter, $string);
    foreach($eachWord as $word)
    {
      $acronym .= strtoupper(substr($word, 0, 1));
    }

    return $acronym;
  }

    /**
     * Creates Name array for certificate
     * the name is the one for display while
     * full name is according to the name
     * on the filesystem
     *
     * @param string
     * @return array
     */
  private function createNameArray($certificateName)
  {
    $explodedFileName = explode('_', $certificateName);
    $acronym = '';
    $count = count($explodedFileName) - 1;
    for($i = 1; $i <= $count; $i++)
    {
        /** @var object $this */
        $acronym .= $this->abbreviate($explodedFileName[$i]);
      if($i != $count)
      {
        $acronym .= ',';
      }
    }
    return [
      'name' => $explodedFileName[0] . '[' . $acronym . ']',
      'fullName' => $certificateName
    ];
  }

  /**
   * Prepare all the certificate names available for
   * display
   *
   * @param array
   * @return array
   */
  private function prepareCertNames($certificates)
  {
    foreach($certificates as $key => $certificate)
    {
      $certificates[$certificate] = $this->createNameArray($certificate);
      unset($certificates[$key]);
    }
    return $certificates;
  }

  /**
   * Prepares the files, removes hidden
   * files from the list ,converts to
   * upper case
   *
   * @return array
   */
  private function trimDir()
  {
    $files = $this->files;
    foreach ($files as $key => $file)
    {
      if ($file === '.' || $file === '..')
      {
        unset($files[$key]);
        continue;
      }
      $files[$key] = ucwords(str_replace('-', ' ', str_replace('.blade.php','',$files[$key])));
    }
    return array_values($files);
  }

    /**
     * Returns the path of a template
     * for either "Create" or "Edit"
     * as specified through the type
     * param or Create if param
     * is empty
     *
     * @param string $certificate
     * @param string $type
     * @return string
     */
  public function getFormPath($certificate, $type = null)
  {
    $certificate = str_replace(' ', '-', $certificate);
    if($type === 'Create')
    {
      return (String) $this->templatePath .'/'.strtolower($certificate).'/form/create';
    }
    if($type === 'Edit')
    {
      return (String) $this->templatePath .'/'.strtolower($certificate).'/form/edit';
    }
    return (String) $this->templatePath .'/'.strtolower($certificate).'/form/create';
  }

  /**
   * Returns the path for view template
   * of a certifiicate or print template
   *
   * @param string $certificate
   * @param string $type
   * @return String
   */
  public function getViewPath($certificate, $type = null)
  {
    $certificate = str_replace(' ', '-', $certificate);
    if($type === 'Print')
    {
      return (String) $this->templatePath .'/'.strtolower($certificate).'/view/print';
    }
    if($type === 'View' || $type === null)
    {
      return (String) $this->templatePath .'/'.strtolower($certificate).'/view/index';
    }

    return (String) $this->templatePath .'/'.strtolower($certificate).'/view/index';
  }
  /**
   * Returns rules required to validate
   * required fields.
   *
   * @return string
   */

  public function getRule ()
  {
    $this->getGuard($this->certificateName);
    foreach ((array) $this->jsonFile as $key => $value) {
      $this->makeRule($key, $value);
    }

    return $this->rules;

  }

  /**
   * Makes rules from the
   * json content stored in json
   * data.
   *
   * @param string
   * @param array
   */

  private function makeRule ($name, $rule)
  {
    if (isset($rule['required'])) {
      if($rule['required']) {
        $this->rules = array_merge($this->rules,[
          $name => 'required'
        ]);
      }

    }
    if ($rule['type'] === 'date') {
      $this->rules = array_merge($this->rules,[
        $name => 'date'
      ]);
    }
  }

  /**
   * Generates random alphanumeric
   * String for a certificate
   *
   * @return string
   */
  public function generateId ()
  {
    $alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N'];
    $three_alph = '';
    $eight_num = '';
    for ($i=0; $i < 3; $i++) {
      $three_alph .= $alphabet[rand(0, (count($alphabet) - 1))];
    }
    for ($i=0; $i < 8; $i++) {
      $eight_num .= rand(1, 9);
    }
    return $three_alph.'-'.$eight_num;
  }

  /**
   * Returns item fields for a particular
   * template
   *
   * @return array
   */
  public function getItemField()
  {
      $array =  [];
    foreach((Array) $this->jsonFile as $key => $field)
    {

        if(str_contains($key, 'items'))
        {
            array_push($array,$key);
        }

    }
      return $array;
  }
}
