<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string certificate_id
 * @property string content
 * @property array|null|string name
 * @property array|null|string customer_id
 * @property int creator_id
 * @property bool has_invoice
 * @property mixed id
 */
class Certificate extends Model
{
  /**
    * Get the customer that owns the certificate.
    */
  public function customer()
  {
    return $this->belongsTo('App\Customer');
  }

  public function invoice()
  {
    return $this->hasOne('App\Invoice', 'certificate_id')->withDefault();
  }

  /**
    * Get the user that created the certificate.
    */
  public function user()
  {
    return $this->belongsTo('App\User', 'creator_id')->withTrashed();
  }
}
