<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property array|null|string firstName
 * @property array|null|string middleName
 * @property array|null|string lastName
 * @property  creator_id
 * @property array|null|string phone
 * @property array|null|string state
 * @property array|null|string email
 * @property array|null|string address
 * @property  creator_id
 */
class Customer extends Model
{
  /**
   * Get the certificates for the user.
   */
  public function certificate()
  {
    return $this->hasMany('App\Certificate');
  }

  /**
     * Get the customer that owns the certificate.
     */
  public function user()
  {
    return $this->belongsTo('App\User', 'creator_id');
  }
}
