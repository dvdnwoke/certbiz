<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use Auth;
use App\Certificate;
use Carbon\Carbon;
use App\BankDetails;
use Illuminate\Support\Facades\DB;
use App\Setting;

class InvoiceController extends Controller
{

  public function __construct () {

    $this->middleware('auth');
    $this->middleware('setting');

  }

  public function paidInvoices() {
    $invoices = Invoice::where('paid', 1)->paginate(10);
    return view('accounting/invoice/list')->with([
      'invoices' => $invoices,
      'paid' => 'Paid'
    ]);
  }

  public function unpaidInvoices() {
    $invoices = Invoice::where('paid', 0)->paginate(10);
    return view('accounting/invoice/list')->with([
      'invoices' => $invoices,
      'unpaid' => 'Unpaid'
    ]);
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $invoice = Invoice::paginate(10);

      return view('accounting/invoice/list')->with([
        'invoices' => $invoice
      ]);
    }

    public function expiredMonthly () {
      $monthly = DB::table('invoices')->whereYear('expiry_date', date('Y'))
      ->whereMonth('expiry_date', date('m'))
      ->count();

      return response()->json([
        'data' => $monthly
      ]);
    }

    public function expiredYearly () {
      $yearly = [];
      for ($i = 1; $i <= 12; $i++) {
        $yearly[$i - 1] = DB::table('invoices')->whereYear('expiry_date', date('Y'))
        ->whereMonth('expiry_date', $i)
        ->count();
      }

      return response()->json([
        'data' => $yearly,
        'label' => [
         'January', 'February', 'March', 'April', 'May', 'June',
         'July', 'August', 'September', 'October', 'November', 'December'
        ]
      ]);
    }

    public function statistics () {
      $paid = [];
      $unpaid = [];
      $total = [];

      for ($i = 1; $i <= 12; $i++) {
        $paid[$i - 1] = DB::table('invoices')->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->where('paid', 1)
        ->count();

        $unpaid[$i - 1] = DB::table('invoices')->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->where('paid', 0)
        ->count();

        $total[$i - 1] = DB::table('invoices')->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->count();
      }

      return response()->json([
        'unpaid' => $unpaid,
        'paid' => $paid,
        'total' => $total,
        'label' => [
         'January', 'February', 'March', 'April', 'May', 'June',
         'July', 'August', 'September', 'October', 'November', 'December'
        ]
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function store(Request $request) {

       $this->validate($request, [
         'certificate_id' => 'required',
         'certificate_name' => 'required',
         'customer_id' => 'required',
         'expiry_date' => 'required',
         'paid' => 'required',
         'amount' => 'required',
       ]);

       $invoice = Invoice::where('certificate_id', $request->input('certificate_id'))
       ->first();
       if($invoice){
         return view('error/operation')->with([
           'message' => 'An invocie with certificate id already exist try editing the invoice, not creating new one'
         ]);
       }

       $invoice = new Invoice;
       $invoice->certificate_name = $request->input('certificate_name');
       $invoice->certificate_id = $request->input('certificate_id');
       $invoice->customer_id = $request->input('customer_id');
       $invoice->creator_id = Auth::user()->id;
       $invoice->expiry_date = Carbon::parse($request->input('expiry_date'));
       $invoice->paid = ($request->input('paid') === 'paid') ? 1 : 0 ;
       $invoice->amount = $request->input('amount');
       $invoice->save();

       $certificate = Certificate::
         where('id', $request->input('certificate_id'))->first();
       $certificate->has_invoice = true;
       $certificate->update();

       return view('accounting/invoice/create')->with([
         'message' => 'Invocie was created successfuly',
         'certificate' => $certificate,
         'invoice' => $invoice
       ]);

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $invoice = Invoice::where('id',$id)->first();
      if(!$invoice) {
        return view('error/operation')->with([
          'message' => 'Such Invoice does not exist'
        ]);
      }
      $bank_details = BankDetails::find(1);
      $setting = Setting::where('type','vat')->first();

      return view('accounting/invoice/view')->with([
        'invoice' => $invoice,
        'bank' => $bank_details,
        'setting' => $setting
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $invoice = Invoice::where('id', $id)->first();
      if(!$invoice){
        return view('error/operation')->with([
          'message' => 'Such Invoice does not exist'
        ]);
      }
      return view('accounting/invoice/update')->with([
        'invoice' => $invoice
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
      if(!$request->input('raise-invoice')) {
        $invoice = Invoice::where('id', $request->input('id'))->first();
        $certificate = Certificate::where('id', $invoice->certificate_id)->first();
        $certificate->has_invoice = false;
        $certificate->save();
        $invoice->delete();
        return view('accounting/invoice/update')->with([
          'invoice' => $invoice,
          'deleted' => true
        ]);
      }

      $this->validate($request, [
        'amount' => 'required',
        'paid' => 'required'
      ]);

      $invoice = Invoice::where('id', $request->input('id'))->first();
      $invoice->paid = ($request->input('paid') === 'paid') ?  1 : 0 ;



      if($request->input('expiry_date')){
        $invoice->expiry_date = $request->input('expiry_date');
      }

      $invoice->amount = $request->input('amount');
      $invoice->save();

      return view('accounting/invoice/update')->with([
        'invoice' => $invoice,
        'updated' => true
      ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $invoice = Invoice::find($id);
      if(!$invoice){
        return view('error/operation')->with([
          'message' => 'Such invoice does not exist'
        ]);
      }
      $certificate = Certificate::where('id', $invoice->certificate->id)->first();
      $invoice->delete();
      $certificate->has_invoice = false;
      $certificate->update();

      return redirect('accounting/invoice')
      ->with('deleted', true);

    }

    public function invoice_print ($id) {
      $invoice = Invoice::where('id', $id)->first();
      if(!$invoice) {
        return view('error/operation')->with([
          'message' => 'Such Invoice does not exist'
        ]);
      }
      $bank_details = BankDetails::find(1);
      $setting = Setting::where('type','vat')->first();
      return view('accounting/invoice/print')->with([
        'invoice' => $invoice,
        'setting' => $setting,
        'bank' => $bank_details
      ]);
    }


    public function search (Request $request) {
      $invoice = false;
      if($request->input('type') === 'invoice-id') {
        $invoice  = Invoice::where('id', 'like', '%'.$request->input('query').'%')
        ->get();

      }
      if($request->input('type') === 'certificate-number') {
        $certificate = Certificate::where('certificate_id', $request->input('query'))
        ->first();
        if($certificate) {
          $invoice = Invoice::where('certificate_id', $certificate->id)->get();
        }
      }

      return view('accounting/invoice/search')->with([
        'invoices' => $invoice,
        'query' => $request->input('query')
      ]);
    }

}
