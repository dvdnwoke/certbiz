<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Setting;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $user = Auth::user();
      if ($user->can('view', Setting::class)) {
        // Executes the "view" method on the relevant policy...
        return view('settings/index');
      }
      return view('error/permission');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $user = Auth::user();
      if ($user->can('view', Setting::class)) {
        $settings = Setting::where('type','general')->get();
        if($settings->count() >= 1) {
          $result = [];
          foreach ($settings as $key => $value) {
            $result = array_merge($result,[
              $value->name => $value->value
            ]);
          }
          return view('settings/general/update')->with([
            'settings' => $result
          ]);
        }
        return view('settings/general/index');
      }
      return view('error/permission');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'business-name' => 'required',
        'logo' => 'required | image:jpeg, png, bmp, gif'
      ]);
      $settings = new Setting;
      $settings->name = 'business-name';
      $settings->value = $request->input('business-name');
      $settings->type = 'general';
      $settings->save();

      if($request->input('alias')){
        $settings = new Setting;
        $settings->name = 'alias';
        $settings->value = $request->input('alias');
        $settings->type = 'general';
        $settings->save();
      }

      if($request->input('phone1')){

        $settings = new Setting;
        $settings->name = 'phone1';
        $settings->value = $request->input('phone1');
        $settings->type = 'general';
        $settings->save();

      }

      if($request->input('phone2')){

        $settings = new Setting;
        $settings->name = 'phone2';
        $settings->value = $request->input('phone2');
        $settings->type = 'general';
        $settings->save();

      }

      if($request->input('address')){
        $settings = new Setting;
        $settings->name = 'address';
        $settings->value = $request->input('address');
        $settings->type = 'general';
        $settings->save();
      }


      $settings = new Setting;
      $settings->name = 'logo';
      $settings->value = $request->file('logo')->store('public');
      $settings->type = 'general';
      $settings->save();


      return view('settings/general/index')->with([
        'created' => true
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $this->validate($request, [
        'logo' => 'image:jpeg, png, bmp, gif',
        'business-name' => 'required'
      ]);

      $setting = Setting::where('name', 'business-name')->first();
      $setting->value = $request->input('business-name');
      $setting->update();
      if($request->input('alias')){
        $setting = Setting::where('name', 'alias')->first();
        $setting->value = $request->input('alias');
        $setting->update();
      }

      if($request->input('phone1')){
        $setting = Setting::where('name', 'phone1')->first();
        if(!$setting) {
          $settings = new Setting;
          $settings->name = 'phone1';
          $settings->value = $request->input('phone1');
          $settings->type = 'general';
          $settings->save();
        }
        else {
          $setting->value = $request->input('phone1');
          $setting->update();
        }

      }

      if($request->input('phone2')){
        $setting = Setting::where('name', 'phone2')->first();
        if(!$setting) {
          $settings = new Setting;
          $settings->name = 'phone2';
          $settings->value = $request->input('phone2');
          $settings->type = 'general';
          $settings->save();
        }
        else {
          $setting->value = $request->input('phone2');
          $setting->update();
        }

      }

      if($request->input('address')){
        $setting = Setting::where('name', 'address')->first();
        if(!$setting) {
          $settings = new Setting;
          $settings->name = 'address';
          $settings->value = $request->input('address');
          $settings->type = 'general';
          $settings->save();
        }
        else {
          $setting->value = $request->input('address');
          $setting->update();
        }

      }


      if($request->file('logo')) {
        $setting = Setting::where('name', 'logo')->first();
        Storage::delete($setting->value);
        $path = $request->file('logo')->store('public');
        $setting->value = $path;
        $setting->update();
      }


      return view('settings/general/update')->with([
        'created' => true,
        'settings' => $setting
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
