<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use App\Certificate;

class TrackCertController extends Controller
{
  public function index () {
    $setting = Setting::where('name', 'logo')->first();
    return view('certificate/track')->with([
      'logo' => substr($setting->value, 7)
    ]);

  }

  public function show (Request $request) {
    $setting = Setting::where('name', 'logo')->first();
    $this->validate($request, [
      'certificate-number' => 'required'
    ]);
    $certificate = Certificate::where('certificate_id', $request->input('certificate-number'))->first();
      return view('certificate/track')->with([
        'logo' => substr($setting->value, 7),
        'certificate' => ($certificate) ? $certificate : false
      ]);
  }
}
