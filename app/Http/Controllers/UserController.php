<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Carbon\Carbon;

class UserController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */

  public function __construct () {
    $this->middleware('auth');
    $this->middleware('setting');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function admin_index()
  {
    $results = User::paginate(10);
    return view('settings/user/index')->with(['results' => $results]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function admin_create()
  {
    $user = Auth::user();
    if($user->can('create', User::class)) {
      return view('settings/user/create');
    }
    return view('error/permission');

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function admin_store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required',
      'email' => 'required | unique:users,email',
      'password' => 'same:password_confirmation',
      'role' => 'required'
    ]);

    $user = new User;
    $user->name = $request->input('name');
    $user->email = $request->input('email');
    $user->phone = $request->input('phone');
    $user->address = $request->input('address');
    $user->state = $request->input('state');
    $user->password = bcrypt($request->input('password'));
    $user->role = $request->input('role');
    $user->created_at = Carbon::now();
    $user->updated_at = Carbon::now();
    $user->save();

    return view('settings/user/create')->with([
      'registered' => 'User was created successfully'
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function admin_edit($id)
  {
    $user = User::find($id);
    return view('settings/user/update')->with([
      'staff' => $user
    ]);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function admin_show($id)
  {
    $staff = User::where('id', $id)->first();
    return view('settings/user/view')->with([
      'staff' => $staff
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function admin_update(Request $request, $id)
  {
    $this->validate($request, [
      'name' => 'required',
      'email' => 'required',
      'password' => 'same:password_confirmation',
    ]);

    $staff = User::find($id);
    $staff->name = $request->input('name');

    if($request->input('password')) {
      $staff->password = $request->input('password');
    }
    if($request->input('role')) {
      $staff->role = $request->input('role');
    }
    $staff->phone = $request->input('phone');
    $staff->state = $request->input('state');
    $staff->email = $request->input('email');
    $staff->address = $request->input('address');
    $staff->update();

    return view('settings/user/update')->with([
      'staff' => $staff,
      'updated' => 'Customer record was updated successfully'
    ]);
  }

  public function profile ($id) {
    if($id == Auth::user()->id) {
      return view('user/profile')->with([
        'user' => Auth::user()
      ]);
    }
    else {
      return view('error.permission');
    }
  }

  public function save (Request $request) {
    $this->validate($request, [
      'password' => 'required',
      'password' => 'same:password_confirmation',
    ]);

    if ($request->input('user-id') == Auth::user()->id) {
      $user = User::where('id', Auth::user()->id)->first();
      $user->password = bcrypt($request->input('password'));
      $user->update();
    }

    return view('user/profile')->with([
      'user' => Auth::user(),
      'updated' => true
    ]);

  }

  public function admin_destroy($id) {
    if(Auth::user()->role === 'admin') {
      $user  = User::find($id);
      $user->delete();
      return redirect('settings/user')->with([
        'deleted' => true
      ]);
    }
  }

}
