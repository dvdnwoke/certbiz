<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\Certificate\Template as CertificateRepository;
use App\Setting;
use Auth;
use Carbon\Carbon;
use App\Certificate;
use App\Invoice;
use App\BankDetails;
/**
 * Controller to handle the accounting
 * feature of the system.
 * 
 */
class AccountingController extends Controller
{
  public function __construct() {
    $this->middleware('auth');

  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function adminIndex()
  {
    $certRepo = new CertificateRepository();
    $certificates = $certRepo->getCertNames();
    $results = [];
    foreach ($certificates as $key => $value) {

      $setting = Setting::where('name', $value)->first();
      $amount = ($setting) ? $setting->value : 0;
      $results[$key] = array_merge($results, [
        'name' => $value['fullName'],
        'amount' => $amount
      ]);
    }

    $bank_details = BankDetails::find(1);
    $vat = Setting::where('type','vat')->first();

    return view('settings/accounting/index')->with([
      'certificates' => $results,
      'bank' => $bank_details,
      'vat' => $vat
    ]);
  }

  public function viewStatistics () {
    $paid = Invoice::where('paid', 1)->get();
    $unpaid = Invoice::where('paid', 0)->get();
    $total = Invoice::get();
    return view('accounting/statistics')->with([
      'paid' => $paid,
      'unpaid' => $unpaid,
      'total' => $total
    ]);
  }

  //returns list of certificate

  public function adminBank (Request $request) {

    $certRepo = new CertificateRepository();
    $certificates = $certRepo->getCertNames();
    $results = [];
    foreach ($certificates as $key => $value) {
      $setting = Setting::where('name', $value)->first();
      $amount = ($setting) ? $setting->value : 0;
      $results[$key] = array_merge($results, [
        'name' => $value['fullName'],
        'amount' => $amount
      ]);
    }

    $status = '';
    $this->validate($request, [
      'account-name' => 'required',
      'account-number' => 'required',
      'bank-name' => 'required'
    ]);

    $bank_details = BankDetails::find(1);
    if ($bank_details) {
      $status = 'updated';
      $bank_details->bank_name = $request->input('bank-name');
      $bank_details->account_name = $request->input('account-name');
      $bank_details->account_number = $request->input('account-number');
      $bank_details->update();

    } else {
      $status = 'created';
      $bank_details = new BankDetails;
      $bank_details->bank_name = $request->input('bank-name');
      $bank_details->account_name = $request->input('account-name');
      $bank_details->account_number = $request->input('account-number');
      $bank_details->save();

    }

    $vat = Setting::where('type','vat')->first();

    return view('settings/accounting/index')->with([
      'certificates' => $results,
      'bank' => $bank_details,
      'status' => $status,
      'vat' => $vat
    ]);

  }


  //returns list of certificate

  public function index() {
    $certificates = Certificate::where('has_invoice', false)->paginate(5);

    return view('accounting/index')->with([
      'certificates' => $certificates
    ]);
  }

  public function vat (Request $request) {

    $certRepo = new CertificateRepository();
    $certificates = $certRepo->getCertNames();
    $results = [];
    foreach ($certificates as $key => $value) {
      $setting = Setting::where('name', $value)->first();
      $amount = ($setting) ? $setting->value : 0;
      $results[$key] = array_merge($results, [
        'name' => $value,
        'amount' => $amount
      ]);
    }
    $status = '';
    $setting = Setting::where('type','vat')->first();

    if($setting) {
      $status = 'updated';
      $setting->name = 'VAT';
      $setting->value = $request->input('vat');
      $setting->type = 'vat';
      $setting->update();

    }else {
      $status = 'created';
      $setting = new Setting;
      $setting->name = 'VAT';
      $setting->value = $request->input('vat');
      $setting->type = 'vat';
      $setting->save();

    }

    $bank_details = BankDetails::find(1);

    return view('settings/accounting/index')->with([
      'certificates' => $results,
      'bank' => $bank_details,
      'vat' => $setting,
      'vatStatus' => $status
    ]);
  }



  public function getCertificateForm() {

    return view('accounting/get_certificate_form');

  }


  public function addInvoice($id) {

    $certificate = Certificate::
      where('certificate_id', $id)->first();
    if(!$certificate) {
      return view('error/operation')->with([
        'message' => 'Such Record does not exist'
      ]);
    }
    $setting = Setting::where('name', $certificate->name)->first();
    return view('accounting/invoice/create')->with([
      'certificate' => $certificate,
      'setting' => $setting
    ]);

  }

  public function getCertificate(Request $request) {

    $certificate = Certificate::
      where('certificate_id', $request->input('certificate_id'))->first();
    if(!$certificate) {
      return view('accounting/get_certificate_form')->with([
        'result' => $request->input('certificate_id')
      ]);
    }
    $setting = Setting::where('name', $certificate->name)->first();
    return view('accounting/invoice/create')->with([
      'certificate' => $certificate,
      'setting' => $setting
    ]);
  }

  /**
   * Add certificate price
   *
   * @return \Illuminate\Http\Response
   */

  public function adminAddCertificatePrice(Request $request) {

    $certRepo = new CertificateRepository();
    $this->validate($request, [
      'certificate-name' => 'required',
      'amount' => 'required'
    ]);

    $setting = Setting::where('name',$request->input('certificate-name'))->first();

    if($setting) {

      $setting->name = $request->input('certificate-name');
      $setting->value = $request->input('amount');
      $setting->type = 'certificate price';
      $setting->update();

    }else {

      $setting = new Setting;
      $setting->name = $request->input('certificate-name');
      $setting->value = $request->input('amount');
      $setting->type = 'certificate price';
      $setting->save();

    }

    $certificates = $certRepo->getCertNames();
    $results = [];
    foreach ($certificates as $key => $value) {
      $setting = Setting::where('name', $value)->first();
      $amount = ($setting) ? $setting->value : 0;
      $results[$key] = array_merge($results, [
        'name' => $value['fullName'],
        'amount' => $amount
      ]);
    }

    return view('settings/accounting/index')->with([
      'name' => $request->input('certificate-name'),
      'amount' => $request->input('amount'),
      'certificates' => $results
    ]);
  }
}
