<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Setting;
use App\Certificate;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Repository\Certificate\Template;
use Carbon\Carbon;
use App\Invoice;

/** 
 * This is a controller for all
 * related certificate tasks on
 * the system.
 */
class CertificateController extends Controller
{

  public function __construct () {

    $this->middleware('auth');
    $this->middleware('setting');

  }

  /**
   * List available certificates.
   *
   * @return \Illuminate\Http\Response
  */

  public function index () {
    $certificates = Certificate::orderBy('id','DESC')->paginate(10);
    return view('certificate/index')->with([
      'certificates' => $certificates
    ]);
  }

  /**
   * Show the form for selecting a new certificate.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

    public function selectCertificate($id)
    {
        $certTemplate = new Template();
        $customer = Customer::find($id);
        return view('certificate/select')->with([
          'certificates' => $certTemplate->getCertNames(),
          'customer' => $customer
        ]);
    }

    /**
     * Shows the certificates which
     *  will expiry in a month time
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
      public function expiredMonthly ()
      {

        $invoice = Invoice::whereYear('expiry_date', date('Y'))
            ->whereMonth('expiry_date', date('m'))
            ->paginate(10);

        return view('certificate/expire')->with([
          'invoices' => $invoice
        ]);

      }

  /**
   * Show the form for selecting a new certificate.
   * and user to create cert for
   *
   * @return \Illuminate\Http\Response
  */

  public function selectCertAndUser () {

    $certTemplate = new Template();
    $customer = Customer::get();

    return view('certificate/create')->with([
      'certificates' => $certTemplate->getCertNames(),
      'customers' => $customer
    ]);

  }

    /**
     * Returns statistics for chart
     *
     * @return \Illuminate\Http\JsonResponse
     */
      public function statistics () {
        $data = [];
        for ($i = 1; $i <= 12; $i++) {
          $data[$i - 1] = DB::table('certificates')->whereYear('created_at', date('Y'))
              ->whereMonth('created_at', $i)
              ->count();
        }
        return response()->json([
          'data' => $data,
          'label' => [
           'January', 'February', 'March', 'April', 'May', 'June',
           'July', 'August', 'September', 'October', 'November', 'December'
          ]
        ]);
      }


    /**
     * Show the form for creating cert.
     * with its template
     *
     * @param Request $request
     * @return Response $response
     */
  public function create (Request $request) {

      $certificateName =  $request->input('certificate-name');
      $this->validate($request, [
          'customer-id' => 'required',
          'certificate-name' => 'required'
      ]);

      $certTemplate = new Template($certificateName);
      $customer = Customer::find($request->input('customer-id'));
      return view($certTemplate->getFormPath($certificateName, 'Create'))->with([
          'certificateName' => $certTemplate->getCertificateName(),
          'certificatePrettyName' => $certTemplate->getCertificateName(true),
          'customer' => $customer
      ]);
  }

  /**
   * Store customer certificate.
   * following  its template
   * @param Request $request
   * @return \Illuminate\Http\Response
  */

  public function store (Request $request) {

    $certTemplate = new Template($request->input('certificate-name'));

    $rules = $certTemplate->getRule();
    $rules =  array_merge($rules, [
      'customer-id' => 'required',
      'certificate-name' => 'required'
    ]);

    $this->validate($request, $rules);

    $certificate = new Certificate;

    if($request->input('raise-invoice')){

      $certificate->has_invoice = true;
    }
    else {
      $certificate->has_invoice = false;
    }

    $certificate->certificate_id = $certTemplate->generateId();
    $certificate->content = json_encode($certTemplate->getData($request), true);
    $certificate->name = $request->input('certificate-name');
    $certificate->customer_id = $request->input('customer-id');
    $certificate->creator_id = Auth::user()->id;
    $certificate->save();

    if($request->input('raise-invoice')){

      $this->validate($request, [
        'certificate-name' => 'required',
        'customer-id' => 'required',
        'expiry_date' => 'required',
        'paid' => 'required',
        'amount' => 'required',
      ]);

      $invoice = Invoice::where('certificate_id', $certificate->id)
      ->first();
      if($invoice){
        return view('error/operation')->with([
          'message' => 'An invoice with certificate id already exist try editing the invoice, not creating new one'
        ]);
      }


      $invoice = new Invoice;
      $invoice->certificate_name = $request->input('certificate-name');
      $invoice->certificate_id = $certificate->id;
      $invoice->customer_id = $request->input('customer-id');
      $invoice->creator_id = Auth::user()->id;
      $invoice->expiry_date = Carbon::parse($request->input('expiry_date'));
      $invoice->paid = ($request->input('paid') === 'paid') ? 1 : 0 ;
      $invoice->amount = $request->input('amount');
      $invoice->save();
    }

    return redirect('certificate/view/' . $certificate->id)->with([
      'status' => true,
      'message' => 'Customer Certificate Was Created Successfully'
    ]);
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function show($id)
  {
      $setting = Setting::where('name', 'logo')->first();
      $address = Setting::where('name', 'address')->first();
      $phone = Setting::where('name', 'phone1')->first();
      $alt_phone = Setting::where('name', 'phone2')->first();
      $certificate = Certificate::where('id', $id)->first();
      $certTemplate = new Template($certificate->name);
      $content = json_decode($certificate->content, true);
      return view($certTemplate->getViewPath($certificate->name))->with([
          'certificateName' => $certTemplate->getCertificateName(),
          'certificatePrettyName' => $certTemplate->getCertificateName(true),
          'certificate' => $certificate,
          'content' => $content,
          'logo' => substr($setting->value, 7),
          'address' => $address->value,
          'phone' => $phone->value,
          'alt_phone' => $alt_phone->value,
          'certificateHeaderName' => $certTemplate->getCertificateHeaderName()
      ]);
  }

  /**
   * Shows certificate for printing
   *
   * @param $id
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function certPrint ($id) {

    $setting = Setting::where('name', 'logo')->first();
    $address = Setting::where('name', 'address')->first();
    $phone = Setting::where('name', 'phone1')->first();
    $alt_phone = Setting::where('name', 'phone2')->first();
    $certificate = Certificate::where('id', $id)->first();
    $certTemplate = new Template($certificate->name);
    $invoice =  Invoice::where('certificate_id', $id)->first();
    if($invoice){
      $content = json_decode($certificate->content, true);
      return view($certTemplate->getViewPath($certificate->name, 'Print'))->with([
        'certificate' => $certificate,
        'content' => $content,
        'logo' => substr($setting->value, 7),
        'address' => $address->value,
        'phone' => $phone->value,
        'alt_phone' => $alt_phone->value,
        'certificateHeaderName' => $certTemplate->getCertificateHeaderName()
      ]);
    }
    else {
      return view('error/operation')->with([
        'message' => 'Certificate must have an invoice before it can be printed.'
      ]);
    }


  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $certTemplate = new Template();
    $certificate = Certificate::find($id);
    $invoice = false;
    if($certificate->has_invoice) {
      $invoice = Invoice::where('certificate_id', $certificate->id)->first();
    }
    $content = json_decode($certificate->content,true);;
    return view($certTemplate->getFormPath($certificate->name, 'Edit'))->with([
      'content' => $content,
      'certificate' => $certificate,
      'invoice' => $invoice,
      'invoice_status' => ($certificate->has_invoice) ? 'true' : 'false'
    ]);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {

    $certificate = Certificate::where('id',$id)->first();
    $invoice = Invoice::where('certificate_id',$id)->first();
    if($invoice) {
      $invoice->delete();
    }
    $certificate->delete();

    return redirect('certificate')
    ->with('deleted', true);

  }

  /**
   * Searches for a particular certificate
   *
   * @param Request $request
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */

  public function search (Request $request) {
    $certificate = '';
    if ($request->input('type') === 'customer-group'){
      $certificate = Certificate::
      where('name', 'like', '%'.$request->input('query').'%')->get();
    }
    if ($request->input('type') === 'customer-number') {
      $certificate = Certificate::
      where('certificate_id', '=', $request->input('query'))->get();
    }

    return view('certificate/search')->with([
      'results' => $certificate,
      'query' => $request->input('query')
    ]);

  }

  /**
   * Updates a particular certificate
   * with new details provided.
   *
   * @param Request $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function update (Request $request) {

    $certTemplate = new Template();

    $rules = $certTemplate->setCertificateName($request->input('certificate-name'))
      ->getRule();
      
    $rules =  array_merge($rules,[
      'customer-id' => 'required',
      'certificate-name' => 'required',
      'certificate-id' => 'required'
    ]);

    $this->validate($request, $rules);
    $certificate = Certificate::where('id', $request->input('certificate-id'))->first();
    if($request->input('raise-invoice')){
      $certificate->has_invoice = true;
    }
    else {
      $certificate->has_invoice = false;
    }

    $certificate->content = json_encode($certTemplate->getData($request), true);


    $invoice = Invoice::where('certificate_id', $request->input('certificate-id'))
          ->first();

	  $certificate->update();

    if($request->input('raise-invoice')){

        $this->validate($request, [
            'certificate-name' => 'required',
            'customer-id' => 'required',
            'expiry_date' => 'required',
            'paid' => 'required',
            'amount' => 'required',
        ]);
      if($invoice){
        //Editing old invoice
        $invoice->expiry_date = Carbon::parse($request->input('expiry_date'));
        $invoice->paid = ($request->input('paid') === 'paid') ? 1 : 0 ;
        $invoice->amount = $request->input('amount');
        $invoice->update();
      }
      else {

        //Creating new invoice
        $invoice = new Invoice;
        $invoice->certificate_name = $request->input('certificate-name');
        $invoice->certificate_id = $certificate->id;
        $invoice->customer_id = $request->input('customer-id');
        $invoice->creator_id = Auth::user()->id;
        $invoice->expiry_date = Carbon::parse($request->input('expiry_date'));
        $invoice->paid = ($request->input('paid') === 'paid') ? 1 : 0 ;
        $invoice->amount = $request->input('amount');
        $invoice->save();
      }


    }else { //Check if there is an already existing invoice and delete
        if ($invoice)
        {
            $invoice->delete();
        }
    }

    return redirect('certificate/view/'.$request->input('certificate-id'))->with([
      'status' => true,
      'message' => 'Certificate was updated successfully'
    ]);


  }

}
