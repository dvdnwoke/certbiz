<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Certificate;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('setting');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $num_of_customers = Customer::count();
      $num_of_certificates = Certificate::count();
      $num_of_users = User::count();
      return view('home')->with([
        'num_of_customers' => $num_of_customers,
        'num_of_certificates' => $num_of_certificates,
        'num_of_users' => $num_of_users
      ]);
    }
}
