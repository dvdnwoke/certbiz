<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Certificate;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{

    public function __construct () {

      $this->middleware('auth');
      $this->middleware('setting');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $results = Customer::paginate(10);
      return view('customer/index')->with(['results' => $results]);
    }

    public function statistics () {
      $data = [];
      for ($i = 1; $i <= 12; $i++) {
        $data[$i - 1] = DB::table('customers')
            ->whereYear('created_at', date('Y'))
            ->whereMonth('created_at', $i)
            ->count();
      }
      return response()->json([
          'data' => $data,
          'label' => [
              'January', 'February', 'March', 'April', 'May', 'June',
              'July', 'August', 'September', 'October', 'November',
              'December'
        ]
      ]);
    }

    /**
     * Show results for searched customers in json format
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */


    public function searchJson (Request $request) {

      $this->validate($request, [
        'query' => 'required'
      ]);

      if($request->input('by') === 'name') {
        $query = '%'.$request->input('query').'%';
        $customers = Customer::where('firstName', 'like', $query)
        ->orWhere('middleName', 'like', $query)
        ->orWhere('lastName', 'like', $query)
        ->take(10)->get();

        return response()->json([
          'results' => $customers,
          'by' => $request->input('by'),
          'query' => $request->input('query')
        ]);

      }
      $query = '%'.$request->input('query').'%';
      $customers = Customer::where($request->input('by'), 'like', $query)
      ->take(10)->get();

      return response()->json([
        'results' => $customers,
        'by' => $request->input('by'),
        'query' => $request->input('query')
      ]);
    }

    /**
     * Show results for searched customers
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
      $this->validate($request, [
        'query' => 'required'
      ]);

      if($request->input('by') === 'name') {
        $query = '%'.$request->input('query').'%';
        $customers = Customer::where('firstName', 'like', $query)
        ->orWhere('middleName', 'like', $query)
        ->orWhere('lastName', 'like', $query)
        ->paginate(10);

        return view('customer/search')->with([
          'results' => $customers,
          'by' => $request->input('by'),
          'query' => $request->input('query')
        ]);

      }
      $query = '%'.$request->input('query').'%';
      $customers = Customer::where($request->input('by'), 'like', $query)
      ->paginate(10);

      return view('customer/search')->with([
        'results' => $customers,
        'by' => $request->input('by'),
        'query' => $request->input('query')
      ]);
    }

    /**
     * Show for creating new customer
     *
     * @return Response;
     */
    public function create()
    {
        return view('customer/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstName' => 'required',
            'lastName' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'state' => 'required',
            'address' => 'required',
        ]);

        $customer = new Customer;
        $customer->firstName = $request->input('firstName');
        $customer->middleName = $request->input('middleName');
        $customer->lastName = $request->input('lastName');
        $customer->creator_id = \Auth::user()->id;
        $customer->phone = $request->input('phone');
        $customer->state = $request->input('state');
        $customer->email = $request->input('email');
        $customer->address = $request->input('address');
        $customer->save();

        return view('customer/create')->with([
            'status' => true,
            'customer' => $customer,
            'message' => 'Customer was created successfully',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $customer = Customer::where('id', $id)->first();

      if($customer) {
        $cert = Certificate::where('customer_id',$customer->id)->paginate(5);
        return view('customer/view')->with([
          'customer' => $customer,
          'has_certificate' => $cert,
          'certificate' => false
        ]);
      }
      return view('error/operation')->with([
        'message' => 'Such customer does not exist'
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $customer = Customer::find($id);
      if($customer) {
        return view('customer/update')->with(['customer' => $customer]);
      }
      return view('error/operation')->with([
        'message' => 'Such customer does not exist'
      ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'firstName' => 'required',
        'lastName' => 'required',
        'phone' => 'required',
        'email' => 'required',
        'state' => 'required',
        'address' => 'required',
      ]);

      $customer = Customer::find($id);
      if(!$customer) {
        return view('error/operation')->with([
          'message' => 'Such customer does not exist'
        ]);
      }
      $customer->firstName = $request->input('firstName');
      $customer->middleName = $request->input('middleName');
      $customer->lastName = $request->input('lastName');
      $customer->phone = $request->input('phone');
      $customer->state = $request->input('state');
      $customer->email = $request->input('email');
      $customer->address = $request->input('address');
      $customer->save();

      return view('customer/update')->with([
        'status' => true,
        'customer' => $customer,
        'message' => 'Customer record was updated successfully'
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $customer = Customer::find($id);
      if(!$customer){
        return view('error/operation')->with([
          'message' => 'Such customer does not exist'
        ]);
      }
      $customer->delete();

      $results = Customer::paginate(10);
      return view('customer/index')->with([
        'delete' => true,
        'results' => $results,
        'message' => 'User was deleted successfully']);
    }
}
