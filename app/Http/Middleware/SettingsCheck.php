<?php

namespace App\Http\Middleware;

use Closure;
use App\Setting;
use App\BankDetails;

class SettingsCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $alias = Setting::where('name', 'alias')->first();
      $address = Setting::where('name', 'address')->first();
      $logo = Setting::where('name', 'logo')->first();
      $name = Setting::where('name', 'business-name')->first();
      $vat = Setting::where('type', 'vat')->first();
      $bank = BankDetails::find(1);

      if (empty($name) || empty($vat) || empty($bank) || empty($logo) || empty($address)) {
        return redirect('settings/')->with([
          'incomplete' => true,
          'address' => (empty($address)) ? 'Adress is empty try filling it <a href="settings/general">click here</a>.' : '',
          'bank' => (empty($bank)) ? 'Bank Details is empty try filling it <a href="settings/accounting">click here</a>.' : '',
          'vat' => (empty($vat)) ? 'Vat is empty try filling it <a href="settings/accounting">click here</a>.' : '',
          'name' => (empty($name)) ? 'Bussiness Name is empty try filling it <a href="settings/general">click here</a>.' : '',
          'logo' => (empty($logo)) ? 'Logo is not uploaded yet try uploading it <a href="settings/general">click here</a>.' : '',
          'alias' => (empty($alias)) ? 'Alias Name is empty try filling it <a href="settings/general">click here</a>.' : ''
        ]);
      }
      return $next($request);
    }
}
