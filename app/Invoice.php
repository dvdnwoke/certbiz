<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property array|null|string certificate_name
 * @property mixed certificate_id
 * @property array|null|string customer_id
 * @property  int creator_id
 * @property static expiry_date
 * @property int paid
 * @property array|null|string amount
 */
class Invoice extends Model
{
  public function user () {

    return $this->belongsTo('App\User', 'creator_id');

  }

  public function customer () {

    return $this->belongsTo('App\Customer');

  }

  public function certificate () {

    return $this->belongsTo('App\Certificate');

  }
}
