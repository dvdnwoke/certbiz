<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->String('firstName')->notNullable();
            $table->String('middleName')->nullable();
            $table->String('lastName')->nullable();
            $table->String('phone')->notNullable();
            $table->String('email')->notNullable();
            $table->integer('creator_id')->notNullable();
            $table->String('state')->notNullable();
            $table->String('address')->notNullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
