<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'firstName' => str_random(10),
            'middleName' => str_random(10),
            'lastName' => str_random(10),
            'phone' => '+234' . random_int(9, 9),
            'email' => str_random(10) . '@gmail.com',
            'creator_id' => 1,
            'state' => str_random(15),
            'address' => str_random(10),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
