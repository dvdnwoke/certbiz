<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index');

Route::get('/', 'HomeController@index');

Route::get('certificate/track', 'TrackCertController@index');
Route::get('certificate/check', 'TrackCertController@show');

Route::get('/register', function () {
  abort(404);
});

Route::post('/register', function () {
  abort(404);
});

Route::prefix('user')->group(function() {
  Route::get('profile/{id}', 'UserController@profile');
  Route::post('profile', 'UserController@save');
});

Route::prefix('customer')->group(function () {
  Route::post('statistics', 'CustomerController@statistics');
  Route::get('/', 'CustomerController@index');
  Route::get('/create', 'CustomerController@create');
  Route::post('/search/json', 'CustomerController@searchJson');
  Route::post('/search', 'CustomerController@search');
  Route::get('/delete/{id}', 'CustomerController@destroy');
  Route::post('/update/{id}', 'CustomerController@update');
  Route::get('/update/{id}', 'CustomerController@edit');
  Route::post('/create', 'CustomerController@store');
  Route::get('/view/{id}', 'CustomerController@show');
});

Route::prefix('certificate')->group(function () {
  Route::get('/', 'CertificateController@index');
  Route::post('yearly/expiry', 'InvoiceController@expiredYearly');
  Route::post('monthly/expiry', 'InvoiceController@expiredMonthly');
  Route::get('monthly/expiry', 'CertificateController@expiredMonthly');
  Route::post('statistics', 'CertificateController@statistics');
  Route::post('/store', 'CertificateController@store');
  Route::get('/create', 'CertificateController@create');
  Route::post('/search', 'CertificateController@search');
  Route::get('/print/{id}', 'CertificateController@certPrint');
  Route::get('delete/{id}', 'CertificateController@destroy');
  Route::get('/select-user-cert', 'CertificateController@selectCertAndUser');
  Route::get('/create/select/{id}', 'CertificateController@selectCertificate');
  Route::get('/view/{id}', 'CertificateController@show');
  Route::post('/update', 'CertificateController@update');
  Route::get('/update/{id}', 'CertificateController@edit');
});

Route::prefix('accounting')->group(function(){
  Route::get('invoices/paid', 'InvoiceController@paidInvoices');
  Route::get('invoices/unpaid', 'InvoiceController@unpaidInvoices');
  Route::get('/', 'AccountingController@index');
  Route::post('statistics', 'InvoiceController@statistics');
  Route::get('/statistics', 'AccountingController@viewStatistics');
  Route::get('manage/invoice/search/', 'InvoiceController@search');
  Route::get('invoice/print/{id}', 'InvoiceController@invoicePrint');
  Route::get('invoice/', 'InvoiceController@index');
  Route::get('invoice/get-certificate', 'AccountingController@getCertificateForm');
  Route::post('invoice/get-certificate', 'AccountingController@getCertificate');
  Route::get('invoice/update/{id}', 'InvoiceController@edit');
  Route::post('invoice/update', 'InvoiceController@update');
  Route::get('invoice/view/{id}', 'InvoiceController@show');
  Route::get('invoice/delete/{id}', 'InvoiceController@destroy');
  Route::get('invoice/add-invoice/{id}', 'AccountingController@addInvoice');
  Route::post('invoice/create', 'InvoiceController@store');
});

Route::prefix('settings')->group(function () {
  Route::get('/general', 'SettingsController@create');
  Route::post('/general/create', 'SettingsController@store');
  Route::post('/general/update/', 'SettingsController@update');
  Route::get('/', 'SettingsController@index');
  Route::get('/user', 'UserController@admin_index');
  Route::get('/user/create', 'UserController@adminCreate');
  Route::get('/user/update/{id}', 'UserController@adminEdit');
  Route::post('/user/update/{id}', 'UserController@adminUpdate');
  Route::get('/user/delete/{id}', 'UserController@adminDestroy');
  Route::get('/user/view/{id}', 'UserController@adminShow');
  Route::post('/user/create', 'UserController@adminStore');
  Route::get('/accounting', 'AccountingController@adminIndex');
  Route::post('/accounting/vat', 'AccountingController@vat');
  Route::post('/accounting/bank-details', 'AccountingController@adminBank');
  Route::post('/accounting/certificate', 'AccountingController@adminAddCertificatePrice');
});

Auth::routes();
