getElementById('printButton').onclick = init


function init (e) {
    e.preventDefault()
    // Load css
    loadCss('/css/print.css')
    let app = getElementById('body')
    let appClone = app.cloneNode(true)
    let printable = getElementById('printThis')
    document.body.innerHTML = printable.innerHTML
    let tables = document.getElementsByTagName('table')
    
    for (let index = 0; index < tables.length; index++) {
        const element = tables[index]
        console.log(element)
        for (let i = 0; i < element.classList.length; i++) {
            const tableClass = element.classList[i];
            
            if (tableClass === 'table') { 
                element.classList.remove('table') 
                element.classList.remove('table-bordered')
                element.classList.add('print-table') 
                element.classList.add('my-table') 
            }
        }
    }
    displayPrintables()
    window.print()
    document.body.innerHTML = appClone.innerHTML
    getElementById('printButton').onclick = init
}

function getElementById (id) {
    return document.getElementById(id)
}

function createElement () {
    return document.createElement('link')
}

function displayPrintables () {
    getElementById('header').style.display = 'block'
    getElementById('footer').style.display = 'block'
    getElementById('detailsView').style.display = 'none'
    getElementById('detailsPrint').style.display = 'table'
    getElementById('showOnPrint').style.display = 'block'
}

function loadCss (href) {
    let head = document.getElementsByTagName('head')[0]
    let link = createElement ('link')
    link.rel  = 'stylesheet'
    link.type = 'text/css'
    link.href = href
    link.media = 'print'
    head.appendChild(link)
}