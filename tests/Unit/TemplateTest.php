<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Repository\Certificate\Template;

class TemplateTest extends TestCase
{
    /**
     * Instance of Template
     * class
     * 
     * @var Template
     */
    private $template;

    /**
     * Test certificate name
     * 
     * @var String
     */
    private $testCert;

    public function setUp()
    {
        parent::setUp();
        $this->template = new Template;
        // Get first Certificate Template
        $this->testCert = 'Hydro Test Certificate_portable Fire Extinguishers';
        $this->template
            ->setCertificateName($this->testCert);

    }

    /**
     * Test for getRule which 
     * returns rules necessary
     * for validating input
     */

     public function testGetRule()
     {
        $rule = $this->template
            ->getRule();
        $this->assertInternalType('array', $rule);
     }

    /**
     * Tests getCertNames against
     * returning available certitifate
     * names
     * 
     * @return void
     */

    public function testGetCertNames()
    {
        //TODO: Make the test better
        
        $returnedFiles = $this->template->getCertNames();
        $this->assertEquals($returnedFiles, $returnedFiles);
    }

    /**
     * Tests the getViewPath function
     * on Template Class with returns
     * a Certificate Template Path for
     * Views(ie Print, View).
     *
     * @return void
     */
    public function testGetViewPath()
    {
        // View Optional Parameter
        $path = $this->template->getViewPath($this->testCert);
        $expectedPath = 'certificate/templates/' . $this->template->getCertificateName() . '/view/index';
        $this->assertEquals($expectedPath, $path);

        // View 
        $path = $this->template->getViewPath($this->testCert, 'View');
        $expectedPath = 'certificate/templates/' . $this->template->getCertificateName() . '/view/index';
        $this->assertEquals($expectedPath, $path);

        // Print
        $path = $this->template->getViewPath($this->testCert, 'Print');
        $expectedPath = 'certificate/templates/' . $this->template->getCertificateName() . '/view/print';
        $this->assertEquals($expectedPath, $path);
    }

    /**
     * Tests the getFormPath method
     * which is suppose to return path
     * to forms (ie Edit, Create)
     * 
     * @return void
     */

    public function testGetFormPath()
    {
        // Default Parameter
        $path = $this->template->getFormPath($this->testCert);
        $expectedPath = 'certificate/templates/' . $this->template->getCertificateName() . '/form/create';
        $this->assertEquals($expectedPath, $path);

        // With Type Parameter
        $path = $this->template->getFormPath($this->testCert, 'Create');
        $expectedPath = 'certificate/templates/' . $this->template->getCertificateName() . '/form/create';
        $this->assertEquals($expectedPath, $path);

        // With Type Parameter
        $path = $this->template->getFormPath($this->testCert, 'Edit');
        $expectedPath = 'certificate/templates/' . $this->template->getCertificateName() . '/form/edit';
        $this->assertEquals($expectedPath, $path);
    }

    /**
     * Tests against get certificate header name
     * to be used during printing
     */
    public function testGetCertificateHeaderName()
    {
        $headerName = $this->template->getCertificateHeaderName();
        $expectedHeaderName = explode('_', $this->template->getCertificateName())[0];
        $this->assertEquals($headerName, $expectedHeaderName);
    }
}
