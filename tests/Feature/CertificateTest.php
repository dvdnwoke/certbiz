<?php

namespace Tests\Feature;

use App\Repository\Certificate\Template;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Customer;
use App\Certificate;

class CertificateTest extends TestCase
{
    /**
     * Factory created user to be used
     * for tests.
     *
     * @var string
     */
    private $user;

    /**
     * Factory created customer to be used
     * for tests.
     *
     * @var string
     */
    private $customer;

    /**
     * Certificate to be used
     * for tests.
     *
     * @var string
     */
    private $certificate;

    /**
     * Contains items name for a particular certificate
     * which can be used to save items data
     *
     * @var array
     */
    private $items;

    public function setUp()
    {
        parent::setUp();
        Session::start();
        $this->user = factory(User::class)->create();
        $this->customer = factory(Customer::class)->create();
        $this->certificate = Certificate::orderBy('id', 'desc')->first();
        $template = new Template($this->certificate->name);
        $this->items = $template->getItemField();
        foreach ($this->items as $key => $value)
        {
            $this->items[$value] = json_encode([
                'S/N' => '233'
            ]);
            unset($this->items[$key]);
        }

    }

    /**
     * Tests against Listing last
     * few created certificates.
     *
     * @return void
     */

    public function testCertificateList()
    {
        $response = $this->actingAs($this->user)->get('certificate');

        $response->assertStatus(200)
            ->assertViewHas('certificates');
    }
    /**
     * Tests against show form
     * for certificate
     *
     * @return void
     */
    public function testCertificateShowForm()
    {
        $response = $this->actingAs($this->user)->call('GET','certificate/create', [
            '_token' => csrf_token(),
            'customer-id' => $this->customer->id,
            'certificate-name' => $this->certificate->name
        ]);

        $response->assertStatus(200)
            ->assertSee($this->certificate->name);
    }


    /**
     * Tests against show form for
     * creating new certificate when
     * a customer is already selected
     *
     * @return void;
     */
    public function testCertificateCreateFormWhenCustomerIsSelected()
    {
        $uri = 'certificate/create/select/' . $this->customer->id;
        $response = $this->actingAs($this->user)
            ->get($uri);

        $response->assertStatus(200)
            ->assertSee($this->customer->firstName)
            ->assertViewIs('certificate.select');
    }

    /**
     * Tests against showing the certificates
     * which will expiry within a month time
     *
     * @return void
     */
    public function testCertificateExpiresMonthly()
    {
        $response = $this->actingAs($this->user)
            ->get('certificate/monthly/expiry');

        $response->assertStatus(200)
            ->assertViewIs('certificate.expire')
            ->assertViewHas('invoices');
    }

    /**
     * Tests against returning template form
     * for creating a certificate
     *
     * @return void
     */
    public function testCertificateGetTemplateForCreatingCertificate()
    {
        $response = $this->actingAs($this->user)->call('GET', 'certificate/create',[
            '_token' => csrf_token(),
            'customer-id' => $this->customer->id,
            'certificate-name' => $this->certificate->name
        ]);

        $response->assertStatus(200)
            ->assertSee($this->certificate->name);
    }
    /**
     * Test against returning statistics for
     * chart
     *
     * @return void
     */
    public function testCertificateStatistics()
    {
        $response = $this->actingAs($this->user)
            ->post('certificate/statistics',[
                '_token' => csrf_token()
            ]);
        $response->assertStatus(200)
            ->assertJsonFragment([
                'label'
            ]);
    }

    /**
     * Tests against show form for
     * selecting customer and certificate
     * before returning certificate form
     *
     * @return void
     */
    public function testCertificateSelectUserAndCertBeforeCreate()
    {
        $certificateTemplate = new Template();
        $certificates = $certificateTemplate->getCertNames();
        $response = $this->actingAs($this->user)->call('GET','certificate/select-user-cert', [
            '_token' => csrf_token()
        ]);

        $response->assertStatus(200)
            ->assertSee('Create new certificate')
            ->assertViewHasAll([
                'certificates' => $certificates,
                'customers'
            ]);
    }

    /**
     * Tests against creating a certificate
     *
     * @return void
     */
    public function testCertificateCreate()
    {
        //Without invoice
        $requestData = [
                '_token' => csrf_token(),
                'customer-id' => $this->customer->id,
                'certificate-name' => $this->certificate->name
            ];
        $requestData = array_merge($requestData, $this->items);

        $response = $this->actingAs($this->user)
            ->post('certificate/store', $requestData);

        $response->assertStatus(302)
            ->assertSessionHas([
                'message' => 'Customer Certificate Was Created Successfully'
            ])
            ->isRedirect();

        //With invoice

        $requestData = [
                '_token' => csrf_token(),
                'raise-invoice' => true,
                'customer-id' => $this->customer->id,
                'certificate-name' => $this->certificate->name,
                'expiry_date' => Carbon::now(),
                'paid' => 'paid',
                'amount' => random_int(3, 5)
            ];
        $requestData = array_merge($requestData, $this->items);
        $response = $this->actingAs($this->user)
            ->post('certificate/store', $requestData);


        $response->assertStatus(302)
            ->assertSessionHas([
                'message' => 'Customer Certificate Was Created Successfully'
            ])
            ->isRedirect();
    }

    /**
     * Tests against showing a particular
     * certificate
     *
     * @return void
     */

    public function testCertificateShow()
    {

        $uri = 'certificate/view/' . $this->certificate->id;
        $response = $this->actingAs($this->user)->get($uri);
        $response->assertStatus(200);
    }

    /**
     * Tests against showing view for
     * printing a certificate
     */
    public function testCertificatePrint()
    {
        $uri = 'certificate/print/' . $this->certificate->id;
        $response = $this->actingAs($this->user)->get($uri);

        $response->assertStatus(200)
            ->assertViewHas('certificate');
    }



    /**
     * Tests against showing the form
     * for editing certificate
     *
     * @return void
     */

    public function testCertificateEdit()
    {
        $uri = 'certificate/update/' . $this->certificate->id;
        $response = $this->actingAs($this->user)->get($uri);

        $response->assertStatus(200)
            ->assertViewHasAll([
                'content',
                'certificate'
            ]);
    }

    /**
     * Tests against editing a certificate
     *
     * @return void
     */

    public function testCertificateUpdate()
    {
        //Without Invoice
        $requestData = [
            '_token' => csrf_token(),
            'certificate-id' => $this->certificate->id,
            'certificate-name' => $this->certificate->name,
            'customer-id' => $this->certificate->customer->id
        ];
        $requestData = array_merge($requestData, $this->items);


        $response =  $this->actingAs($this->user)->post('certificate/update', $requestData);


        $response->assertStatus(302)
            ->assertSessionHas('message');

        $requestData = [
                '_token' => csrf_token(),
                'certificate-id' => $this->certificate->id,
                'certificate-name' => $this->certificate->name,
                'raise-invoice' => true,
                'customer-id' => $this->certificate->customer->id,
                'expiry_date' => Carbon::tomorrow(),
                'paid' => 'paid',
                'amount' => random_int(3, 4)
            ];
        $requestData = array_merge($requestData, $this->items);

        //With Invoice
        $response =  $this->actingAs($this->user)->post('certificate/update', $requestData);

        $response->assertStatus(302)
            ->assertSessionHas('message');
    }


    /**
     * Tests against searching for a certificate
     *
     * @return void
     */

    public function testCertificateSearch()
    {
        $response = $this->actingAs($this->user)->post('certificate/search/', [
            'query' => $this->certificate->name,
            'type' => 'customer-group',
            '_token' => csrf_token()
        ]);

        $response->assertStatus(200)
            ->assertViewHasAll([
                'results',
                'query' => $this->certificate->name
            ]);
        $response = $this->actingAs($this->user)->post('certificate/search/', [
            'query' => $this->certificate->certificate_id,
            'type' => 'customer-number',
            '_token' => csrf_token()
        ]);

        $response->assertStatus(200)
            ->assertViewHasAll([
                'results',
                'query' => $this->certificate->certificate_id
            ]);
    }

    /**
     * Tests against deleting a particular
     * certificate.
     *
     * @return void
     */

    public function testCertificateDestroy()
    {
        $uri = 'certificate/delete/' . $this->certificate->id;
        $response = $this->actingAs($this->user)->get($uri);

        $response->assertStatus(302)
            ->assertSessionHas([
                'deleted' => true
            ]);

    }

}
